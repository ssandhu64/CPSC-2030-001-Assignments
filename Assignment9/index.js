let value;

//OPTIONAL: This function refreshes the page every 3 seconds to load the chat results if there are more than one windows open.
setInterval( function(){display_message()} , 3000);

function display_message()
{
  value = new XMLHttpRequest();

  value.onreadystatechange = function()
  {
    document.getElementById("chatId").innerHTML = value.responseText;
  }

  value.open('GET' , 'chatFile.php' , true);
  value.send();
}
