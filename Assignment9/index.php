<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="UTF-8">

        <!--Styles-->
        <link rel="stylesheet/less" type="text/css" href="index.less">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.3/less.min.js"></script>

        <!--Javascript-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="index.js" defer></script>

        <title>Assignment 9: Chat System</title>

        <?php

            //For the database connection.
            $server = 'localhost';
            $database = 'chatDatabase';
            $conn = new mysqli($server, "root", "" , $database);
            if ($conn->connect_error)
            {
              die("Connection failed: " . $conn->connect_error);
            }

            else
            {
              //echo "CONNECTED SUCCESSFULLY!";
            }

        ?>

    </head>

    <!--The function 'message()' called when the page is loaded.-->
    <body onload='display_message()'>

      <h2>CHAT SYSTEM</h2>

      <!--chatArea and Php-->
      <div class="chatSystem">

        <!--chatId and Form-->
        <div class="chatArea">

          <div id="chatId">
            <!--Container to display the chat of the user.-->
          </div>

                <!--User uses a form to chat.-->
                <form action="index.php" method="post">

                    <input type="text" class="username" name="username" placeholder="Name of the User...">
                    <br>

                    <textarea type="text" class="message" name="message" placeholder="Type a Message..."></textarea>
                    <br>
                    <button type="submit" class="button" name="submit" value="submit">Send Message</button>

                </form>

          </div>
          <!--End of chatId-->

          <?php

                $server = 'localhost';
                $database = 'chatDatabase';
                $conn = new mysqli($server, "root", "" , $database);
                if ($conn->connect_error)
                {
                    die("Connection failed: " . $conn->connect_error);
                }

                else
                {
                    //echo "CONNECTED SUCCESSFULLY!";
                }

                if(isset($_POST['submit']))
                {

                    $username = $_POST["username"];
                    $message = $_POST["message"];

                    //Using stored procedure to insert 'username' and 'message' into the database.
                    $result = $conn->query("call store_message(\"$username\" , \"$message\" )");

                  }

          ?>

        </div>
        <!--End of chatSystem-->

     </body>

 </html>
