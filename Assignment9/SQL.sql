CREATE DATABASE chatDatabase;

CREATE TABLE chatTable (

  id                           INT                AUTO_INCREMENT,
  username                  VARCHAR(60)                         ,
  message                   VARCHAR(250)                        ,
  messageTime                TIMESTAMP                          ,
  PRIMARY KEY(id)

);

DELIMITER //
CREATE PROCEDURE store_chatTable()
BEGIN
SELECT *
FROM  chatTable;
END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE store_message
( IN user VARCHAR(60) ,
  IN msg VARCHAR(250) )
 BEGIN
 INSERT INTO  chatTable( username  , message )   VALUES   (user , msg);
 END//
DELIMITER ;
