<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="icon" href="icon.jpg" type="image">
  <link rel="stylesheet/less" type="text/css" href="contactPage.less" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <?php

     require_once 'sqlhelper.php';
     require_once './vendor/autoload.php';
     $loader = new Twig_Loader_Filesystem('./templates');
     $twig = new Twig_Environment($loader);

      //SQL SetUp.
      $conn = connectToMyDatabase();

      //Displaying the image and the heading.
      echo $twig->render('heading.twig.html',array(
      'heading' => 'FireArms' ));

      //Displaying the navigation menu.
      echo $twig->render('menu.twig.html',array(
      'link1' => 'homePage.php' ,
      'link2' => 'featuredPage.php' ,
      'link3' => 'firearmsPage.php' ,
      'link4' => 'toolsPage.php' ,
      'link5' => 'targetsPage.php' ,
      'link6' => '#' ,

      ));

   ?>

    <main>

      <div class="about_me">

        <h2>About Me</h2>
        <img src="characterImage.jpg" alt="Tequila Yuen Ho-yan">
        <p class="name"><strong>Tequila Yuen Ho-yan</strong></p>
        <p>Inspector with the <strong>Hong Kong Police Force</strong></p>
        <p>Mai Shun Industrial Building Block <br> 18-24 Kwai Cheong Rd. <br> KWAI CHUNG <br> NEW TERRITORIES <br> HONG KONG</p>
        <p>Tel: 654-334-1896</p>
        <p>Email: yuenhoyan@gmail.com</p>
        <p><i class="fab fa-facebook"></i>Yuen Ho-yan</p>
        <p><i class="fab fa-instagram"></i>hoyan_yuen</p>

      </div>

      <div class="contact_me">

        <h2>Contact Me</h2>
        <form action="http://webdevbasics.net/scripts/demo.php">

          <label for="name">Name</label>

          <input type="text" id="name" placeholder="Full name..." >

          <br>

          <label for="telephone">Telephone</label>

          <input type="tel" id="tel" placeholder="Telephone number..." >

          <br>

          <label for="email">E-mail</label>

          <input type="text" id="email" placeholder="E-mail address..." >

          <br>

          <label for="subject">Subject</label>

          <input type="text" id="subject" placeholder="Subject...">

          <br>

          <label for="message">Message</label>

          <textarea id="message" name="message" placeholder="Message..." ></textarea>

          <br>

          <button type="send">Send</button>

        </form>

      </div>

    </main>

    <?php

        echo $twig->render('footer.twig.html',array(
        'sign' => '@' ,
        'year' => '2018' ,
        'name' => 'Tequila Yuen Ho-yan' ,
        'text' => 'All right reserved' ,

        ));

        $conn->close();

     ?>

  </body>

</html>
