<?php
    require_once 'sqlhelper.php';
?>

<!DOCTYPE html>

<html>

<head>

    <title>Search results</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="search.less"/>

</head>

<body>

  <?php
     require_once 'sqlhelper.php';
     require_once './vendor/autoload.php';
     $loader = new Twig_Loader_Filesystem('./templates');
     $twig = new Twig_Environment($loader);

      //SQL SetUp.
      $conn = connectToMyDatabase();

      //Displaying the image and the heading.
      echo $twig->render('heading.twig.html',array(
      'heading' => 'FireArms' ));

      //Displaying the navigation menu.
      echo $twig->render('menu.twig.html',array(
      'link1' => 'homePage.php' ,
      'link2' => 'featuredPage.php' ,
      'link3' => 'firearmsPage.php' ,
      'link4' => 'toolsPage.php' ,
      'link5' => 'targetsPage.php' ,
      'link6' => 'contactPage.php' ,
      ));
    ?>

    <?php

        $conn = connectToMyDatabase();

        //Stores the type of gun being selected by the user in the search box;
        $query = $_GET['query'];

    ?>

    <div class="flex">

      <?php

          if($query == "handguns")
          {
              echo "<h2>Handguns</h2>";
              $result = $conn->query("call get_handguns()");
              while($row = $result->fetch_assoc())
              {
                $Name = $row["Name"];
                $url = $row["Image"];
                $Price = $row["Price"];
                echo "<div class='handguns'>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
              }
          }

          else if($query == "shotguns")
          {
              echo "<h2>Shotguns</h2>";
              $result = $conn->query("call get_shotguns()");
              while($row = $result->fetch_assoc())
              {
               $Name = $row["Name"];
               $url = $row["Image"];
               $Price = $row["Price"];
               echo "<div class='shotguns'>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
             }
          }

          else if($query == "pistols")
          {
              echo "<h2>Pistols</h2>";
              $result = $conn->query("call get_pistols()");
              while($row = $result->fetch_assoc())
              {
               $Name = $row["Name"];
               $url = $row["Image"];
               $Price = $row["Price"];
               echo "<div class='pistols'>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
             }

          }

          else if($query == "rifles")
          {
              echo "<h2>Rifles</h2>";
              $result = $conn->query("call get_rifles()");

              while($row = $result->fetch_assoc())
              {
               $Name = $row["Name"];
               $url = $row["Image"];
               $Price = $row["Price"];
               echo "<div class='rifles'>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
             }
          }

          else
          {
              $result = "Result is unavailable";
          }

        ?>

      </div>

        <?php
             echo $twig->render('footer.twig.html',array(
             'sign' => '@' ,
             'year' => '2018' ,
             'name' => 'Sukhpreet Kaur Sandhu' ,
             'text' => 'All right reserved' ,
             ));
             $conn->close();
        ?>

</body>
</html>
