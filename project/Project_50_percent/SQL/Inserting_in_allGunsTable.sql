
INSERT INTO allGuns(      Name   ,   Image   ,  Price   , Type    )   VALUES   ('Smith and Wesson 1911 Government 45 Auto', 'Images/smith.jpg' ,  '1379.99'    ,    'handguns'    );
INSERT INTO allGuns(      Name   ,   Image   ,  Price   , Type    )   VALUES   ('Browning 1911-380 Black Label Medallion Pro', 'Images/browningBlack.jpg' ,  '629.99'  ,  'handguns' );
INSERT INTO allGuns(      Name   ,   Image   ,  Price   , Type    )   VALUES   ('Ruger Mark IV Target .22LR Stainless', 'Images/ruger.jpg' ,  '1029.99' ,     'handguns' );
INSERT INTO allGuns(      Name   ,   Image   ,  Price   , Type    )   VALUES   ('SIG Sauer P226 RX 9mm Luger', 'Images/sig.jpg' ,  '1729.99' ,  'handguns' );
INSERT INTO allGuns(      Name   ,   Image   ,  Price   , Type    )   VALUES   ('Walther PPQ M2B 9mm Luger 5" Barrel ', 'Images/walther.jpg' ,  '1929.99' ,   'handguns' );
INSERT INTO allGuns(      Name   ,   Image   ,  Price    , Type   )   VALUES   ('A. Uberti 1873 CATTLEMAN CHISHOLM NM .45lc', 'Images/uberti.jpg' ,  '1379.99' ,  'handguns' );
INSERT INTO allGuns(      Name   ,   Image   ,  Price    , Type   )   VALUES   ('Agency Arms Custom Glock 17 9mm Luger', 'Images/agency.jpg' ,  '1229.99'  ,  'handguns' );
INSERT INTO allGuns(      Name   ,   Image   ,  Price    , Type   )   VALUES   ('Alfa Model 251 .22LR', 'Images/alfaModel.jpg' ,  '1449.99'   , 'handguns' );
INSERT INTO allGuns(      Name   ,   Image   ,  Price    , Type   )   VALUES   ('Alfa Proj 2361 .22 LR/.22 WMR 6" Barrel Stainless', 'Images/alfaProjBarrel.jpg' ,  '719.99'  ,      'handguns' );
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Alfa Proj 3561 .357 Magnum 6" Barrel', 'Images/alfaProjMagnum.jpg' ,  '619.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Alfa Proj 9251 Classic 9mm Luger 4.5" Blued', 'Images/alfaProjLuger.jpg' ,  '779.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Arminius WSA, 6 3/4" .22LR', 'Images/arminius.jpg' ,  '469.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price    , Type   )   VALUES   ('Arsenal Firearms Strike One 9mm', 'Images/arsenalStrikeOne.jpg' ,  '449.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price   , Type    )   VALUES   ('Arsenal Firearms Strike One Black/Tan 9mm', 'Images/arsenalStrikeOneBlack.jpg' ,  '1529.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Beretta 90-Two 9mm Para', 'Images/beretta.jpg' ,  '1729.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Boberg Arms XR45-S Onyx .45ACP', 'Images/boberg45.jpg' ,  '1929.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Boberg Arms XR9-S Onyx 9mm', 'Images/boberg9.jpg' ,  '4449.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Bond Arms Snake Slayer .45/410', 'Images/bondArms.jpg' ,  '1929.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Browning Buck Mark Challenge Rosewood .22LR', 'Images/browningBuck.jpg' ,  '1529.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('FNH FNX-45 Tactical .45ACP', 'Images/tactical.jpg' ,  '1029.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Cabot 1911 - S100 .45ACP Checkered Walnut Grips', 'Images/cabot.jpg' ,  '1399.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price    , Type   )   VALUES   ('Canuck HP 9mm Luger', 'Images/canuck.jpg' ,  '1249.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price    , Type   )   VALUES   ('Desert Eagle Brushed Chrome .44MAG', 'Images/desert.jpg' ,  '1629.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Century Arms Canik - TP9SF 9mm Luger', 'Images/century.jpg' ,  '449.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Girsan Yavuz 16 Compact Black and White 9mm', 'Images/girsan.jpg' ,  '1729.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price    , Type    )   VALUES   ('Colt 1911 A1 Gold Cup .22LR', 'Images/colt.jpg' ,  '1529.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price   , Type    )   VALUES   ('Dan Wesson Pointman 9 9mm', 'Images/dan.jpg' ,  '1529.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price    , Type   )   VALUES   ('Ed Brown Custom Special Forces Molon Labe 1911 ', 'Images/edBrown.jpg' ,  '449.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price    , Type   )   VALUES   ('Wilson Combat CQB Elite .45ACP', 'Images/WilsonCombat.jpg' ,  '1529.99',      'handguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price      ,Type )   VALUES   ('Zastava M57 7.62x25', 'Images/Zastava.jpg' ,  '1399.99',      'handguns');


INSERT INTO allGuns(       Name   ,   Image   ,  Price    , Type   )   VALUES   ('Remington 11-87 Sportsman Field 12 GA 28" ', 'Images/remington28.jpg' ,  '899.99',      'shotguns');
INSERT INTO allGuns(       Name   ,   Image   ,  Price     , Type  )   VALUES   ('Benelli Super Black Eagle II 28" Synthetic 12GA', 'Images/benelliBlack.jpg' ,  '2139.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price     , Type  )   VALUES   ('Armed Sash Single Shot .410GA with Chokes', 'Images/sash.jpg' ,  '209.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price    , Type   )   VALUES   ('Boito SxS 20 GA 28" Barrels', 'Images/boito.jpg' ,  '589.99',      'shotguns');
INSERT INTO allGuns(         Name   ,   Image   ,  Price    , Type   )   VALUES   ('Canuck Defender 12 GA 14" Barrel', 'Images/canuckshot.jpg' ,  '1919.99',      'shotguns');
INSERT INTO allGuns(          Name   ,   Image   ,  Price    , Type   )   VALUES   ('Mossberg 500 Home Security .410GA', 'Images/mossberg500.jpg' ,  '549.99',      'shotguns');
INSERT INTO allGuns(     Name   ,   Image   ,  Price   , Type    )   VALUES   ('Remington 11-87 Sportsman 12 Ga', 'Images/remington.jpg' ,  '769.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price     , Type  )   VALUES   ('Stoeger Condor Competition O/U 12 GA', 'Images/stoeger.jpg' ,  '999.99',      'shotguns');
INSERT INTO allGuns(       Name   ,   Image   ,  Price    , Type   )   VALUES   ('Winchester SX4 Waterfowl Hunter 12 GA 28" Barrel ', 'Images/winchester.jpg' ,  '1239.99',      'shotguns');
INSERT INTO allGuns(          Name   ,   Image   ,  Price  , Type     )   VALUES   ('Yildiz TK 12 GA Shotgun', 'Images/yildiz.jpg' ,  '349.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price    , Type   )   VALUES   ('Chiappa Single Badger .410 Bore', 'Images/chiappa.jpg' ,  '1299.99',      'shotguns');
INSERT INTO allGuns(           Name   ,   Image   ,  Price   , Type    )   VALUES   ('Browning Maxus 12GA 28" Barrel - Realtree Max5 DT', 'Images/maxus.jpg' ,  '1899.99',      'shotguns');
INSERT INTO allGuns(       Name   ,   Image   ,  Price     , Type  )   VALUES   ('Derya MK-12 AS-109B 12 GA Spitfire Distressed Tan', 'Images/deryaTan.jpg' ,  '1099.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price    , Type   )   VALUES   ('Benelli Nova Tactical 12 GA 18.5"', 'Images/benelliNova.jpg' ,  '679.99',      'shotguns');
INSERT INTO allGuns(          Name   ,   Image   ,  Price     , Type  )   VALUES   ('Derya MK-12 AS-103SF 12 GA', 'Images/derya.jpg' ,  '999.99',      'shotguns');
INSERT INTO allGuns(         Name   ,   Image   ,  Price     , Type  )   VALUES   ('Remington Versamax Waterfowl Hunter 12ga', 'Images/remingtonHunter.jpg' ,  '1299.99',      'shotguns');
INSERT INTO allGuns(          Name   ,   Image   ,  Price   , Type      )   VALUES   ('Winchester Super X Marine Defender 12 Gauge', 'Images/winchesterSuper.jpg' ,  '459.99',      'shotguns');
INSERT INTO allGuns(         Name   ,   Image   ,  Price     , Type  )   VALUES   ('Remington Versamax Tactical 12GA', 'Images/remingtonVersamax.jpg' ,  '1999.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price   , Type    )   VALUES   ('Remington 870 Police 12ga 18" Bead Sights Wood ', 'Images/remingtonPolice.jpg' ,  '449.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price     , Type  )   VALUES   ('Mossberg 930 Tactical 12ga with XS Express Sights', 'Images/mossberg930.jpg' ,  '1129.99',      'shotguns');
INSERT INTO allGuns(           Name   ,   Image   ,  Price     , Type  )   VALUES   ('Benelli SuperNova Tactical 14" Entry 12GA', 'Images/benelliSuperNova.jpg' ,  '679.99',      'shotguns');
INSERT INTO allGuns(          Name   ,   Image   ,  Price     , Type  )   VALUES   ('Boito Luxe SxS 12 GA', 'Images/boitoLuxe.jpg' ,  '1099.99',      'shotguns');
INSERT INTO allGuns(       Name   ,   Image   ,  Price     , Type  )   VALUES   ('Stoeger Condor Field O/U .410 Bore', 'Images/stoegerBore.jpg' ,  '699.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price     , Type  )   VALUES   ('Boito Reuna AE 12 GA', 'Images/boitoReuna.jpg' ,  '1459.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price     , Type  )   VALUES   ('Browning A5 Stalker 28" 12GA', 'Images/browingStalker.jpg' ,  '669.99',      'shotguns');
INSERT INTO allGuns(          Name   ,   Image   ,  Price    , Type   )   VALUES   ('Daniel Defense M4 V7 Pro 5.56 NATO 18" Barre', 'Images/daniel.jpg' ,  '1129.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price      , Type )   VALUES   ('Browning Maxus Wicked Wing 12 GA 28" Barrel ', 'Images/wing.jpg' ,  '1349.99',      'shotguns');
INSERT INTO allGuns(        Name   ,   Image   ,  Price    , Type   )   VALUES   ('Boito Hiker 12G 12"', 'Images/hiker.jpg' ,  '1439.99',      'shotguns');
INSERT INTO allGuns(           Name   ,   Image   ,  Price     , Type  )   VALUES   ('Sauer 404 Elegance .300 Win.Mag.', 'Images/sauer.jpg' ,  '1029.99',      'shotguns');
INSERT INTO allGuns(      Name   ,   Image   ,  Price     , Type  )   VALUES   ('Derya MK-12 AS-102S 12 GA', 'Images/deryaMK.jpg' ,  '1229.99',      'shotguns');



INSERT INTO allGuns(        Name   ,   Image   ,  Price   , Type    )   VALUES   ('2mm Kolibri' , 'Images/kolibri.jpg' ,  '1429.99'    ,      'pistols');
INSERT INTO allGuns(         Name   ,   Image   ,  Price    , Type   )   VALUES   ('ALFA Defender' , 'Images/defender.jpg' ,  '2449.99'  ,      'pistols');
INSERT INTO allGuns(     Name   ,   Image   ,  Price     , Type  )   VALUES   ('AMT Backup' , 'Images/backup.jpg' ,  '699.99'  ,      'pistols');
INSERT INTO allGuns(         Name   ,   Image   ,  Price    , Type   )   VALUES   ('Armatix iP' , 'Images/armatix.jpg' ,  '1229.99'  ,      'pistols');
INSERT INTO allGuns(        Name   ,   Image   ,  Price     , Type  )   VALUES   ('Arsenal Firearms AF1 "Strike One"' , 'Images/arsenal.jpg' ,  '1024.99'  ,      'pistols');
INSERT INTO allGuns(        Name   ,   Image   ,  Price     , Type  )   VALUES   ('ASP pistol' , 'Images/asp.jpg' ,  '1999.99'  ,      'pistols');
INSERT INTO allGuns(         Name   ,   Image   ,  Price     , Type  )   VALUES   ('Beholla pistol' , 'Images/beholla.jpg' ,  '1299.99'  ,      'pistols');
INSERT INTO allGuns(        Name   ,   Image   ,  Price     , Type  )   VALUES   ('Benelli MP 95E' , 'Images/benelliMP.jpg' ,  '1459.99'  ,      'pistols');
INSERT INTO allGuns(        Name   ,   Image   ,  Price    , Type   )   VALUES   ('Beretta Pico' , 'Images/pico.jpg' ,  '1499.99'  ,      'pistols');
INSERT INTO allGuns(         Name   ,   Image   ,  Price    , Type   )   VALUES   ('Beretta U22 Neos' , 'Images/neos.jpg' ,  '1439.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price     , Type  )   VALUES   ('Browning BDA' , 'Images/BDA.jpg' ,  '1299.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price    , Type   )   VALUES   ('BUL M-5' , 'Images/BULM.jpg' ,  '999.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price     , Type  )   VALUES   ('Caracal pistol' , 'Images/caracal.jpg' ,  '2149.99'  ,      'pistols');
INSERT INTO allGuns(         Name   ,   Image   ,  Price     , Type  )   VALUES   ('Colt Delta Elite' , 'Images/elite.jpg' ,  '1029.99'  ,      'pistols');
INSERT INTO allGuns(           Name   ,   Image   ,  Price     , Type  )   VALUES   ('Daewoo Precision Industries K5' , 'Images/K5.jpg' ,  '1499.99'  ,      'pistols');
INSERT INTO allGuns(      Name   ,   Image   ,  Price      , Type )   VALUES   ('FN Model 1903' , 'Images/FNModel.jpg' ,  '699.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price      , Type )   VALUES   ('Gaztanaga Destroyer' , 'Images/gaztanaga.jpg' ,  '1299.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price     , Type  )   VALUES   ('GSh-18' , 'Images/GSh.jpg' ,  '1029.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price     , Type  )   VALUES   ('Heckler & Koch HK4' , 'Images/HK4.jpg' ,  '1129.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price     , Type  )   VALUES   ('HS2000' , 'Images/HS2000.jpg' ,  '1999.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price      , Type )   VALUES   ('Jericho 941' , 'Images/jericho.jpg' ,  '2449.99'  ,      'pistols');
INSERT INTO allGuns(         Name   ,   Image   ,  Price     , Type  )   VALUES   ('OTs-02 Kiparis' , 'Images/kiparis.jpg' ,  '1029.99'  ,      'pistols');
INSERT INTO allGuns(         Name   ,   Image   ,  Price      , Type )   VALUES   ('Para-Ordnance P14-45' , 'Images/ordnance.jpg' ,  '1349.99'  ,      'pistols');
INSERT INTO allGuns(        Name   ,   Image   ,  Price     , Type  )   VALUES   ('Rohrbaugh R9' , 'Images/R9.jpg' ,  '669.99'  ,      'pistols');
INSERT INTO allGuns(     Name   ,   Image   ,  Price      , Type )   VALUES   ('Schwarzlose Model 1898' , 'Images/schwarzlose.jpg' ,  '1029.99'  ,      'pistols');
INSERT INTO allGuns(        Name   ,   Image   ,  Price      , Type )   VALUES   ('Semmerling LM4' , 'Images/semmerling.jpg' ,  '1439.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price     , Type  )   VALUES   ('SIG P210' , 'Images/SIGP210.jpg' ,  '1299.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price      , Type )   VALUES   ('SP-21 Barak' , 'Images/barak.jpg' ,  '1399.99'  ,      'pistols');
INSERT INTO allGuns(          Name   ,   Image   ,  Price      , Type )   VALUES   ('Stechkin APS' , 'Images/APS.jpg' ,  '1799.99'  ,      'pistols');
INSERT INTO allGuns(        Name   ,   Image   ,  Price      , Type )   VALUES   ('Type 64 pistol' , 'Images/type64.jpg' ,  '899.99'  ,      'pistols');

INSERT INTO allGuns(        Name   ,   Image   ,  Price      , Type )   VALUES   ('35M rifle' , 'Images/35M.jpg' ,  '1299.99'  ,      'rifles');
INSERT INTO allGuns(          Name   ,   Image   ,  Price    , Type   )   VALUES   ('Albini-Braendlin rifle' , 'Images/albini.jpg' ,  '1799.99' ,      'rifles');
INSERT INTO allGuns(         Name   ,   Image   ,  Price     , Type  )   VALUES   ('Arisaka' , 'Images/arisaka.jpg' ,  '899.99' ,      'rifles');
INSERT INTO allGuns(           Name   ,   Image   ,  Price     , Type  )   VALUES   ('Armalite AR-30' , 'Images/armalite.jpg' ,  '1349.99' ,      'rifles');
INSERT INTO allGuns(           Name   ,   Image   ,  Price     , Type  )   VALUES   ('Berkut rifle' , 'Images/berkut.jpg' ,  '1129.99' ,      'rifles');
INSERT INTO allGuns(            Name   ,   Image   ,  Price    , Type   )   VALUES   ('Blaser R8	' , 'Images/blaser.jpg' ,  '1029.99' ,      'rifles');
INSERT INTO allGuns(             Name   ,   Image   ,  Price    , Type   )   VALUES   ('C12A1' , 'Images/C12A1.jpg' ,  '1129.99' ,      'rifles');
INSERT INTO allGuns(                Name   ,   Image   ,  Price     , Type  )   VALUES   ('BSA Autorifle' , 'Images/BSA.jpg' ,  '1439.99' ,      'rifles');
INSERT INTO allGuns(               Name   ,   Image   ,  Price     , Type  )   VALUES   ('Brunswick rifle' , 'Images/brunswick.jpg' ,  '2149.99' ,      'rifles');
INSERT INTO allGuns(            Name   ,   Image   ,  Price   , Type     )   VALUES   ('Chiang Kai-shek rifle' , 'Images/chiang.jpg' ,  '669.99' ,      'rifles');
INSERT INTO allGuns(             Name   ,   Image   ,  Price      , Type )   VALUES   ('CZ 455' , 'Images/CZ455.jpg' ,  '1439.99' ,      'rifles');
INSERT INTO allGuns(             Name   ,   Image   ,  Price     , Type  )   VALUES   ('Ferguson rifle' , 'Images/ferguson.jpg' ,  '2149.99' ,      'rifles');
INSERT INTO allGuns(           Name   ,   Image   ,  Price    , Type   )   VALUES   ('Vereinsgewehr 1857' , 'Images/vereinsgewehr.jpg' ,  '1459.99' ,      'rifles');
INSERT INTO allGuns(           Name   ,   Image   ,  Price     , Type  )   VALUES   ('General Liu rifle' , 'Images/liu.jpg' ,  '899.99' ,      'rifles');
INSERT INTO allGuns(               Name   ,   Image   ,  Price      , Type )   VALUES   ('Gewehr 98' , 'Images/gewehr.jpg' ,  '1449.99' ,      'rifles');
INSERT INTO allGuns(                 Name   ,   Image   ,  Price     , Type  )   VALUES   ('Harpers Ferry Model 1803' , 'Images/harpers.jpg' ,  '1649.99' ,      'rifles');
INSERT INTO allGuns(                Name   ,   Image   ,  Price     , Type  )   VALUES   ('Infanteriegewehr Modell 1842' , 'Images/infanteriegewehr.jpg' ,  '1849.99' ,      'rifles');
INSERT INTO allGuns(              Name   ,   Image   ,  Price     , Type  )   VALUES   ('Jarmann M1884' , 'Images/jarmann.jpg' ,  '1299.99' ,      'rifles');
INSERT INTO allGuns(              Name   ,   Image   ,  Price     , Type  )   VALUES   ('Jungle Carbine' , 'Images/jungleCarbine.jpg' ,  '2149.99' ,      'rifles');
INSERT INTO allGuns(               Name   ,   Image   ,  Price    , Type   )   VALUES   ('Karabiner 98k' , 'Images/karabiner.jpg' ,  '3349.99' ,      'rifles');
INSERT INTO allGuns(               Name   ,   Image   ,  Price      , Type )   VALUES   ('Kropatschek rifle' , 'Images/kropatschek.jpg' ,  '1549.99' ,      'rifles');
INSERT INTO allGuns(               Name   ,   Image   ,  Price   , Type    )   VALUES   ('M1870 Belgian Comblain' , 'Images/belgian.jpg' ,  '2249.99' ,      'rifles');
INSERT INTO allGuns(             Name   ,   Image   ,  Price      , Type )   VALUES   ('Lorenz rifle' , 'Images/lorenz.jpg' ,  '2349.99' ,      'rifles');
INSERT INTO allGuns(                Name   ,   Image   ,  Price     , Type  )   VALUES   ('M1917 Enfield' , 'Images/M1917.jpg' ,  '1179.99' ,      'rifles');
INSERT INTO allGuns(                 Name   ,   Image   ,  Price     , Type  )   VALUES   ('Mannlicher M1886' , 'Images/mannlicher.jpg' ,  '999.99' ,      'rifles');
INSERT INTO allGuns(               Name   ,   Image   ,  Price      , Type )   VALUES   ('Marlin Model 70P' , 'Images/marlin.jpg' ,  '1449.99' ,      'rifles');
INSERT INTO allGuns(                Name   ,   Image   ,  Price      , Type )   VALUES   ('Mauser-Koka' , 'Images/mauser.jpg' ,  '3249.99' ,      'rifles');
INSERT INTO allGuns(                Name   ,   Image   ,  Price      , Type )   VALUES   ('Snider-Enfield' , 'Images/snider.jpg' ,  '1459.99' ,      'rifles');
INSERT INTO allGuns(                Name   ,   Image   ,  Price      , Type )   VALUES   ('Springfield Model 1861' , 'Images/springfield.jpg' ,  '1429.99' ,      'rifles');
INSERT INTO allGuns(                Name   ,   Image   ,  Price      , Type )   VALUES   ('Thorneycroft carbine' , 'Images/thorneycroft.jpg' ,  '2999.99' ,      'rifles');