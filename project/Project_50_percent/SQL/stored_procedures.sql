DELIMITER //
CREATE PROCEDURE get_handguns()
BEGIN
  SELECT Name  ,  Image , Price 
  From handguns ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_shotguns()
BEGIN
  SELECT Name  ,  Image , Price 
  From shotguns ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pistols()
BEGIN
  SELECT Name  ,  Image , Price 
  From pistols ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_rifles()
BEGIN
  SELECT Name  ,  Image , Price 
  From rifles ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE guns_by_type
(IN tp VARCHAR(20))
BEGIN
select *
from allguns
WHERE allguns.Type = tp;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_allguns()
BEGIN
  SELECT *
  From allguns ;
END //
DELIMITER ;