<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="icon" href="icon.jpg" type="image">
  <link rel="stylesheet/less" type="text/css" href="homePage.less" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <?php

     require_once 'sqlhelper.php';
     require_once './vendor/autoload.php';
     $loader = new Twig_Loader_Filesystem('./templates');
     $twig = new Twig_Environment($loader);

      //SQL SetUp.
      $conn = connectToMyDatabase();


      //Displaying the image and the heading.
      echo $twig->render('heading.twig.html',array(
      'heading' => 'FireArms' ));

      //Displaying the navigation menu.
      echo $twig->render('menu.twig.html',array(
      'link1' => '#' ,
      'link2' => 'featuredPage.php' ,
      'link3' => 'firearmsPage.php' ,
      'link4' => 'toolsPage.php' ,
      'link5' => 'chatPage.php' ,
      'link6' => 'contactPage.php' ,
      'link7' =>  'loginAccount.php'

      ));


   ?>

   <div class="intro">FireArms’ give a broad selection of handguns, rifles, shotguns and pistols that can be bought online.
        Buy your next firearm here!</div>

    <?php

        echo $twig->render('footer.twig.html',array(
        'sign' => '@' ,
        'year' => '2018' ,
        'name' => 'Sukhpreet Kaur Sandhu' ,
        'text' => 'All right reserved' ,

        ));

        $conn->close();

     ?>

  </body>

</html>
