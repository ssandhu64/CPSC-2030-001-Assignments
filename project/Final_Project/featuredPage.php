<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="icon" href="icon.jpg" type="image">
  <link rel="stylesheet/less" type="text/css" href="featuredPage.less">
  <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="featuredPage.js" defer></script>

  <body>

  <?php
     require_once 'sqlhelper.php';
     require_once './vendor/autoload.php';
     $loader = new Twig_Loader_Filesystem('./templates');
     $twig = new Twig_Environment($loader);

      //SQL SetUp.
      $conn = connectToMyDatabase();

      //Displaying the image and the heading.
      echo $twig->render('heading.twig.html',array(
      'heading' => 'FireArms' ));

      //Displaying the navigation menu.
      echo $twig->render('menu.twig.html',array(
      'link1' => 'homePage.php' ,
      'link2' => '#' ,
      'link3' => 'firearmsPage.php' ,
      'link4' => 'toolsPage.php' ,
      'link5' => 'chatPage.php' ,
      'link6' => 'contactPage.php' ,
      'link7' =>  'loginAccount.php'
      ));
    ?>

    <div class="message1">
      Click on the Product to add To Favourite list.
    </div>

    <div class="message2">
      Hover on each item to view the details of each product.
    </div>

    <div class="message3">
      Click on the product to :
      <ul>
        <li>Add To Favourite list.</li>
        <li>View details of each product.</li>
      </ul>
    </div>

    <div class="message4">
      Click on the item in the favourite list to remove it.
    </div>

    <div class="container">


        <div class="product_list">
            <h2>Featured Products</h2>
        </div>

        <div class="addToFavourite">
            <h2>Favourite</h2>
            <p>5 Products Allowed.</p>
        </div>

        <div class="image_details">

            <div class="img">
              <h2>Image</h2>
              <img src="" class="image">

            </div>


            <div class="details">
                <h2>Details</h2>
                <div class="detail"><span>Name:         </span>    <label class="name">             </label></div>
                <div class="detail"><span>Type:         </span>    <label class="type">            </label></div>
                <div class="detail"><span>Price:        </span>    <label class="price">            </label></div>
                <div class="detail"><span>Action:       </span>    <label class="action">           </label></div>
                <div class="detail"><span>Weight:       </span>    <label class="weight">           </label></div>
                <div class="detail"><span>Barrel Length:</span>    <label class="barrel_length">    </label></div>
                <div class="detail"><span>Length:       </span>    <label class="length">           </label></div>
                <div class="detail"><span>Width:        </span>    <label class="width">            </label></div>
                <div class="detail"><span>Height:       </span>    <label class="height">           </label></div>
                <div class="detail"><span>Cartridge:    </span>    <label class="cartridge">        </label></div>
                <div class="detail"><span>Description:  </span>    <label class="description">      </label></div>
            </div>

        </div>


    </div>


    <?php
         echo $twig->render('footer.twig.html',array(
         'sign' => '@' ,
         'year' => '2018' ,
         'name' => 'Sukhpreet Kaur Sandhu' ,
         'text' => 'All right reserved' ,
         ));
         $conn->close();
     ?>

   </body>

</html>
