<!DOCTYPE html>
<html>

  <head>

     <meta charset="UTF-8">

     <!-- Style sheet -->
     <link rel="stylesheet/less" type="text/css" href="createAccount.less" />
     <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>

     <title>FireArms</title>

   </head>

  <body>

    <?php

          //For the database connection.
           $server = 'localhost';
           $database = 'account';
           $conn = new mysqli($server, "root", "" , $database);
           if ($conn->connect_error)
           {
              die("Connection failed: " . $conn->connect_error);
           }

           else
           {
             //echo "CONNECTED SUCCESSFULLY!";
           }

           if(isset($_POST['submit']))
           {
             $firstname = $_POST['firstname'];
             $lastname = $_POST['lastname'];
             $username = $_POST['username'];
             $password = $_POST['password'];

             $result = $conn->query("call store_userData(\" $firstname\"  , \" $lastname\" , \" $username\" , \" $password\")");

             if($result)
             {
               header("location:loginAccount.php");
             }

           }


     ?>

    <div class="create_account">

        <h2>Create Your User Account</h2>

        <form method="post">

              <label>Enter Your First Name</label>
              <input type="text" name="firstname" placeholder="firstname" required>
              <label>Enter Your Last Name</label>
              <input type="text" name="lastname" placeholder="lastname" required>
              <label>Enter A User Name</label>
              <input type="text" name="username" placeholder="username" required>
              <label>Choose a Password</label>
              <input type="password" name="password" placeholder="password" required>
              <button type="submit" class="button" name="submit" value="submit">Create Account</button>

        </form>

        <div class="loginLink">
            <a href="loginAccount.php">Already have an account?</a>
        </div>



  </div>


  </body>


</html>
