
CREATE TABLE handguns (
	Image 		VARCHAR(500) ,
	Name 		VARCHAR(400) 	 NOT NULL,
	Price                             DECIMAL(7 , 2),
	PRIMARY KEY ( Name)
);

CREATE TABLE pistols (
	Image 		VARCHAR(500) ,
	Name 		VARCHAR(400) 	 NOT NULL,
	Price                             DECIMAL(7 , 2),
	PRIMARY KEY ( Name)
);


CREATE TABLE shotguns (
	Image 		VARCHAR(500) ,
	Name 		VARCHAR(400) 	 NOT NULL,
	Price                             DECIMAL(7, 2),
	PRIMARY KEY ( Name)
);


CREATE TABLE rifles (
	Image 		VARCHAR(500) ,
	Name 		VARCHAR(400) 	 NOT NULL,
	Price                             DECIMAL(7 , 2),
	PRIMARY KEY ( Name)
);

CREATE TABLE allGuns (
	Image 		VARCHAR(500) ,
	Name 		VARCHAR(400) 	 NOT NULL,
	Price                             DECIMAL(7 , 2),
    	Type         VARCHAR(20),
	PRIMARY KEY ( Name)
);


CREATE TABLE tools (
	Image 		VARCHAR(500) ,
	Name 		VARCHAR(400) 	 NOT NULL,
	Price                             DECIMAL(7 , 2),
	Type         VARCHAR(60),
	PRIMARY KEY ( Name)
);


CREATE TABLE userAccount (
	id             INT           AUTO_INCREMENT,
  firstname      VARCHAR(50) ,
	lastname	   VARCHAR(50) ,
	username       VARCHAR(50) ,
	password       VARCHAR(50) ,
	PRIMARY KEY(id)
);
