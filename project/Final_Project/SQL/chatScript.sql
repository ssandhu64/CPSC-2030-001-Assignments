
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `chatFireArmsDatabase` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `chatFireArmsDatabase`;

DELIMITER $$
DROP PROCEDURE IF EXISTS `get_last_hour_user_messages`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_last_hour_user_messages` ()  NO SQL
select * from (select   *, TIMESTAMPDIFF(MINUTE,time,CURRENT_TIMESTAMP) as diff from messages
where TIMESTAMPDIFF(MINUTE, time, CURRENT_TIMESTAMP) < 60
ORDER BY idx DESC
LIMIT 10) as tb
ORDER BY idx ASC$$

DROP PROCEDURE IF EXISTS `get_latest_user_messages`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_latest_user_messages` (IN `latest_time` DATETIME)  NO SQL
select * from messages
WHERE time > latest_time
ORDER BY idx ASC$$

DROP PROCEDURE IF EXISTS `get_user_messages`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_messages` ()  NO SQL
select * from messages ORDER BY idx ASC$$

DROP PROCEDURE IF EXISTS `insert_user_message`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_user_message` (IN `user` TEXT, IN `msg` TEXT)  NO SQL
insert into messages (usr, message, time)
values( user, msg, CURRENT_TIMESTAMP)$$

DELIMITER ;


DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `idx` int(11) NOT NULL,
  `usr` text NOT NULL,
  `message` text NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `messages`
  ADD UNIQUE KEY `idx` (`idx`);


ALTER TABLE `messages`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
