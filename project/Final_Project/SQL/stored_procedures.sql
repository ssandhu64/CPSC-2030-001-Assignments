DELIMITER //
CREATE PROCEDURE get_handguns()
BEGIN
  SELECT Name  ,  Image , Price
  From handguns ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_shotguns()
BEGIN
  SELECT Name  ,  Image , Price
  From shotguns ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pistols()
BEGIN
  SELECT Name  ,  Image , Price
  From pistols ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_rifles()
BEGIN
  SELECT Name  ,  Image , Price
  From rifles ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE guns_by_type
(IN tp VARCHAR(20))
BEGIN
select *
from allguns
WHERE allguns.Type = tp;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_allguns()
BEGIN
  SELECT *
  From allguns ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE store_userData
(IN firstname VARCHAR(50) ,
 IN lastname  VARCHAR(50) ,
 IN username VARCHAR (50) ,
 IN password VARCHAR(50) )
 BEGIN
 INSERT INTO  userAccount(firstname , lastname , username , password )   VALUES   (firstname ,lastname , username , password );
 END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE match_userData
(IN username VARCHAR (50) ,
 IN password VARCHAR(50) )
 BEGIN
 SELECT *
 FROM userAccount
 WHERE username = userAccount.username AND password = userAccount.password;
 END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE handguns_parts()
BEGIN
  SELECT Name  ,  Image , Price
  From tools WHERE Type = 'Handgun Replacement Parts' ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE shotguns_parts()
BEGIN
  SELECT Name  ,  Image , Price
  From tools WHERE Type = 'Shotgun Replacement Parts' ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE rifles_parts()
BEGIN
  SELECT Name  ,  Image , Price
  From tools WHERE Type = 'Rifle Replacement Parts' ;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE pistols_parts()
BEGIN
  SELECT Name  ,  Image , Price
  From tools WHERE Type = 'Pistol Replacement Parts' ;
END //
DELIMITER ;
