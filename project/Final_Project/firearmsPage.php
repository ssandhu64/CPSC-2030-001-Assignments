<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="icon" href="icon.jpg" type="image">
  <link rel="stylesheet/less" type="text/css" href="firearmsPage.less">
  <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="firearmsPage.js" defer></script>

  <body>

  <?php
     require_once 'sqlhelper.php';
     require_once './vendor/autoload.php';
     $loader = new Twig_Loader_Filesystem('./templates');
     $twig = new Twig_Environment($loader);

      //SQL SetUp.
      $conn = connectToMyDatabase();

      //Displaying the image and the heading.
      echo $twig->render('heading.twig.html',array(
      'heading' => 'FireArms' ));

      //Displaying the navigation menu.
      echo $twig->render('menu.twig.html',array(
      'link1' => 'homePage.php' ,
      'link2' => 'featuredPage.php' ,
      'link3' => '#' ,
      'link4' => 'toolsPage.php' ,
      'link5' => 'chatPage.php' ,
      'link6' => 'contactPage.php' ,
      'link7' =>  'loginAccount.php'
      ));
    ?>

    <div class="message">

      Click On The Grey Search Bar Below To Expand It.

    </div>


      <div class="flex">

                <div class="sidebar">

                      <h3>Search By Type</h3>

                    <form action="search.php" method="GET">
                        <input class="textbox" type="text" name="query" />
                        <input class="button" type="submit" value="Search" />
                    </form>

                </div>

                <div class="right-column">

                      <div class="handguns">
                            <h2>Handguns</h2>
                            <?php
                                $conn = connectToMyDatabase();
                                $result = $conn->query("call get_handguns()");
                                if ($result)
                                {
                                  while($row = $result->fetch_assoc())
                                  {
                                      $Name = $row["Name"];
                                      $url = $row["Image"];
                                      $Price = $row["Price"];
                                      echo "<div>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
                                    }
                                  }
                            ?>
                      </div>    <!--End of the handguns column div-->

                      <div class="shotguns">
                            <h2>Shotguns</h2>
                            <?php
                                  $conn = connectToMyDatabase();
                                  $result = $conn->query("call get_shotguns()");
                                  if ($result)
                                  {
                                    while($row = $result->fetch_assoc())
                                    {
                                        $Name = $row["Name"];
                                        $url = $row["Image"];
                                        $Price = $row["Price"];
                                        echo "<div>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
                                      }
                                  }
                              ?>
                        </div>   <!--End of the shotguns column div-->

                        <div class="pistols">
                              <h2>Pistols</h2>
                              <?php
                                    $conn = connectToMyDatabase();
                                    $result = $conn->query("call get_pistols()");
                                    if ($result)
                                    {
                                      while($row = $result->fetch_assoc())
                                      {
                                          $Name = $row["Name"];
                                          $url = $row["Image"];
                                          $Price = $row["Price"];
                                          echo "<div>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
                                        }
                                    }
                                ?>
                        </div>  <!--End of the pistols column div-->

                        <div class="rifles">
                                <h2>Rifles</h2>
                                <?php
                                      $conn = connectToMyDatabase();
                                      $result = $conn->query("call get_rifles()");
                                      if ($result)
                                      {
                                        while($row = $result->fetch_assoc())
                                        {
                                            $Name = $row["Name"];
                                            $url = $row["Image"];
                                            $Price = $row["Price"];
                                            echo "<div>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
                                          }
                                      }
                                  ?>
                          </div>  <!--End of the rifles column div-->

                  </div> <!--End of the right column div-->





          </div> <!--End of the flex div-->

          <div class="backToTop">
            <a href="firearmsPage.php">Back to Top</a>
          </div>



   <?php
        echo $twig->render('footer.twig.html',array(
        'sign' => '@' ,
        'year' => '2018' ,
        'name' => 'Sukhpreet Kaur Sandhu' ,
        'text' => 'All right reserved' ,
        ));
        $conn->close();
   ?>

  </body>

</html>
