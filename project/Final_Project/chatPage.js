
window.onload = setup;

function setup()
{
    initial_user_messages();
    setupUIHandlers();
}


let lastIndex = 1;

let lastTime = (new Date()).toLocaleString();

function create_new_div(user_message)
{
    let messageDate = new Date(user_message.time);
    return `<div class="message" idx="${user_message.idx}">
       <div class="time">${messageDate.toLocaleTimeString()}</div>
       <div class="name">${user_message.usr}</div>
       <div class="message">${user_message.message}</div><br>
    </div>
    `
}

//Retrieving messages.
function requestingUserMessage(url, data)
{
    return $.ajax({
        method: "POST",
        url: url,
        data: data
    }).done(function (msg)
      {
        let messageArray = JSON.parse(msg);

        if (messageArray.length != 0)
        {
            let messages = $(".userMessages");

            for (let msg of messageArray)
            {
                messages.append($(create_new_div(msg)));
            }

            lastTime = messageArray[messageArray.length - 1].time;

            messages.scrollTop(messages[0].scrollHeight);

        }
    }).always(function ()
    {
        setTimeout(update_user_messages,500);
    })
}

//To get the first (initial) messages.
function initial_user_messages()
{
    return requestingUserMessage("chatApi/last10_user_messages.php", {});
}

//For the last message received, updating the user messages.
function update_user_messages() {
    return requestingUserMessage("chatApi/latest_user_messages.php", { time : lastTime });
}

function setupUIHandlers(){
    $("#button").click(function (event) {
        $.ajax({
            method: "POST",
            url: "chatApi/insert_user_messages.php",
            data: {
                user: $("#name").val(),
                message: $("#message").val()
            }
        })
    });
}
