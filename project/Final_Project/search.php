<?php
    require_once 'sqlhelper.php';
?>

<!DOCTYPE html>

<html>

<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="icon" href="icon.jpg" type="image">
  <link rel="stylesheet/less" type="text/css" href="search.less">
  <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>

<body>

  <?php
     require_once 'sqlhelper.php';
     require_once './vendor/autoload.php';
     $loader = new Twig_Loader_Filesystem('./templates');
     $twig = new Twig_Environment($loader);

      //SQL SetUp.
      $conn = connectToMyDatabase();

      //Displaying the image and the heading.
      echo $twig->render('heading.twig.html',array(
      'heading' => 'FireArms' ));

      //Displaying the navigation menu.
      echo $twig->render('menu.twig.html',array(
      'link1' => 'homePage.php' ,
      'link2' => 'featuredPage.php' ,
      'link3' => 'firearmsPage.php' ,
      'link4' => 'toolsPage.php' ,
      'link5' => 'chatPage.php' ,
      'link6' => 'contactPage.php' ,
      'link7' => 'loginAccount.php'
      ));
    ?>

    <?php

        $conn = connectToMyDatabase();

        //Stores the type of gun being selected by the user in the search box;
        $query = $_GET['query'];

    ?>

    <div class="flex">

      <?php

          //To display results for handguns.
          if($query == "handguns")
          {
              echo "<h2>Handguns</h2>";
              $result = $conn->query("call get_handguns()");
              while($row = $result->fetch_assoc())
              {
                $Name = $row["Name"];
                $url = $row["Image"];
                $Price = $row["Price"];
                echo "<div class='handguns'>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
              }
          }

          //To display results for shotguns.

          else if($query == "shotguns")
          {
              echo "<h2>Shotguns</h2>";
              $result = $conn->query("call get_shotguns()");
              while($row = $result->fetch_assoc())
              {
               $Name = $row["Name"];
               $url = $row["Image"];
               $Price = $row["Price"];
               echo "<div class='shotguns'>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
             }
          }

          //To display results for pistols.
          else if($query == "pistols")
          {
              echo "<h2>Pistols</h2>";
              $result = $conn->query("call get_pistols()");
              while($row = $result->fetch_assoc())
              {
               $Name = $row["Name"];
               $url = $row["Image"];
               $Price = $row["Price"];
               echo "<div class='pistols'>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
             }

          }

          //To displsy results for rifles.
          else if($query == "rifles")
          {
              echo "<h2>Rifles</h2>";
              $result = $conn->query("call get_rifles()");

              while($row = $result->fetch_assoc())
              {
               $Name = $row["Name"];
               $url = $row["Image"];
               $Price = $row["Price"];
               echo "<div class='rifles'>"."<img src= '$url'>"."<br>".$Name."<br>"."$".$Price."</div>";
             }
          }

          else
          {
              $result = "Result is unavailable";
          }

        ?>

      </div>

      <div class="backToTop">
        <a href="search.php">Back to Top</a>
      </div>

        <?php
             echo $twig->render('footer.twig.html',array(
             'sign' => '@' ,
             'year' => '2018' ,
             'name' => 'Sukhpreet Kaur Sandhu' ,
             'text' => 'All right reserved' ,
             ));
             $conn->close();
        ?>

</body>
</html>
