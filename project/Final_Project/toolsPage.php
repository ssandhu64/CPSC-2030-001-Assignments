<?php

      //Start the Session.
      session_start();

      //Declaring Session variable
      if(!isset($_SESSION["favouriteTools"])){
          $_SESSION["favouriteTools"] = array();
      }

      require_once 'sqlhelper.php';
      $conn = connectToMyDatabase();

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="icon" href="icon.jpg" type="image">
  <link rel="stylesheet/less" type="text/css" href="toolsPage.less">
  <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <body>

  <?php

         require_once 'sqlhelper.php';
         require_once './vendor/autoload.php';
         $loader = new Twig_Loader_Filesystem('./templates');
         $twig = new Twig_Environment($loader);

          //SQL SetUp.
          $conn = connectToMyDatabase();

          //Displaying the image and the heading.
          echo $twig->render('heading.twig.html',array(
          'heading' => 'FireArms' ));

          //Displaying the navigation menu.
          echo $twig->render('menu.twig.html',array(
          'link1' => 'homePage.php' ,
          'link2' => 'featuredPage.php' ,
          'link3' => 'firearmsPage.php' ,
          'link4' => '#' ,
          'link5' => 'chatPage.php' ,
          'link6' => 'contactPage.php' ,
          'link7' => 'loginAccount.php'
          ));

    ?>

    <div class="tools">

        <div class="right_column">

            <h3>Replacement Parts of Hanguns</h3>

            <div class="handguns_parts">

                <?php

                    $conn = connectToMyDatabase();
                    $result = $conn->query("call handguns_parts()");
                    if ($result)
                    {
                      while($row = $result->fetch_assoc())
                      {
                          $Name = "toolsPage.php?Name=".urlencode($row["Name"]);
                          $url = $row["Image"];
                          $Price = $row["Price"];
                          echo "<div>"."<img src= '$url'>"."<br>"."<a href= '$Name'>".$row["Name"]."</a> <br>"."$".$Price."</div>";
                      }
                    }

                ?>

           </div>

           <h3>Replacement Parts of Shotguns</h3>

           <div class="shotguns_parts">

               <?php
                   $conn = connectToMyDatabase();
                   $result = $conn->query("call shotguns_parts()");
                   if ($result)
                   {
                     while($row = $result->fetch_assoc())
                     {
                         $Name = "toolsPage.php?Name=".urlencode($row["Name"]);
                         $url = $row["Image"];
                         $Price = $row["Price"];
                         echo "<div>"."<img src= '$url'>"."<br>"."<a href= '$Name'>".$row["Name"]."</a> <br>"."$".$Price."</div>";
                     }
                   }

               ?>

           </div>

           <h3>Replacement Parts of Pistols</h3>

           <div class="pistols_parts">

               <?php
                   $conn = connectToMyDatabase();
                   $result = $conn->query("call pistols_parts()");
                   if ($result)
                   {
                     while($row = $result->fetch_assoc())
                     {
                         $Name = "toolsPage.php?Name=".urlencode($row["Name"]);
                         $url = $row["Image"];
                         $Price = $row["Price"];
                         echo "<div>"."<img src= '$url'>"."<br>"."<a href= '$Name'>".$row["Name"]."</a> <br>"."$".$Price."</div>";
                     }
                   }
               ?>

           </div>

           <h3>Replacement Parts of Rifles</h3>

           <div class="rifles_parts">

               <?php
                   $conn = connectToMyDatabase();
                   $result = $conn->query("call rifles_parts()");
                   if ($result)
                   {
                     while($row = $result->fetch_assoc())
                     {
                         $Name = "toolsPage.php?Name=".urlencode($row["Name"]);
                         $url = $row["Image"];
                         $Price = $row["Price"];
                         echo "<div>"."<img src= '$url'>"."<br>"."<a href= '$Name'>".$row["Name"]."</a> <br>"."$".$Price."</div>";
                     }
                   }

               ?>

           </div>


        </div>

        <div class="favourites">

            <h2>Click on the name of the tool to add to FAVOURITES.</h2>

            <ul>

                  <?php

                      if(isset($_GET["Name"]))
                      {
                         array_push($_SESSION["favouriteTools"],$_GET["Name"]);
                      }

                      foreach ($_SESSION["favouriteTools"] as $value)
                      {
                          echo "<li>".$value ."</li>";
                      }

                 ?>

            </ul>

        </div>

    </div>

    <div class="backToTop">
      <a href="toolsPage.php">Back to Top</a>
    </div>

    <?php
         echo $twig->render('footer.twig.html',array(
         'sign' => '@' ,
         'year' => '2018' ,
         'name' => 'Sukhpreet Kaur Sandhu' ,
         'text' => 'All right reserved' ,
         ));
         $conn->close();
    ?>

   </body>

 </html>
