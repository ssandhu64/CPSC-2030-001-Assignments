<?php

  require_once("chatDatabaseHelper.php");

  $conn = connectToDatabase();

  $user = $conn->escape_string($_POST["user"]);

  $message = $conn->escape_string($_POST["message"]);

  $query = "call insert_user_message('$user','$message')";

  $result = $conn->query($query);

  if($result)
  {
      echo json_encode(array("Success"=>"Message Inserted."));
  }
  else
  {
      echo json_encode(array("Error"=>"An unknown SQL Error."));
  }

?>
