<?php

  require_once("chatDatabaseHelper.php");

  $conn = connectToDatabase();

  $query = "";

  if(array_key_exists("time", $_POST))
  {
      $param = $conn->escape_string($_POST["time"]);
      $query = "call get_latest_user_messages('$param')";
  }

  $result = $conn->query($query);

  if($result)
  {
     echo json_encode($result->fetch_all( MYSQLI_ASSOC));
  }
  else
  {
      echo json_encode(array("Error"=>"Data could not be fetched."));
  }
  
?>
