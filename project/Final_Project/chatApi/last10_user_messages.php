<?php

  require_once("chatDatabaseHelper.php");

  $conn = connectToDatabase();

  $query = "call get_last_hour_user_messages()";

  $result = $conn->query($query);

  if($result)
  {
      echo json_encode($result->fetch_all( MYSQLI_ASSOC));
  }
  else
  {
      echo json_encode(array("Error"=>"Data cannot be fetched."));
  }

?>
