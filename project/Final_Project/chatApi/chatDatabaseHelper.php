<?php

  function clearConnection($mysql)
  {
      while($mysql->more_results())
      {
         $mysql->next_result();
         $mysql->use_result();
      }
  }

  function connectToDatabase()
  {
      $server = 'localhost';
      $username = 'CPSC2030';
      $password = 'CPSC2030';
      $database = 'chatFireArmsDatabase';
      return connect($server , $username , $password , $database);
  }

  function connect($server, $user, $password, $database)
  {
      $conn = new mysqli($server, $user, $password, $database);
      if($conn->connect_errno)
      {
         echo "Error: Failed to make a MySQL connection.\n";
         echo "Errno is: " . $conn->connect_errno . "\n";
         echo "Error is: " . $conn->connect_error . "\n";

         exit;
      }
      return $conn;
  }

?>
