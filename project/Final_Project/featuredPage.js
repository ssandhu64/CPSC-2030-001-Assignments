let featured_guns = [
    {
        src: "featuredImage/glock.jpg",
        name: "Glock 19",
        type: "Handgun",
        price: "$999.99",
        action: "Hybrid Action(Single Action and Double Action)",
        weight: "21.16oz",
        barrel_length: "102mm",
        length: "187mm",
        width: "0.32mm",
        height: "128mm",
        cartridge: "0.45 ACP",
        description: "Known for its reliability, the G19 is by far the most popular handgun in the United States. It is a striker fired polymer frame pistol that other handguns in the same class are compared to and sets the standard for this class of handgun. The gen 5 Glock 19 can be configured with upgrades to the slide, barrel and grip. It also has a rail that allows a flashlight or other accessories to be attached."
    } ,

    {
      src: "featuredImage/hk.jpg",
      name: "HK VP9",
      type: "Handgun",
      price: "$2245.99",
      action: "Short Recoil Operated",
      weight: "25.56 oz",
      barrel_length: "104mm",
      length: "186.5mm",
      width: "33.5mm",
      height: "137.5mm",
      cartridge: "0.40 S & W",
      description: "The Heckler & Koch VP is an excellent choice for a striker fired weapon. It offers interchangeable backstraps and grip panels and has to be one of the more comfortable and smoothest 9mm pistols on the market. Some of the features are a striker fired indicator, ambi controls, luminescent 3 - dot sights and a 5.4pound trigger."
    } ,

    {
      src: "featuredImage/sig.jpg",
      name: "Sig Sauer P320",
      type: "Handgun",
      price: "$4224.99",
      action: "Short Recoil Operated",
      weight: "29.4oz",
      barrel_length: "120mm",
      length: "203mm",
      width: "35.5mm",
      height: "140mm",
      cartridge: "0.56 ACP",
      description: "The Sig Sauer P320 has been chosen by the army as it’s new sidearm. It has a lot of variants so finding something that fits your requirements is made easier. The trigger breaks around 6.5 pounds allowing for precise and quick follow up shots. This is an excellent shooting pistol that is known for its ease of use, reliability, modularity and accuracy."
    } ,
    {
      src: "featuredImage/cobra.jpg",
      name: "Night Cobra",
      type: "Handgun",
      price: "$2224.99",
      action: "Double Action",
      weight: "24.4oz",
      barrel_length: "135mm",
      length: "200mm",
      width: "32.5mm",
      height: "130mm",
      cartridge: "0.26 ACP",
      description: "The Colt Cobra is now available in a carry-ready format. Matte black DLC (Diamond-Like Carbon) coating over Stainless Steel frame ensures maximum corrosion resistance and ultimate concealability. Double Action Only configuration retains the smooth trigger pull with a snag-free bobbed hammer design. Custom Colt G10 Grips provide a smooth drag free grip that won’t hang up on clothing or cover garments. The Cobra® is chambered in .38 Special and is +P capable."
    } ,

    {
      src: "featuredImage/ruger.jpg",
      name: "Ruger LC",
      type: "Pistol",
      price: "$549.99",
      action: "Single Strike Double Action Mechanism",
      weight: "9.4oz",
      barrel_length: "70mm",
      length: "131mm",
      width: "21mm",
      height: "91mm",
      cartridge: "0.380 ACP",
      description: "LCP stands for 'Lightweight Compact Pistol' and was designed in direct response to customer requests for a compact firearm for use by police as a back-up, and as a defensive handgun for civilian concealed carry needs.The pistol has a glass-filled nylon frame, a two-finger grip, and a through-hardened blued steel slide."
    } ,


    {
      src: "featuredImage/kolibri.jpg",
      name: "2mm Kolibri",
      type: "Pistol",
      price: "$749.99",
      action: "Single Action",
      weight: "7.6oz",
      barrel_length: "80mm",
      length: "125mm",
      width: "20mm",
      height: "112mm",
      cartridge: "2.7mm",
      description: "The 2mm Kolibri (also known as the 2.7mm Kolibri Car Pistol or 2.7×9mm Kolibri) is the smallest commercially available centerfire cartridge,[2] patented in 1910 and introduced in 1914 by Franz Pfannl, an Austrian watchmaker, with financial support from Georg Grabner. It was designed to accompany the Kolibri semi-auto pistol or single shot pistol, both marketed as self-defense weapons."
    } ,

    {
      src: "featuredImage/asp.jpg",
      name: "ASP Pistol",
      type: "Pistol",
      price: "$967.99",
      action: "Tilting Barrel",
      weight: "680gm",
      barrel_length: "82.5mm",
      length: "113.5mm",
      width: "0.45mm",
      height: "125mm",
      cartridge: "9*19mm Parabellum",
      description: "The ASP was a custom made handgun designed and built by Paris Theodore, owner of Seventrees, Ltd. a custom gun leather shop in New York City from the early 1970s to 1987. The ASP was based on the Smith & Wesson Model 39 pistol. The ASP featured clear Lexan grips allowing the shooter to see how much ammunition is left, a rounded hammer, hooked triggerguard and no front sight."
    } ,

    {
      src: "featuredImage/bayard.jpg",
      name: "Bayard 1908",
      type: "Pistol",
      price: "$999.99",
      action: "Blowback Action",
      weight: "465gm",
      barrel_length: "57mm",
      length: "126mm",
      width: "0.56mm",
      height: "130mm",
      cartridge: "0.32 ACP",
      description: "The Bayard 1908 is a semi-automatic pistol designed by Belgian Bernard Clarus in 1908 as a short-range self-defense handgun. The Bayard 1908 was sold on the civilian market, chambered in .25 ACP, .32 ACP, and .380 ACP. It was produced at the Belgian factory Anciens Etablissements Pieper from 1908 until the later 1930s."
    } ,

    {
      src: "featuredImage/armsel.jpg",
      name: "Armsel Striker",
      type: "Shotgun",
      price: "$1099.99",
      action: "Break Action",
      weight: "400gm",
      barrel_length: "57mm",
      length: "126mm",
      width: "0.56mm",
      height: "130mm",
      cartridge: "12 gauge",
      description: "The Armsel Striker also known as the Sentinel Arms Co Striker-12, Protecta and Protecta Bulldog is a 12-gauge shotgun with a revolving cylinder that was designed for riot control and combat."
    } ,

    {
      src: "featuredImage/benelli.jpg",
      name: "Benelli Supernova",
      type: "Shotgun",
      price: "$2399.99",
      action: "Bolt Action",
      weight: "540gm",
      barrel_length: "45mm",
      length: "134mm",
      width: "0.5mm",
      height: "145mm",
      cartridge: "12 gauge",
      description: "The Benelli Supernova is a pump action shotgun popular for hunting and self-defense, made by Italian firearm manufacturer Benelli Armi SpA. The Supernova's most innovative and distinguishing feature is a one-piece receiverand interchangeable stock with shim kit. It is known for its large trigger guard, extreme durability and reliability."
    } ,

    {
      src: "featuredImage/ithaca.jpg",
      name: "Ithaca 37",
      type: "Shotgun",
      price: "$1399.99",
      action: "Break Action",
      weight: "450gm",
      barrel_length: "35mm",
      length: "125mm",
      width: "0.56mm",
      height: "135mm",
      cartridge: "10 gauge",
      description: "The Ithaca 37 is a pump-action shotgun made in large numbers for the civilian, military, and police markets. It utilizes a novel combination ejection/loading port on the bottom of the gun which leaves the sides closed to the elements. Since shotshells load and eject from the bottom, operation of the gun is equally convenient for both right and left hand shooters. This makes the gun popular with left-handed shooters."
    } ,

    {
      src: "featuredImage/remington.jpg",
      name: "Remington Model 19",
      type: "Shotgun",
      price: "$1399.99",
      action: "Pump Action",
      weight: "375gm",
      barrel_length: "45mm",
      length: "155mm",
      width: "0.50mm",
      height: "155mm",
      catridge: "20 gauge",
      description: "The Model 17 was a trim, 20-gauge shotgun that served as the design basis for three highly successful shotguns: the Remington Model 31, the Ithaca 37 and the Browning BPS. Additionally, features of the Model 17 were also incorporated in the later Mossberg 500 and Remington 870."
    } ,

    {
      src: "featuredImage/armalite.jpg",
      name: "Armalite AR-5",
      type: "Rifle",
      price: "$2149.99",
      action: "Bolt Action",
      weight: "4kg",
      barrel_length: '14"',
      length: '20"',
      width: "0.65mm",
      height: "135mm",
      cartridge: "0.22 Hornet",
      description: "The ArmaLite AR-5 is a lightweight bolt-action rifle, chambered for the .22 Hornet cartridge, and adopted as the MA-1 aircrew survival rifle by the United States Air Force. It was developed by ArmaLite, a division of Fairchild Engine and Airplane Corporation in 1954."
    } ,

    {
      src: "featuredImage/brunswick.jpg",
      name: "Brunswick rifle",
      type: "Rifle",
      price: "$3349.99",
      action: "Caplock Action",
      weight: "4.5kg",
      barrel_length: '18"',
      length: '30"',
      width: "0.45mm",
      height: "130mm",
      cartridge: "Lead Ball",
      description: "The Brunswick rifle was a large calibre (.704) muzzle-loading percussion rifle manufactured for the British Army at the Royal Small Arms Factory at Enfield in the early 19th century."
    } ,

    {
      src: "featuredImage/howell.jpg",
      name: "Howell Automatic Rifle",
      type: "Rifle",
      price: "$2449.99",
      action: "Break Action",
      weight: "3.75kg",
      barrel_length: '15"',
      length: '27"',
      width: "0.55mm",
      height: "170mm",
      cartridge: "0.303 British",
      description: "The Howell Automatic Rifle is an automatic conversion for the Lee–Enfield rifle.[1] The weapon was reliable but unergonomic for the user as the force of the recoiling bolt interfered with handling. Similar conversions were the South African Rieder and Charlton of New Zealand origin which had full automatic capability."
    }
]

window.addEventListener("load",setUp);

function setUp()
{
    let  gun_list = document.querySelector(".container>.product_list");
    for(let featured_product of featured_guns)
    {
        let product = document.createElement("div");

        product.addEventListener("click", createFavouriteListAddListener(featured_product));

        product.addEventListener("mouseover", createUpdateDetailsListener(featured_product));

        product.innerHTML = featured_product.name;
        gun_list.appendChild(product);
    }
}


function updateDetails(product){
    document.querySelector(".image_details .image").src = product.src;
    document.querySelector(".image_details .details .name").innerHTML = product.name;
    document.querySelector(".image_details .details .type").innerHTML = product.type;
    document.querySelector(".image_details .details .price").innerHTML = product.price;
    document.querySelector(".image_details .details .action").innerHTML = product.action;
    document.querySelector(".image_details .details .weight").innerHTML = product.weight;
    document.querySelector(".image_details .details .barrel_length").innerHTML = product.barrel_length;
    document.querySelector(".image_details .details .length").innerHTML = product.length;
    document.querySelector(".image_details .details .width").innerHTML =product.width;
    document.querySelector(".image_details .details .height").innerHTML =product.height;
    document.querySelector(".image_details .details .cartridge").innerHTML =product.cartridge;
    document.querySelector(".image_details .details .description").innerHTML = product.description;
}

function createUpdateDetailsListener(product)
{
    return function(event)
    {
        updateDetails(product);
    }
}

function removeProduct(event)
{
    event.currentTarget.remove();
}

const maximum_products = 5;

function createFavouriteListAddListener(product)
{
    return function(event)
    {
      //Checking if member is already in unit list.
      let product_list = document.querySelectorAll(".addToFavourite div.product");

      if(product_list.length >= 5)
      {
          return;
      }
      for(let gun of product_list)
      {
          if(gun.innerHTML == product.name){
              return;
          }
      }
      //If not add new element to list.
      let gun = document.createElement("div");
      gun.classList.add("product")
      gun.addEventListener("click", removeProduct);
      gun.addEventListener("mouseover", createFavouriteListAddListener(product));
      gun.innerHTML = product.name;
      document.querySelector(".container>.addToFavourite").appendChild(gun);

    }
}
