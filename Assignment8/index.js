function resClick(event) {

    let menu = $(".menu");

    if (menu.hasClass("open")) {
        menu.removeClass("open");
    }

    else {
        menu.addClass("open");
    }

}

//ONCLICK FUNCTION FOR THE MENU HEADING.
$(".menu .item:last-of-type").click(resClick);


//ONCLICK FUNCTION FOR THE FIVE ITEMS OF THE MENU(EXCEPT FOR THE MENU AND BULLET).
$(".menu a").click(function() {

    let x = event.pageX;
    let y = event.pageY;

    //THE INITIAL POSITION OF THE BULLET IS AT (103px , 0)
    x = x-87;
    y = y-161;

    $(".menu .item:nth-last-child(2)").animate( { left:x+'px' ,   top: y+'px' } ,   1700  ).fadeOut();

});

function openLink (link)
{
    setTimeout( function() { window.location = link }, 2500 );
}
