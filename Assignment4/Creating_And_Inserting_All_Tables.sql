CREATE TABLE pokemon_table1 ( 
	NPNumber		 INT 		NOT NULL, 
	HPNumber                   INT                             NOT NULL,
	Image 		VARCHAR(500) ,
	Name 		VARCHAR(40) 	 NOT NULL,
	HP		INT, 
	Atk 		INT, 
	Def 		INT, 
	SAt 		INT, 
	SDf 		INT, 
	Spd		INT, 
	BST 		INT, 
	PRIMARY KEY ( Name) 
);

CREATE TABLE pokemon_table2( 
	Name 		VARCHAR(40) 	NOT NULL, 
	Type 		VARCHAR(20) 	NOT NULL, 
	FOREIGN KEY(Name) REFERENCES pokemon_table1(Name), 
	PRIMARY KEY(Name , Type)
);

CREATE TABLE type_strong( 
	Type		VARCHAR(40) 	NOT NULL, 
	StrongAgainst	VARCHAR(40) 	NOT NULL,
	PRIMARY KEY(Type , StrongAgainst)
);

CREATE TABLE type_weak( 
	Type		VARCHAR(40) 	NOT NULL, 
	WeakAgainst	VARCHAR(40) 	NOT NULL,
	PRIMARY KEY(Type , WeakAgainst)
);

CREATE TABLE type_resistant( 
	Type		VARCHAR(40) 	NOT NULL, 
	ResistantTo 	VARCHAR(40) 	NOT NULL,
	PRIMARY KEY(Type , ResistantTo)
);

CREATE TABLE type_vulnerable( 
	Type		VARCHAR(40) 	NOT NULL, 
	VulnerableTo	VARCHAR(40) 	NOT NULL,
	PRIMARY KEY(Type , VulnerableTo)
);

INSERT INTO pokemon_table1 (NPNumber,  HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('387', '0' , 'https://www.google.ca/search?q=turtwig&rlz=1C1EJFA_enCA780CA780&source=lnms&tbm=isch&sa=X&ved=0ahUKEwi8iL3Tq__dAhUYIzQIHfaIC4AQ_AUIDigB&biw=1242&bih=553#imgrc=_-5YgRh5MKmOvM:', 'Turtwig', '55', '68', '64', '45', '55', '31', '318');

INSERT INTO pokemon_table1 (NPNumber,  HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('388',  '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=ysG_W97uFde50PEP4euTkAU&q=grotle&oq=grotle&gs_l=img.3..0l10.1048603.1049985.0.1050325.8.7.1.0.0.0.139.679.5j2.7.0....0...1c.1.64.img..0.8.686...0i67k1.0.htUO_8jOJi8#imgrc=ZqNgZLlezOpycM:', 'Grotle', '75', '89', '85', '55', '65', '36', '405');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('389', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=58W_W5yaC8Gt0PEPv5az0Aw&q=torterra&oq=torterra&gs_l=img.3..0i67k1j0l9.17844.18808.0.19033.8.7.0.1.1.0.92.470.7.7.0....0...1c.1.64.img..0.8.476....0.uxEJHXEf25s#imgrc=iGJsDDZIcSaiAM:', 'Torterra', '95', '109', '105', '75', '85', '56', '525');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('390', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=_MW_W6evFbTl9APPlaa4Aw&q=chimchar&oq=chimchar&gs_l=img.3..0l10.15142.16017.0.16349.8.6.0.2.2.0.160.555.4j2.6.0....0...1c.1.64.img..0.8.576...0i67k1.0.wxynb--FJVU#imgrc=QaVwetf_MJ_XvM:', 'Chimchar', '44', '58', '44', '58', '44', '61', '309');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('391', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=Dsa_W8_5HLGU0PEPw_Cc8AM&q=monferno&oq=monferno&gs_l=img.3..0i67k1j0l9.21853.23445.0.24733.8.7.0.1.1.0.100.439.5j1.6.0....0...1c.1.64.img..1.7.440...0i10k1.0.LNV7OEcbNIM#imgrc=5Z4tjvhcD7E-_M:', 'Monferno', '64', '78', '52', '78', '52', '81', '405');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('392', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=Kca_W4qkCsHa9APY66GgBQ&q=infernape&oq=infernape&gs_l=img.3..0i67k1j0l9.19634.20708.0.20967.9.8.0.1.1.0.155.673.6j2.8.0....0...1c.1.64.img..0.9.680....0.5419d2jbe5k#imgrc=ITeYRZ_fGFE__M:', 'Infernape', '76', '104', '71', '104', '71', '108', '534');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('393',  '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=QMa_W6HUB9mx0PEP9LqLoAk&q=piplup&oq=piplup&gs_l=img.3..0l3j0i67k1j0l6.14461.15384.0.15708.6.5.0.1.1.0.83.340.5.5.0....0...1c.1.64.img..0.6.352....0.1meKvL8F9K4#imgrc=mkhxJgGc9Qoe-M:', 'Piplup', '53', '51', '53', '61', '56', '40', '314');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('394', '0' , 'https://www.google.ca/search?q=prinplup&rlz=1C1EJFA_enCA780CA780&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiDoZCJsP_dAhUzIjQIHb1_CwAQ_AUIDigB&biw=1242&bih=553#imgrc=20C1ZkhriOSvqM:', 'Prinplup', '64', '66', '68', '81', '76', '50', '405');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('395', '0' ,  'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=b8a_W4ufILXd9APHiKBg&q=empoleon&oq=empoleon&gs_l=img.3..0i67k1j0l9.13495.14707.0.15011.8.6.0.2.2.0.76.342.6.6.0....0...1c.1.64.img..0.8.360....0.EyGYfMdWnlg#imgrc=AqsfvI55YGnyBM:', 'Empoleon', '84', '86', '88', '111', '101', '60', '530');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('396', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=gMa_W-zLFpnD0PEP5pCXgAQ&q=starly&oq=star&gs_l=img.3.1.0i67k1l5j0l5.14013.15147.0.20027.4.4.0.0.0.0.92.296.4.4.0....0...1c.1.64.img..0.4.296....0.Bnowa08jFLY#imgrc=yT2ZORL14Cij2M:', 'Starly', '40', '55', '30', '30', '30', '60', '245');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('397', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=q8a_W5GhKtnL0PEPpuizgAs&q=starvia&oq=starvia&gs_l=img.3..0i7i30k1l10.1266.2731.0.3468.2.2.0.0.0.0.75.131.2.2.0....0...1c.1.64.img..0.2.127...0i10k1.0.i-bblp27Tpc#imgrc=jdR63Pf9mvGbBM:', 'Staravia', '55', '75', '50', '40', '40', '80', '340');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('398', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=sca_W6n3Dq-s0PEPgZ2s-Ak&q=Staraptor&oq=Staraptor&gs_l=img.3..0i67k1j0l2j0i67k1j0l6.2336658.2337551.0.2337951.2.1.0.1.1.0.64.64.1.1.0....0...1c.1.64.img..0.2.76....0.zXb7QJM_Bls#imgrc=jdUVPFNqH-DVYM:', 'Staraptor', '85', '120', '70', '50', '60', '100', '485');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('399', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=1M-_W6PsN__G0PEPqcOTiA8&q=bidoof&oq=bidoof&gs_l=img.3..0l10.16080.16785.0.16913.6.5.0.1.1.0.80.310.5.5.0....0...1c.1.64.img..0.6.318...0i67k1.0.TdwfEDrdxZg#imgrc=NwHbiDeYBFzeTM:', 'Bidoof', '59', '45', '40', '35', '40', '31', '250');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('400', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=6M-_W4bhDenD0PEPyO-36AM&q=bibarel&oq=bibarel&gs_l=img.3..0l6j0i67k1j0j0i30k1j0i5i30k1.14755.15678.0.15942.7.7.0.0.0.0.110.482.6j1.7.0....0...1c.1.64.img..0.7.478...0i10k1.0.0CzUas1_VYU#imgrc=5LPY7NZUtfte8M:', 'Bibarel', '79', '85', '60', '55', '60', '71', '410');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('401', '0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=H9C_W8PNGtO_0PEPnaCT8Ak&q=kricketot&oq=kricketot&gs_l=img.3..0l4j0i10k1l6.17273.17321.0.17461.2.2.0.0.0.0.124.200.1j1.2.0....0...1c.1.64.img..0.2.196...0i10i30k1j0i5i30k1.0.UvqMDg6rA_4#imgrc=AijQHGSFmpEtIM:', 'Kricketot', '37', '25', '41', '25', '41', '25', '194');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('402',  '0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=DdC_W7neE77D0PEPhoCt-Ag&q=kricketune&oq=kricketune&gs_l=img.3..0l10.14961.15704.0.16195.5.3.0.1.1.0.76.209.3.3.0....0...1c.1.64.img..1.4.217...0i67k1j0i10k1j0i10i30k1j0i5i30k1.0.exPui2_eCRY#imgrc=As1S6Cu4n1vlxM:', 'Kricketune', '77', '85', '51', '55', '51', '65', '384');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('403', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=MtC_W7PaKf3B0PEPg9OiuAY&q=shinx+pokemon&oq=shinx&gs_l=img.1.1.0l10.20822.21319.0.23189.5.5.0.0.0.0.140.404.4j1.5.0....0...1c.1.64.img..0.5.390...0i67k1.0.ZeZzIxR0VqU#imgrc=2FSALY3Wj9JbfM:', 'Shinx', '45', '65', '34', '40', '34', '45', '263');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('404','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=X9C_W_jYA-6x0PEPptuKsAE&q=luxio&oq=luxio&gs_l=img.3...0.0.0.9934.0.0.0.0.0.0.0.0..0.0....0...1c..64.img..0.0.0....0.Xpwpup1xxr4#imgrc=SJBN8nsAAcB-oM:', 'Luxio', '60', '85', '49', '60', '49', '60', '363');

INSERT INTO pokemon_table1 (NPNumber,  HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('405','0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=atC_W8jYO7O90PEPs9GnkAY&q=luxray+pokemon&oq=luxray&gs_l=img.1.1.0i67k1j0l2j0i67k1l3j0l4.24802.24915.0.27094.3.2.0.1.1.0.120.201.1j1.2.0....0...1c.1.64.img..1.2.93....0.xAIGYxEqDlU#imgrc=6o3xvu1Zw040JM:', 'Luxray', '80', '120', '79', '95', '79', '70', '523');

INSERT INTO pokemon_table1 (NPNumber, HPNumber, Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('406', '97', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=h9C_W4qxHbnN0PEP8ra_8Ag&q=budew&oq=budew&gs_l=img.3..0j0i67k1j0l2j0i67k1j0l5.16066.16863.0.17288.5.5.0.0.0.0.75.319.5.5.0....0...1c.1.64.img..0.5.316....0.7DCA5rsYuGU#imgrc=_UdbJM622LqxbM:', 'Budew', '40', '30', '35', '50', '70', '55', '280');

INSERT INTO pokemon_table1 (NPNumber, HPNumber, Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('407', '99', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=mtC_W7jbM4e50PEPj4i2sAo&q=roserade&oq=roserade&gs_l=img.3..0j0i67k1j0l8.17582.19101.0.19522.8.7.0.1.1.0.96.516.7.7.0....0...1c.1.64.img..0.8.524...0i10k1.0.rJf-w5C3fxk#imgrc=ZBig8m__4H1KaM:', 'Roserade', '60', '70', '65', '125', '105', '90', '515');


INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('408',  '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=sNC_W-LkDb3B0PEPhua62AE&q=cranidose&oq=cranidose&gs_l=img.3..0i10i24k1.13092.14113.0.14553.9.7.0.2.2.0.147.514.6j1.7.0....0...1c.1.64.img..0.9.521...0j0i67k1.0.Cc4MJyGDjHg#imgrc=KYmXKEoSsOFFUM:', 'Cranidos', '67', '125', '40', '30', '30', '58', '350');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('409', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=wNC_W4yrHK_E0PEPnd-lgAM&q=rampardose&oq=rampardose&gs_l=img.3..0i10i24k1.12523.14326.0.14716.10.9.0.1.1.0.80.587.9.9.0....0...1c.1.64.img..0.10.591...0j0i67k1.0.O9kpQMNb2x0#imgrc=HdCrmL0ZNIQ-1M:', 'Rampardos', '97', '165', '60', '65', '50', '58', '495');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('410','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=0NC_W-mqN96-0PEPtcWqwAo&q=shieldon&oq=shieldon&gs_l=img.3..0l10.12999.14044.0.14500.8.6.0.2.2.0.120.458.5j1.6.0....0...1c.1.64.img..0.8.483...0i67k1.0.lsKYHlaRy7k#imgrc=-x8dPCoVrTZgAM:', 'Shieldon', '30', '42', '118', '42', '88', '30', '350');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('411', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=4dC_W9KiEfPg9AOzl7jYCw&q=bastiodon&oq=bastiodon&gs_l=img.3..0l2j0i67k1j0l7.15934.17403.0.17636.9.8.0.1.1.0.96.552.8.8.0....0...1c.1.64.img..0.9.559....0.gWLA0eq0NPM#imgrc=4eu2e011gzCY1M:', 'Bastiodon', '60', '52', '168', '47', '138', '30', '495');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('412', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=9NC_W_b-LuTF0PEP-JuzkAg&q=burmy&oq=burmy&gs_l=img.3..0l10.21989.24357.0.24536.6.6.0.0.0.0.112.454.5j1.6.0....0...1c.1.64.img..0.6.446...0i67k1.0.xv32Do_fOBU#imgrc=HBjd4ZsQjpXVfM:', 'Burmy', '40', '29', '45', '29', '45', '36', '224');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('413','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=TtG_W_fdNebH0PEPv-mbyAY&q=Wormadam&oq=Wormadam&gs_l=img.3..0j0i67k1l3j0j0i67k1j0l4.183521.183521.0.187937.1.1.0.0.0.0.99.99.1.1.0....0...1c.1.64.img..0.1.96....0.jaRYx_xTyjo#imgrc=qdCuRfa60JAgvM:', 'Wormadam', '60', '59', '85', '79', '105', '36', '424');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('413', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=DNK_W9fcJ8er0PEP4sO18AI&q=wormadam+sandy+cloak&oq=Wormadam+sandy&gs_l=img.3.1.0l2j0i24k1.20886.21539.0.23555.6.3.0.3.3.0.80.189.3.3.0....0...1c.1.64.img..0.6.205...0i67k1.0.Zf2saXf8pMs#imgrc=uMT_sFIzX1ZLEM:', 'Wormadam(Sandy Cloak)', '60', '79', '105', '59', '85', '36', '424');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('413', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=JdK_W-e9MsjF0PEPjYusIA&q=wormadam+trashcloak&oq=wormadam+trashcloak&gs_l=img.3..0i7i30k1.28700.29248.0.29946.6.5.0.0.0.0.119.354.4j1.5.0....0...1c.1.64.img..2.4.282...0i24k1.0.9rQcqM6zhmE#imgrc=OfpW37OgBI7ZCM:', 'Wormadam(Trash Cloak)', '60', '69', '95', '69', '95', '36', '424');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('414','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=RdK_W-nmHv6x0PEP8b6FoA0&q=mothim&oq=mothim&gs_l=img.3..0l6j0i10k1j0l3.30935.32818.0.33042.6.6.0.0.0.0.79.397.6.6.0....0...1c.1.64.img..0.6.393...0i67k1.0.WBM_Z0a_n1o#imgrc=VtShlF3_YurJVM:', 'Mothim', '70', '94', '50', '94', '50', '66', '424');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('415', '0' , 'https://www.google.ca/search?q=combee&rlz=1C1EJFA_enCA780CA780&source=lnms&tbm=isch&sa=X&ved=0ahUKEwik_IuDx__dAhVkMn0KHVETALsQ_AUIDigB&biw=1242&bih=553#imgrc=mb6qDBdzm2W-pM:', 'Combee', '30', '30', '42', '30', '42', '70', '244');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('416', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=e96_W92ZCLKz0PEPh4eYmAc&q=vespique&oq=vespique&gs_l=img.3..0l10.15403.16427.0.16891.8.6.0.2.2.0.86.422.6.6.0....0...1c.1.64.img..0.8.438...0i67k1.0.1d9sim9wXMM#imgrc=Clx7V5ir-CsQiM:','Vespiquen', '70', '80', '102', '80', '102', '40','474');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('417', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=jd6_W6DtLMKu0PEPi527-AM&q=pachirisu&oq=pachirisu&gs_l=img.3..0l10.16978.18802.0.19143.9.7.0.2.2.0.143.611.5j2.7.0....0...1c.1.64.img..0.9.623...0i67k1.0.qgsHBC0ZsoQ#imgrc=MYS2vc5ob9wnjM:', 'Pachirisu', '60', '45', '70', '45', '90', '95', '405');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('418','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=ot6_W47eNPnB0PEPi-e2QA&q=buezel&oq=buezel&gs_l=img.3..0i10i24k1.21801.23102.0.23288.6.6.0.0.0.0.96.418.6.6.0....0...1c.1.64.img..0.6.412...0j0i67k1j0i5i30k1j0i5i10i30k1j0i24k1.0.sW1ORas3IdE#imgrc=d3BwUGOUpcvAfM:', 'Buizel', '55', '65', '35', '60', '30', '85', '330');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('419', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=u96_W4CCPO640PEPp_emgAk&q=floatzel&oq=floatzel&gs_l=img.3..0l10.14518.15605.0.16028.8.7.0.1.1.0.196.626.5j2.7.0....0...1c.1.64.img..0.8.638...0i67k1.0.0IdhmYibOR8#imgrc=PeqLmsrGGEZl8M:', 'Floatzel', '85', '105', '55', '85', '50', '115', '495');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('420', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=zd6_W-a1NaOq0PEP-ZCAwAM&q=cherubi&oq=cherubi&gs_l=img.3..0l10.14116.15077.0.15373.7.7.0.0.0.0.83.450.7.7.0....0...1c.1.64.img..0.7.438...0i67k1.0.paQj9gZekRQ#imgrc=GtqWe5PsJg84QM:', 'Cherubi', '45', '35', '45', '62', '53', '35', '275');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('421','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=396_W5PACNmx0PEP9LqLoAk&q=cherrim&oq=cherrim&gs_l=img.3..0l10.48322.48704.0.49045.4.4.0.0.0.0.76.196.3.3.0....0...1c.1.64.img..1.3.189...0i67k1.0.TeK_m0hCkEY#imgrc=4CF_xc3pgi_RIM:', 'Cherrim', '70', '60', '70', '87', '78', '85', '450');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('421', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=Et-_W76SA-HB0PEPhpy5wA8&q=cherrim+sunshine+form&oq=cherrim+sunshine+form&gs_l=img.3..0j0i24k1.16126.17630.0.18027.14.10.0.3.3.0.151.789.7j3.10.0....0...1c.1.64.img..1.13.805...0i67k1j0i8i30k1.0.IxNXMMRd5rM#imgrc=S2RqWolibILF3M:', 'Cherrim(Sunshine Form)', '70', '60', '70', '87', '78', '85', '450');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('422', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=Jt-_W87zAZmy0PEPhv-dsAM&q=shellos&oq=shello&gs_l=img.3.1.0l10.20604.21325.0.24081.6.6.0.0.0.0.96.376.6.6.0....0...1c.1.64.img..0.6.376...0i67k1.0.gCvjxJqdSBA#imgrc=kC_M_yB6MWdMzM:', 'Shellos', '76', '48', '48', '57', '62', '34', '325');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('423', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=P9-_W6iWOL650PEP1vCO0AQ&q=gastrodon&oq=gastrodon&gs_l=img.3..0i67k1j0l3j0i67k1j0l5.16994.18104.0.18328.9.9.0.0.0.0.123.758.6j3.9.0....0...1c.1.64.img..0.9.754....0.fcjtD8s6_JE#imgrc=nzrCopChb_CjyM:', 'Gastrodon', '111', '83', '68', '92', '82', '39', '475');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('424', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=VN-_W_aQCMm90PEP46WxkAQ&q=ambipom&oq=ambipom&gs_l=img.3..0l8j0i10k1j0.28454.30930.0.31706.11.9.2.0.0.0.79.536.9.9.0....0...1c.1.64.img..0.11.551...0i67k1.0.cWruKYXtc9M#imgrc=3U3Y46itL-qD6M:', 'Ambipom', '75', '100', '66', '60', '66', '115', '482');

INSERT INTO pokemon_table1 (NPNumber,  HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('425', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=dd-_W7m8MLTl9APPlaa4Aw&q=drifloon&oq=drifloon&gs_l=img.3..0l10.14587.16595.0.16940.8.5.0.3.3.0.68.271.5.5.0....0...1c.1.64.img..0.8.303...0i67k1.0.al3LB3Bw3tI#imgrc=s2TywZHKpBN1fM:', 'Drifloon', '90', '50', '34', '60', '44', '70', '348');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('426', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=iN-_W__-Lp6z0PEPxtGGsAw&q=drifblim&oq=drifblim&gs_l=img.3..0j0i67k1j0l8.15430.17555.0.17847.8.5.0.3.3.0.79.295.5.5.0....0...1c.1.64.img..0.8.321....0.64W-y4HHjzE#imgrc=IOPM2A_yLM7OBM:', 'Drifblim', '150', '80', '44', '90', '54', '80', '498');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('427', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=nN-_W8KOFcTv9AOg35yQBw&q=buneary+pokemon&oq=bunear&gs_l=img.3.1.0l10.15574.16304.0.17954.6.6.0.0.0.0.69.361.6.6.0....0...1c.1.64.img..0.6.361...0i67k1.0.fWhJRsmhmL0#imgrc=OlM2WWWTyJKgCM:', 'Buneary', '55', '66', '44', '44', '56', '85', '350');

INSERT INTO pokemon_table1 (NPNumber,   HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('428','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=sN-_W6ycBLDJ0PEP-OKU8AQ&q=lipunny&oq=lipunny&gs_l=img.3..0i10i24k1.15225.16470.0.16744.7.7.0.0.0.0.63.371.7.7.0....0...1c.1.64.img..0.7.371...0j0i67k1j0i10k1j0i24k1.0.cwhTHseQFFI#imgrc=0UKAevZPaxnNbM:', 'Lopunny', '65', '76', '84', '54', '96', '105', '480');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('428','0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=wt-_W7y1LqG-0PEP3IaF2AM&q=mega+lipunny&oq=mega+lipunny&gs_l=img.3..0i10i24k1.74440.75082.0.75198.5.5.0.0.0.0.52.224.5.5.0....0...1c.1.64.img..0.1.52....0.m5wPmBDzfh8#imgrc=H_eD1O0iT8s_yM:', 'Mega Lopunny', '65', '136', '94', '54', '96', '135', '580');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('429','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=D-C_W-veOJXw9AOhsZSICw&q=mismagius&oq=mismagius&gs_l=img.3..0j0i67k1l2j0l7.28098.29401.0.29618.9.7.0.2.2.0.112.536.6j1.7.0....0...1c.1.64.img..1.8.437....0.fPCgv4zoa18#imgrc=uMk6BB4PCDeV9M:', 'Mismagius', '60', '60', '60', '105', '105', '105', '495');

INSERT INTO pokemon_table1 (NPNumber,  HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('430', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=L-C_W-T-Nr650PEP1vCO0AQ&q=honchkrow&oq=honchr&gs_l=img.3.0.0i10k1l10.18643.19633.0.20790.6.6.0.0.0.0.68.338.6.6.0....0...1c.1.64.img..0.6.334...0j0i67k1.0.nzbGxVgtLD4#imgrc=sSUbRknyufVbZM:', 'Honchkrow', '100', '125', '52', '105', '52', '71', '505');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('431', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=RuC_W_3lE_Xo9APt5rmoCg&q=glameow&oq=glameo&gs_l=img.3.0.0l10.17834.18594.0.19972.6.6.0.0.0.0.89.381.6.6.0....0...1c.1.64.img..0.6.381...0i67k1j0i10k1.0.YTRPYWBwwRo#imgrc=s9D78U9XGrEcxM:', 'Glameow', '49', '55', '42', '42', '37', '85', '310');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('432', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=W-C_W_fwO6XB7gLByYvIAQ&q=purugly&oq=purugly&gs_l=img.3..0l10.27149.28650.0.28875.7.6.0.1.1.0.72.374.6.6.0....0...1c.1.64.img..0.7.381...0i67k1.0.XyuFhFVn_Zs#imgrc=BFvT-U1Hm84AKM:', 'Purugly', '71', '82', '64', '64', '59', '112', '452');

INSERT INTO pokemon_table1 (NPNumber, HPNumber, Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('433' ,'157','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=euC_W4CrMNTK0PEP94SN-AM&q=chingling&oq=chingling&gs_l=img.3..0l10.41601.42650.0.42941.9.8.0.0.0.0.139.585.7j1.8.0....0...1c.1.64.img..1.8.581...0i67k1j0i10k1.0.qD6QlVbgVGg#imgrc=rtzpn80h7P1nnM:', 'Chingling', '45', '30', '50', '65', '50', '45', '285');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('434', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=p-C_W9efHr-50PEPzpuvKA&q=stunky&oq=stunky&gs_l=img.3..0l9j0i10k1.20599.23190.0.23675.10.8.2.0.0.0.92.543.8.8.0....0...1c.1.64.img..0.10.560...0i67k1j0i5i30k1j0i10i24k1.0.L5scL73psjY#imgrc=-7rg5nULLiBBdM:', 'Stunky', '63', '63', '47', '41', '41', '74', '329');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('435','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=2-C_W-6_KJzC0PEPkoiDyAI&q=skuntank&oq=skuntank&gs_l=img.3..0i67k1j0l2j0i67k1j0j0i67k1j0l3j0i67k1.2795.7011.0.7320.12.10.0.2.2.0.65.571.10.10.0....0...1c.1.64.img..0.12.592...0i10k1.0.3iGQBVET-cQ#imgrc=ORM2vK7cDI6s_M:', 'Stuntank', '103', '93', '67', '71', '61', '84', '479');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('436','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=5OC_W4nKNNW60PEPucyroAM&q=brozor&oq=brozor&gs_l=img.3..0i10i24k1l6.15010.15665.0.15853.6.6.0.0.0.0.86.408.6.6.0....0...1c.1.64.img..0.6.404...0j0i67k1j0i10k1.0.lbclG8yXDlQ#imgrc=c1jq4mecPfvC3M:', 'Bronzor', '57', '24', '86', '24', '86', '23', '300');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('437','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=9uC_W_7QEfiq0PEPwPiNqAc&q=bronzong&oq=bronzong&gs_l=img.3..0l10.18955.20531.0.20591.5.5.0.0.0.0.80.311.5.5.0....0...1c.1.64.img..0.5.304....0.afFB30Sx3hw#imgrc=M3JM8L82SoWLMM:', 'Bronzong', '67', '89', '116', '79', '116', '33', '500');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('438','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=DOG_W42wN7O_0PEPiIuR6AU&q=bonsly&oq=bonsly&gs_l=img.3..0l10.22622.23337.0.23583.6.6.0.0.0.0.89.429.6.6.0....0...1c.1.64.img..0.6.429...0i67k1j0i10k1.0.DFnZkP2-rBQ#imgrc=trVtCCXTP47A4M:', 'Bonsly', '50', '80', '95', '10', '45', '10', '290');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('439', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=JuG_W5ryEZKz0PEP9KSb0A8&q=mime+junior&oq=mime+jun&gs_l=img.3.0.0l2j0i24k1.44304.46220.0.48207.8.8.0.0.0.0.76.494.8.8.0....0...1c.1.64.img..0.8.494...0i67k1j0i8i30k1j0i10i24k1.0.9bx4dREQXvk#imgrc=wTs7jDko-xE5jM:', 'Mime Jr.', '20', '25', '45', '70', '90', '60', '310');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('440', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=WOG_W9W2G-q80PEP0faLyAY&q=happiny&oq=happiny&gs_l=img.3..0l10.18756.19789.0.20212.7.6.0.1.1.0.76.384.6.6.0....0...1c.1.64.img..0.7.389...0i67k1.0.24YOdkRgk1o#imgrc=jpavO3hdnYDFcM:', 'Happiny', '100', '5', '5', '15', '65', '30', '220');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('441','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=guG_W-7EIb690PEPlOOi4AM&q=chatot&oq=chatot&gs_l=img.3..0l8j0i10k1j0.2376.2600.0.2872.3.3.0.0.0.0.64.180.3.3.0....0...1c.1.64.img..0.3.176...0i67k1.0.IcP5c6cvZAM#imgrc=9wuul1EOYC9pvM:', 'Chatot', '76', '65', '45', '92', '42', '91', '411');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('442', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=h-G_W_WyFsvE0PEPuLOq4A0&q=spititomb&oq=spititomb&gs_l=img.3..0i10i24k1.36513.38077.0.38329.9.9.0.0.0.0.147.766.7j2.9.0....0...1c.1.64.img..0.9.761...0j0i67k1j0i10k1.0.luvnfTx6BZg#imgrc=K2SX7Z09sGSQ0M:', 'Spiritomb', '50', '92', '108', '92', '108', '35', '485');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('443', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=r-G_W5DHF8_D0PEP6t6m8AQ&q=gible&oq=gible&gs_l=img.3..0l10.14005.14482.0.14674.5.5.0.0.0.0.123.414.4j1.5.0....0...1c.1.64.img..0.5.414...0i67k1j0i10k1.0.zHBi36BYuGw#imgrc=g65d7Hzz3O1_XM:', 'Gible', '58', '70', '45', '40', '45', '42', '300');

INSERT INTO pokemon_table1 (NPNumber,  HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('444', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=v-G_W_T7M4q40PEP79arkAw&q=gabite&oq=gabite&gs_l=img.3..0i67k1j0l9.9818.10398.0.10662.6.6.0.0.0.0.129.392.4j1.5.0....0...1c.1.64.img..1.5.392....0.AGDUmRp8FvY#imgrc=qrjS0IxqHrjruM:', 'Gabite', '68', '90', '65', '50', '55', '82', '410');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('445', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=KuK_W--wL8jD0PEPhJ6I4AY&q=garchomp&oq=garchomp&gs_l=img.3..0i67k1j0l7j0i67k1j0.4474.5141.0.5509.6.4.0.2.2.0.68.240.4.4.0....0...1c.1.64.img..0.6.257....0.9yRybcneA3E#imgrc=BfGmIHGMvmltNM:', 'Garchomp', '108', '130', '95', '80', '85', '102', '600');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('445','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&tbm=isch&q=garchomp&chips=q:garchomp,online_chips:mega&usg=AI4_-kQjtCUhQCosMd0wbdyhTSZLqf91Lw&sa=X&ved=0ahUKEwiW5dDJyv_dAhVBO30KHWgWAI8Q4lYIJigA&biw=1242&bih=553&dpr=1.1#imgrc=Ojks89k-TOqCHM:', 'Mega Garchomp', '108', '170', '115', '120', '95', '92', '700');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('446','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=ROK_W-fhHMGV0PEP9ISQuAM&q=munchlax&oq=munchlax&gs_l=img.3..0j0i67k1l4j0l5.19558.20595.0.20779.8.6.0.2.2.0.92.402.6.6.0....0...1c.1.64.img..0.8.417....0.x2zSipAcP-c#imgrc=Ety41B6ggBU1vM:', 'Munchlax', '135', '85', '40', '40', '85', '5', '390');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('447', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=W-K_W_iXCYu50PEPhNOUqAs&q=riolu&oq=riolu&gs_l=img.3..0l10.12845.14310.0.14566.5.5.0.0.0.0.103.363.4j1.5.0....0...1c.1.64.img..0.5.363...0i67k1.0.xpD0ApzFNgo#imgrc=qTv2TZIqcpu-xM:', 'Riolu', '40', '70', '40', '35', '40', '60', '285');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('448', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=a-K_W_f_GdHN0PEP64CQWA&q=lucario&oq=lucario&gs_l=img.3..0i67k1l3j0l2j0i67k1j0l2j0i67k1j0.16221.17311.0.17551.7.7.0.0.0.0.145.572.5j2.7.0....0...1c.1.64.img..0.7.572....0.GehHo4UhQGE#imgrc=YSfXBxxtHpe12M:', 'Lucario', '70', '110', '70', '115', '70', '90', '525');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('448','0' , 'https://www.google.ca/search?q=mega+lucario&rlz=1C1EJFA_enCA780CA780&source=lnms&tbm=isch&sa=X&ved=0ahUKEwic1cGyy__dAhV0FTQIHZrrBoIQ_AUIDigB&biw=1242&bih=553#imgrc=uwWKZFCd0b-xQM:', 'Mega Lucario', '70', '145', '88', '140', '70', '112', '625');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('449','0' ,  'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=553&tbm=isch&sa=1&ei=fuK_W7qjJNG60PEPxrqgiAE&q=hippopotas&oq=hippopota&gs_l=img.3.1.0j0i67k1j0l8.13446.14878.0.16590.9.7.0.2.2.0.71.419.7.7.0....0...1c.1.64.img..0.9.443....0.hCQG6LGRKcI#imgrc=ql05saBPCCE6gM:', 'Hippopotas', '68', '72', '78', '38', '42', '32', '330');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('450','0' ,'https://www.google.ca/search?q=hippowdon&rlz=1C1EJFA_enCA780CA780&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjpnP2Z3v_dAhVuGjQIHXk0DiMQ_AUIDigB&biw=1242&bih=597#imgrc=lL7-dY72xl4jYM:' , 'Hippowdon', '108', '112', '118', '68', '72', '47', '525');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('451', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=yva_W7v-A7G70PEP_6O2mAU&q=%27Skorupi%27&oq=%27Skorupi%27&gs_l=img.3..0i30k1l10.40267.40267.0.40879.1.1.0.0.0.0.139.139.0j1.1.0....0...1c.1.64.img..0.1.136....0.lRnh1eBl2u8#imgrc=OI1G8es_ACiUaM:', 'Skorupi', '40', '50', '90', '30', '55', '65', '330');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('452','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=9Pa_W_uBIqa90PEPhI6liAI&q=drapion&oq=drapoin&gs_l=img.3.0.0i10i24k1l2.206847.207905.0.209307.7.7.0.0.0.0.144.473.6j1.7.0....0...1c.1.64.img..0.7.468...0j0i67k1.0.8FgQJRftl44#imgrc=mCmmp261h9zW5M:', 'Drapion', '70', '90', '110', '60', '75', '95', '500');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('453', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=yPe_W_j1A-W80PEPnuyM8Ak&q=croagunk&oq=craogun&gs_l=img.3.0.0i10i24k1.17971.19606.0.20778.7.7.0.0.0.0.103.523.6j1.7.0....0...1c.1.64.img..0.6.482...0j0i67k1.0.SFLbS2vGl5Y#imgrc=46YJ4zJQxCIdNM:', 'Croagunk', '48', '61', '40', '61', '40', '50', '300');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('454', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=__e_W9mxLeTY9APBzoP4Dw&q=toxicroak&oq=toxicroak&gs_l=img.3..0l10.11647.12982.0.13498.6.5.0.1.1.0.63.262.5.5.0....0...1c.1.64.img..0.6.268...0i10i24k1.0.O7zxHsj3fFg#imgrc=FpHENYX7U10FAM:', 'Toxicroak', '83', '106', '65', '86', '65', '85', '490');

INSERT INTO pokemon_table1 (NPNumber,  HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('455', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=D_i_W8edDvvC0PEP2fuW0AI&q=carnivine&oq=carnivine&gs_l=img.3..0l10.14759.17227.0.17549.9.9.0.0.0.0.146.867.4j5.9.0....0...1c.1.64.img..0.9.861...0i67k1.0.TbpMg91_8S8#imgrc=S_2Xe2CZ0tJvzM:', 'Carnivine', '74', '100', '72', '90', '72', '46', '454');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('456', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Ivi_W7iIFd2v0PEP3vC7oAw&q=finneon&oq=finneon&gs_l=img.3..0l9j0i30k1.20948.21849.0.22157.7.6.0.1.1.0.80.363.6.6.0....0...1c.1.64.img..0.7.368...0i67k1.0.8wnT9Is1lu0#imgrc=K2Q4h_tDj-NmQM:', 'Finneon', '49', '49', '56', '49', '61', '66', '330');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('457', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Ovi_W_eKIuLb9APnpayYCg&q=lumenoen&oq=lumenoen&gs_l=img.3...15779.16971.0.17404.8.8.0.0.0.0.66.424.8.8.0....0...1c.1.64.img..0.6.332...0j0i67k1.0.tqzrvuV-1_8#imgrc=VdwblvHpZKDaAM:', 'Lumineon', '69', '69', '76', '69', '86', '91', '460');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('458','0' ,  'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Tfi_W_-hPOTF0PEP-JuzkAg&q=%27Mantyke&oq=%27Mantyke&gs_l=img.3..0l10.16652.16652.0.17164.1.1.0.0.0.0.65.65.1.1.0....0...1c.1.64.img..0.1.63....0.k8cnBja6I-I#imgrc=-YAnuKLxCvR3PM:', 'Mantyke', '45', '20', '50', '60', '120', '50', '345');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('459', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Yfi_W9-aC5q70PEPmdeNsAs&q=snover&oq=snover&gs_l=img.3..0l10.23939.24460.0.24762.6.6.0.0.0.0.130.535.4j2.6.0....0...1c.1.64.img..0.6.531...0i67k1.0.TSLK4-IyVGU#imgrc=p_9nAqDTOSRYCM:', 'Snover', '60', '62', '50', '62', '60', '40', '334');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('460','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=e_i_W730L5H49AOr0pnQCw&q=abomasnow&oq=abosnow&gs_l=img.3.0.0i10i24k1.22362.29687.0.31068.22.14.8.0.0.0.104.896.13j1.14.0....0...1c.1.64.img..0.20.829...0j0i67k1j0i10k1.0.6N3S3Ttalgk#imgrc=2ZtUerCHjPuwnM:', 'Abomasnow', '90', '92', '75', '92', '85', '60', '494');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('460','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=nfi_W-eiCNbD0PEP_YKMgAo&q=mega+abomasnow&oq=mega+abomasnow&gs_l=img.3..0l6j0i30k1l3j0i5i30k1.20579.21302.0.21902.5.5.0.0.0.0.67.287.5.5.0....0...1c.1.64.img..0.5.284...0i7i30k1j0i8i7i30k1.0.4s2j6F1A9y4#imgrc=Hw9IDOHrle4iBM:', 'Mega Abomasnow', '90', '132', '105', '132', '105', '30', '594');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('461','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=z_i_W6mVNuKs0PEP6d2UmAc&q=weavile&oq=weavile&gs_l=img.3..0l10.7514.9549.0.9732.7.7.0.0.0.0.126.447.6j1.7.0....0...1c.1.64.img..0.7.442...0i10k1j0i5i30k1j0i5i10i30k1j0i10i24k1j0i24k1.0.C6LDz7HLsjk#imgrc=kLNJj1hCB16HIM:', 'Weavile', '70', '120', '65', '45', '85', '125', '510');

INSERT INTO pokemon_table1 (NPNumber, HPNumber, Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('462', '86','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=3Pi_W7_cGI2y0PEP0a-hwAU&q=Magnezone&oq=Magnezone&gs_l=img.3..0l10.16009.16009.0.17096.1.1.0.0.0.0.67.67.1.1.0....0...1c.1.64.img..0.1.64....0.L5Nw9WiFPZk#imgrc=AQVRKcoEzC9XBM:',  'Magnezone', '70', '70', '115', '130', '90', '60', '535');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('463', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=7_i_W46VD9680PEPiYei8A8&q=lickylikcy&oq=lickylikcy&gs_l=img.3...17045.18269.0.18650.10.10.0.0.0.0.111.655.8j2.10.0....0...1c.1.64.img..0.5.352...0j0i67k1j0i10k1.0.fPNClVfPezE#imgrc=cKhSQZlwZ3zd5M:', 'Lickilicky', '110', '85', '95', '80', '95', '50', '515');

INSERT INTO pokemon_table1 (NPNumber,  HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('464', '178', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=A_m_W4beLOew0PEPjPqGsAU&q=Rhyperior&oq=Rhyperior&gs_l=img.3..0l10.32321.32321.0.33380.1.1.0.0.0.0.70.70.1.1.0....0...1c.1.64.img..0.1.69....0.tTTiX9HXu3o#imgrc=S6c2MbHLxH0JIM:', 'Rhyperior', '115', '140', '130', '55', '55', '40', '535');

INSERT INTO pokemon_table1 (NPNumber,  HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('465', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Jvm_W8HRN-u80PEP1LyJMA&q=tangrowtj&oq=tangrowtj&gs_l=img.3..0i10i24k1.16281.17372.0.17596.9.8.0.1.1.0.93.490.8.8.0....0...1c.1.64.img..0.9.498...0j0i67k1j0i10k1.0.jIeBOO0K79Q#imgrc=OfzouZd0Gk9dfM:', 'Tangrowth', '100', '100', '125', '110', '50', '50', '535');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('466','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Ovm_W5mOFb7L0PEPg5GM6AU&q=electivire&oq=electivire&gs_l=img.3..0l10.15352.21439.0.21659.24.14.7.3.3.0.103.822.13j1.14.0....0...1c.1.64.img..0.19.705...0i67k1j0i10k1j0i10i24k1j0i24k1.0.as5EBDVwlq0#imgrc=WGlRveFgCNfhYM:', 'Electivire', '75', '123', '67', '95', '85', '95', '540');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('467', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Ufm_W5GfOpq_0PEP7IChuAc&q=magmortar&oq=magmortar&gs_l=img.3..0l3j0i67k1l2j0l5.19072.22126.0.28863.9.7.0.2.2.0.91.403.6.6.0....0...1c.1.64.img..1.8.424....0.0G3aEn8sNC0#imgrc=yAlDMfanT7gzJM:', 'Magmortar', '75', '95', '67', '125', '95', '83', '540');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('468', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=cPm_W63MOMLQ9APnr4q4Ag&q=Togekiss&oq=Togekiss&gs_l=img.3..0l10.993065.993065.0.993627.1.1.0.0.0.0.62.62.1.1.0....0...1c.1.64.img..0.1.62....0.U79iz3_e9eM#imgrc=zDXVrHkueWgY3M:' , 'Togekiss', '85', '50', '95', '120', '115', '80', '545');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('469', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=VP2_W_CuIPPB0PEPhaersAI&q=Yanmeg&oq=Yanmeg&gs_l=img.3..0l10.29791.29791.0.30366.1.1.0.0.0.0.63.63.1.1.0....0...1c.1.64.img..0.1.61....0.W36nGif-3rY#imgrc=Kmi3m59_CoEJDM:' , 'Yanmega', '86', '76', '86', '116', '56', '95', '515');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('470', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=dP2_W7qYMNuT0PEPr9mguAE&q=LEAFOEN&oq=LEAFOEN&gs_l=img.3..0i10i24k1.14028.14955.0.15181.7.7.0.0.0.0.111.539.5j2.7.0....0...1c.1.64.img..0.7.534...0j0i67k1j0i10k1.0.9t2QcFEwpB4#imgrc=oNElObqKU6FtwM:' , 'Leafeon', '65', '110', '130', '60', '65', '95', '525');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('471', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=hv2_W_zGBq3F0PEPxseb8AE&q=GLACEIN&oq=GLACEIN&gs_l=img.3..0i10i24k1l2.13381.14333.0.14630.7.7.0.0.0.0.83.438.7.7.0....0...1c.1.64.img..0.7.433...0j0i67k1j0i10k1.0.Osl6VarnoZU#imgrc=azNk-AbAeRhxEM:' , 'Glaceon', '65', '60', '110', '130', '95', '65', '525');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('472', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=p_2_W-qGDLXT9APbt4v4DA&q=GLISCOR&oq=GLISCOR&gs_l=img.3..0l10.2492.2727.0.3010.2.2.0.0.0.0.60.111.2.2.0....0...1c.1.64.img..0.2.109...0i7i30k1j0i8i7i30k1j0i10i24k1.0.Sqzxs7J3WM0#imgrc=PoyneX24ucFcjM:' , 'Gliscor', '75', '95', '125', '45', '75', '95', '510');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('473', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=rP2_W9CFB7XB7gKa8oigBQ&q=MAMOSWINE&oq=MAMOSWINE&gs_l=img.3..0l10.43820.45219.0.45447.9.6.0.3.3.0.95.404.6.6.0....0...1c.1.64.img..0.9.430...0i67k1j0i10k1.0._yeHedvfyTQ#imgrc=OTwy1Mu4jRxMTM:' , 'Mamoswine', '110', '130', '80', '70', '60', '80', '530');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('474','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=2_2_W_H8CdX19AOf6rnYBg&q=PORYGON-Z&oq=PORYGON-Z&gs_l=img.3..0l5j0i30k1l5.39628.41109.0.41561.9.7.0.2.2.0.126.531.5j2.7.0....0...1c.1.64.img..0.9.547...0i67k1.0.fL9YA07aXJg#imgrc=xopsbBpv4-dSbM:' , 'Porygon-Z', '85', '80', '70', '135', '75', '90', '535');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('475','32' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Bv6_W4OsHpC_0PEP3JqjiAM&q=Gallade&oq=Gallade&gs_l=img.3..0l10.1799929.1800801.0.1801041.2.2.0.0.0.0.73.124.2.2.0....0...1c.1.64.img..0.2.123...0i30k1.0.ftsGJHP9apE#imgrc=eYFgSmYm2Tyn0M:' , 'Gallade', '68', '125', '65', '65', '115', '80', '518');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('475','32' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=EAXAW_CfMYLQ9APRuoLIDQ&q=mega+Gallade&oq=mega+Gallade&gs_l=img.3..0l3j0i67k1j0l3j0i67k1j0l2.21767.22294.0.22718.5.5.0.0.0.0.60.268.5.5.0....0...1c.1.64.img..0.5.266...0i7i30k1.0.uRuwcrxfpyo#imgrc=B6Mx1-rMdpGrKM:' , 'Mega Gallade', '68', '125', '95', '65', '115', '110', '618');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('476','62' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=KQXAW-XCD8zL0PEP5p2lyA8&q=Probopass&oq=Probopass&gs_l=img.3..0l10.12333.12333.0.12624.1.1.0.0.0.0.66.66.1.1.0....0...1c.1.64.img..0.1.65....0.8lRr8hsSodc#imgrc=B0U2N8LIXDyuhM:' , 'Probopass', '60', '55', '145', '75', '150', '40', '525');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('477','155' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=SQXAW9ONKo--0PEP5KGfqA8&q=dusknoir&oq=dusknoir&gs_l=img.3..0l10.17585.19109.0.19414.8.5.0.3.3.0.81.341.5.5.0....0...1c.1.64.img..0.8.372...0i67k1.0.V7vzbIdKfvM#imgrc=VibUTOgCf3YUHM:' , 'Dusknoir', '45', '100', '135', '65', '135', '45', '525');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('478','181' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=XgXAW_iDK8fK0PEPmPSN2AE&q=Froslass&oq=Froslass&gs_l=img.3..0l10.53587.53587.0.54248.1.1.0.0.0.0.62.62.1.1.0....0...1c.1.64.img..0.1.61....0.SEO0EZEDHeQ#imgrc=RBD-5eFCRf0fSM:' , 'Froslass', '70', '80', '70', '80', '70', '110', '480');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('479', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=JzDAW5r3DKXB7gLByYvIAQ&q=rotom+pokemon&oq=roton+pokem&gs_l=img.3.1.0i10i30k1j0i10i24k1.1969.2911.0.4759.6.6.0.0.0.0.84.399.6.6.0....0...1c.1.64.img..0.6.395...0j0i67k1j0i8i30k1j0i24k1.0.HAC91KaNnrc#imgrc=5QaG6kLrJeQF4M:' , 'Rotom', '50', '50', '77', '95', '77', '91', '440');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('479','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=LTDAW-3AMpzL0PEP5pWnoAU&q=heat+rotom&oq=heat&gs_l=img.3.0.0i67k1l3j0l5j0i67k1j0.110531.112275.0.113850.10.8.0.0.0.0.76.405.6.6.0....0...1c.1.64.img..5.5.338....0.4BGytJip2OM#imgrc=pLNMuFjurcscLM:' , 'Rotom(Heat Rotom)', '50', '65', '107', '105', '107', '86', '520');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('479','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=oTDAW9bJIdG60PEPxrqgiAE&q=wash+rotom&oq=wash+rotom&gs_l=img.3..0l2j0i7i30k1j0i5i30k1j0i8i30k1l5j0i24k1.16005.16485.0.16832.4.4.0.0.0.0.112.323.3j1.4.0....0...1c.1.64.img..1.3.248...0i7i5i30k1j0i8i7i30k1.0.GPT8arPPvTU#imgrc=Jjjay6l-sQ_lgM:' , 'Rotom(Wash Rotom)', '50', '65', '107', '105', '107', '86', '520');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('479', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=tDDAW-vaDsXK0PEPxNGqwAg&q=frost+roton&oq=frost+roton&gs_l=img.3...16483.21270.0.21454.18.15.3.0.0.0.80.939.15.15.0....0...1c.1.64.img..0.17.907...0j0i67k1j0i10k1j0i10i24k1j0i5i30k1j0i8i30k1j0i24k1j0i30k1.0.9I_SvMXjGXU#imgrc=K2bZt92mEeQ_kM:' , 'Rotom(Frost Rotom)', '50', '65', '107', '105', '107', '86', '520');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('479', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=yzDAW_j9IZOt0PEP8umKyAo&q=fan+rotom&oq=fan+rotom&gs_l=img.3..0l3j0i8i30k1l4j0i8i10i30k1j0i8i30k1j0i24k1.88253.91016.0.92038.7.6.1.0.0.0.88.398.6.6.0....0...1c.1.64.img..0.6.351...0i10i24k1.0.HTPJKEcl1aw#imgrc=CpreLMRY5yL2zM:' , 'Rotom(Fan Rotom)', '50', '65', '107', '105', '107', '86', '520');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('479', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=KTHAW4mIIoqx0PEPw5GhEA&q=mow+rotom&oq=mow+rotom&gs_l=img.3..0l2j0i5i30k1j0i24k1l7.27584.28864.0.29093.5.5.0.0.0.0.146.482.3j2.5.0....0...1c.1.64.img..1.2.231....0.MeUVXDaj9J4#imgrc=d3765FfFdM-vkM:' , 'Rotom(Mow Rotom)', '50', '65', '107', '105', '107', '86', '520');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('480', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=SDHAW7K3JOa70PEPtum8oAI&q=uxie&oq=uxie&gs_l=img.3..0l10.20989.22081.0.22419.4.4.0.0.0.0.128.384.2j2.4.0....0...1c.1.64.img..0.4.384...0i67k1j0i10k1.0.OVGqe3FEF74#imgrc=ggtbM_z3HUs5hM:' , 'Uxie', '75', '75', '130', '75', '130', '95', '580');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('481', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=YDHAW-CPN7Cx0PEPjM6K2As&q=mesprit&oq=mesprit&gs_l=img.3..0l7j0i67k1j0l2.16747.18427.0.18827.7.7.0.0.0.0.156.665.3j3.6.0....0...1c.1.64.img..1.6.657....0.zEv0UhcK2kQ#imgrc=gEtJXJqjyjGYhM:' , 'Mesprit', '80', '105', '105', '105', '105', '80', '580');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('482', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=dTHAW_GwJoC80PEPoaOkoAk&q=Azelf&oq=Azelf&gs_l=img.3..0i67k1j0l2j0i67k1j0l4j0i67k1j0.17534.17534.0.17972.1.1.0.0.0.0.72.72.1.1.0....0...1c.1.64.img..0.1.68....0._DF33_BZT1A#imgrc=zAbHbM5PIrbmkM:' , 'Azelf', '75', '125', '70', '125', '70', '115', '580');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('483','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=iTHAW4fYGpSv0PEPsLGvWA&q=dialga&oq=dial&gs_l=img.3.0.0i67k1j0l2j0i67k1l2j0l5.196140.198395.0.199767.8.6.2.0.0.0.81.385.6.6.0....0...1c.1.64.img..0.8.405....0.DDlZiwdFKe8#imgrc=kxorkF7ZUuW1tM:' , 'Dialga', '100', '120', '120', '150', '100', '90', '680');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('484','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=UzLAW42QBoL19AO_zJKQCA&q=palkia&oq=palkia&gs_l=img.3..0i67k1j0l2j0i67k1j0l4j0i67k1j0.11575.12511.0.12875.6.6.0.0.0.0.76.391.6.6.0....0...1c.1.64.img..0.6.391....0.rCkEWeYeklM#imgrc=1EfTZ-SriKxqJM:' , 'Palkia', '90', '120', '100', '150', '120', '100', '680');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('485', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=YjLAW4zIBL-50PEPzpuvKA&q=heatran&oq=heatran&gs_l=img.3..0l10.14564.15419.0.15748.7.7.0.0.0.0.108.539.6j1.7.0....0...1c.1.64.img..0.7.535...0i67k1j0i10k1.0.V3qR2zVB83E#imgrc=nmjSeIeJ_qQ0PM:' , 'Heatran', '91', '90', '106', '130', '106', '77', '600');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('486','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=czLAW_T2Mc-70PEPr8i84Ao&q=Regigigas&oq=Regigigas&gs_l=img.3..0j0i67k1j0l8.22686.22686.0.23111.1.1.0.0.0.0.69.69.1.1.0....0...1c.1.64.img..0.1.69....0.qRzhNtvqEmE#imgrc=R6cE6XbmW7tvEM:' , 'Regigigas', '110', '160', '110', '80', '110', '100', '670');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('487','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=jTLAW4qiAcOt0PEP-8yEiAM&q=Giratina&oq=Giratina&gs_l=img.3..0i67k1j0j0i67k1j0j0i67k1j0l2j0i67k1j0l2.17383.17995.0.18843.1.1.0.0.0.0.76.76.1.1.0....0...1c.1.64.img..0.1.72....0.tWifjXhC3kM#imgrc=87JYH6lxDIgPVM:' , 'Giratina', '150', '100', '120', '100', '120', '90', '680');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('487', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=LTPAW5jHJIqx0PEPw5GhEA&q=Giratina+origin+form&oq=Giratina+origin+form&gs_l=img.3..0l7j0i5i30k1l3.20180.22219.0.22710.12.8.0.4.4.0.136.596.7j1.8.0....0...1c.1.64.img..0.12.643...0i67k1j0i8i30k1j0i24k1.0.81nKSAX0sRA#imgrc=gG5h27eCQGQXUM:' , 'Giratina(Origin Forme)', '150', '120', '100', '120', '100', '90', '680');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('488','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=RjPAW8X_F7Os0PEPv7O3kAg&q=cresselia&oq=cresselia&gs_l=img.3..0l3j0i67k1j0l6.19987.22767.0.22955.13.10.2.1.1.0.90.652.10.10.0....0...1c.1.64.img..0.13.673...0i30k1j0i10i24k1j0i24k1.0.soy7cDMXjOY#imgrc=1csUpHa_ZXTTpM:' , 'Cresselia', '120', '70', '120', '75', '130', '85', '600');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('489', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=XzPAW7nHCcbK0PEPge662A8&q=phione&oq=phione&gs_l=img.3..0l6j0i10k1j0l2j0i10k1.10961.11820.0.12335.6.6.0.0.0.0.142.538.4j2.6.0....0...1c.1.64.img..0.6.534...0i67k1.0.yjXzLjsTHQQ#imgrc=JE99KnXd_qnTEM:' , 'Phione', '80', '80', '80', '80', '80', '80', '480');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('490', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=bTPAW9jNI_is0PEP0u6piAU&q=manaphy&oq=manaphy&gs_l=img.3..0i67k1l2j0l8.41325.43149.0.43456.7.6.0.1.1.0.100.428.5j1.6.0....0...1c.1.64.img..0.7.437....0.gNKUvg1uD6E#imgrc=VKmoT829eKdI_M:' , 'Manaphy', '100', '100', '100', '100', '100', '100', '600');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('491','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=mjPAW_6WMpe_0PEPtJy-wAk&q=darkrai&oq=darkrai&gs_l=img.3..0l10.11769.12928.0.13242.7.6.0.1.1.0.88.395.6.6.0....0...1c.1.64.img..0.7.398...0i67k1.0.26hMrHVuI5Q#imgrc=Y9Ye1c7UnKAudM:' , 'Darkrai', '70', '90', '90', '135', '90', '125', '600');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('492','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=qjPAW9XXBdX19AOf6rnYBg&q=shaymin&oq=shaymin&gs_l=img.3..0i67k1j0l9.56622.57524.0.57865.7.6.0.1.1.0.76.403.6.6.0....0...1c.1.64.img..0.7.410....0.vk-wGoo1N3I#imgrc=bdmMV8XT_QHYXM:' , 'Shaymin', '100', '100', '100', '100', '100', '100', '600');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('492','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=5TPAW6bPN4PC0PEPl-CFiAM&q=Shaymin%28Sky+Forme%29&oq=Shaymin%28Sky+Forme%29&gs_l=img.3..0i30k1.29361.29361.0.30351.1.1.0.0.0.0.65.65.1.1.0....0...1c.1.64.img..0.1.65....0.hJtrzVs706A#imgrc=Rd1KYKE4rH8D-M:' , 'Shaymin(Sky Forme)', '100', '103', '75', '120', '75', '127', '600');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('493','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=BjTAW_SrCaLo9APY2YTYAw&q=arceus&oq=arceus&gs_l=img.3..0l5j0i67k1j0l2j0i67k1l2.28892.29932.0.30096.6.5.0.1.1.0.72.332.5.5.0....0...1c.1.64.img..0.6.331....0.4XUfPhNLT4w#imgrc=xXW8Y8km_ZdMpM:' , 'Arceus', '120', '120', '120', '120', '120', '120', '720');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('494','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=JjTAW6iZD8u10PEP8uaqqA8&q=Victini&oq=Victini&gs_l=img.3..0i67k1j0j0i67k1j0l7.16674.17898.0.18235.2.2.0.0.0.0.85.165.2.2.0....0...1c.1.64.img..0.2.161...0i30k1.0.ILKMdiWoPHQ#imgrc=LJg2vhAWK3ggSM:' , 'Victini', '100', '100', '100', '100', '100', '100', '600');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('495','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=OjTAW72YGp7K0PEPm9a38Ac&q=snivy&oq=snivy&gs_l=img.3..0l10.23757.24601.0.25387.5.5.0.0.0.0.84.337.5.5.0....0...1c.1.64.img..0.5.337...0i67k1.0.vto84aombFo#imgrc=Xa6RcUMc5nZotM:' , 'Snivy', '45', '45', '55', '45', '55', '63', '308');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('496','0' , 'https://www.google.ca/search?q=Servine&rlz=1C1EJFA_enCA780CA780&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjruZuAmoDeAhWVMn0KHfKWCw8Q_AUIDigB&biw=1242&bih=597#imgrc=xfCHz8QHw1tEYM:' , 'Servine', '60', '60', '75', '60', '75', '83', '413');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('497', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=fDXAW8uzOJm70PEP8diBiAQ&q=Serperior&oq=Serperior&gs_l=img.3..0i67k1j0l9.33103.33103.0.33739.1.1.0.0.0.0.71.71.1.1.0....0...1c.1.64.img..0.1.68....0.Flp1taz0HRU#imgrc=7Z6fL6e7zbAZuM:' , 'Serperior', '75', '75', '95', '75', '95', '113', '528');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('498', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=oDXAW4yFHNLZ9AOqubboDA&q=tepig&oq=tepig&gs_l=img.3..0i67k1j0l4j0i67k1j0l4.15665.16691.0.16919.5.5.0.0.0.0.108.375.4j1.5.0....0...1c.1.64.img..0.5.375....0.Xkj-cbMzp9I#imgrc=4sZQT50i3s7wZM:' , 'Tepig', '65', '63', '45', '45', '45', '45', '308');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('499', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=szXAW4GmD5i90PEPq-WlGA&q=pignite&oq=pignite&gs_l=img.3..0l10.21938.23142.0.23960.7.7.0.0.0.0.75.442.7.7.0....0...1c.1.64.img..0.7.442...0i67k1j0i10k1.0.Jx8plgeWxgE#imgrc=IjazR53TSh1HvM:' , 'Pignite', '90', '93', '55', '70', '55', '55', '418');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('500', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=zTXAW6elApe10PEPy-2NgAY&q=emboar&oq=emboar&gs_l=img.3..0l10.16555.18031.0.18548.6.6.0.0.0.0.297.561.4j0j1.5.0....0...1c.1.64.img..1.5.557...0i67k1.0.TV9mDg1qLk0#imgrc=8zIhkDDMsRtSdM:' , 'Emboar', '110', '123', '65', '100', '65', '65', '528');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('501','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=4TXAW7TrGMTv9AOg35yQBw&q=Oshawott&oq=Oshawott&gs_l=img.3..0l10.49522.49522.0.49870.1.1.0.0.0.0.80.80.1.1.0....0...1c.1.64.img..0.1.76....0.7dae_rjZVkk#imgrc=yNLOyAYih1-GZM:' , 'Oshawott' , '55', '55', '45', '63', '45', '45', '308');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('502','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=FTbAW-WfFP250PEPybuc6A0&q=dewott&oq=dewott&gs_l=img.3..0i67k1j0l2j0i67k1j0l6.12904.13712.0.13980.6.5.0.1.1.0.81.320.5.5.0....0...1c.1.64.img..0.6.332....0.7J-1yFN00TI#imgrc=xveewUTZcRVs-M:' , 'Dewott', '75', '75', '60', '83', '60', '60', '413');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('503', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=JTbAW4CKE8Tc9APKg4PoCg&q=samurott&oq=samurott&gs_l=img.3..0i67k1j0l3j0i67k1j0l5.11014.12391.0.12510.8.7.0.1.1.0.116.555.6j1.7.0....0...1c.1.64.img..0.8.561....0.C86PayrieNc#imgrc=GwlQ4wiZV2MVEM:' , 'Samurott', '95', '100', '85', '108', '70', '70', '528');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('504', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=MzbAW5jGG8_D0PEP6t6m8AQ&q=patrat&oq=patrat&gs_l=img.3..0l10.12681.14519.0.14935.6.6.0.0.0.0.136.476.5j1.6.0....0...1c.1.64.img..0.6.468...0i67k1.0.m_I8wV33rMg#imgrc=w6G2va3YPmfnoM:' , 'Patrat', '45', '55', '39', '35', '39', '42', '255');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('505','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=WzbAW6WuNfe40PEPypK9yAc&q=watchog+pokemon&oq=watchog+pokemon&gs_l=img.3..0l2j0i8i30k1l2j0i24k1.97569.97569.0.97814.1.1.0.0.0.0.68.68.1.1.0....0...1c.1.64.img..0.1.68....0.11eD8J-RBxM#imgrc=DhN1P3Pt48fqBM:' , 'Watchog', '60', '85', '69', '60', '69', '77', '420');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('506', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=vzbAW7bvFfP19AOWyL8g&q=lilipup&oq=lilipup&gs_l=img.3..0i10k1l10.23357.24856.0.25206.7.7.0.0.0.0.72.440.7.7.0....0...1c.1.64.img..0.7.431...0j0i67k1j0i10i67k1.0.UL-eEAAr5-g#imgrc=DqAZ54_Tp2o-_M:' , 'Lillipup', '45', '60', '45', '25', '45', '55', '275');

INSERT INTO pokemon_table1 (NPNumber,HPNumber ,  Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('507', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=2jbAW7CeHNK80PEP0c2T2Ac&q=herdier&oq=herdier&gs_l=img.3..0l9j0i30k1.31605.32698.0.33214.7.7.0.0.0.0.75.433.7.7.0....0...1c.1.64.img..0.7.426...0i67k1j0i10k1.0.Z86e_sHCmwI#imgrc=SN1FOD6jFT8neM:' , 'Herdier', '65', '80', '65', '35', '65', '60', '370');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('508','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=_TbAW5GkLYPs9AOA346QDQ&q=stoutland&oq=stuotland&gs_l=img.3.0.0i10i24k1.12831.14643.0.15913.9.9.0.0.0.0.164.642.8j1.9.0....0...1c.1.64.img..0.9.634...0j0i67k1j0i10k1.0.MJmKxSFB0Qo#imgrc=nQsuABgeKHyCaM:' , 'Stoutland', '85', '110', '90', '45', '90', '80', '500');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('509','0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=DzfAW_SgJOTA0PEP_aKR-Ao&q=purrloin&oq=purrloin&gs_l=img.3..0l10.18449.20612.0.20919.8.6.0.2.2.0.80.383.6.6.0....0...1c.1.64.img..0.8.399...0i67k1.0.qk4uHq-ZIdM#imgrc=inA5Ld98gNTt9M:' , 'Purrloin', '41', '50', '37', '50', '37', '66', '281');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('510', '0' ,'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=JjfAW9COJtHE0PEP6JuhmAU&q=liepard&oq=liepard&gs_l=img.3..0l2j0i67k1j0j0i67k1l3j0l3.12940.13899.0.14181.7.6.0.1.1.0.116.416.5j1.6.0....0...1c.1.64.img..0.7.427....0.olZjsvPMwac#imgrc=r3z6ub0Kn9V2GM:' , 'Liepard', '64', '88', '50', '88', '50', '106', '446');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('511','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=NjfAW63SJaTs9AP_qaqgCw&q=pansage&oq=pansage&gs_l=img.3..0l10.15888.16985.0.17438.7.6.0.1.1.0.90.404.6.6.0....0...1c.1.64.img..0.7.406...0i67k1.0.xqvuzsYSD_0#imgrc=BfTnQoM_-q6aJM:' , 'Pansage', '50', '53', '48', '53', '48', '64', '316');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('512', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=STfAW_mFMfTC0PEP59-VGA&q=simisage&oq=simisage&gs_l=img.3..0l10.13622.15637.0.15927.10.8.1.1.1.0.128.564.7j1.8.0....0...1c.1.64.img..0.10.580...0i67k1.0.Na-r1N3dExo#imgrc=QuMd3F0xpTemOM:' , 'Simisage', '75', '98', '63', '98', '63', '101', '498');

INSERT INTO pokemon_table1 (NPNumber, HPNumber,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('513', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=WzfAW9bwKva-0PEPrp6_qAQ&q=pansear&oq=pansear&gs_l=img.3..0l10.13348.14247.0.14624.7.7.0.0.0.0.73.452.7.7.0....0...1c.1.64.img..0.7.448...0i67k1.0.okp3EKcvQIw#imgrc=2EVckGPkG4n9uM:' , 'Pansear', '50', '53', '48', '53', '48', '64', '316');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('514', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=jzfAW6aaMOKz0PEP74KoIA&q=simisear&oq=simisear&gs_l=img.3..0i67k1j0l7j0i67k1j0.1180.2089.0.2701.8.8.0.0.0.0.101.586.7j1.8.0....0...1c.1.64.img..0.8.580...0i10k1.0.uoiITrNYWnU#imgrc=5koxoYQJtennCM:' , 'Simisear', '75', '98', '63', '98', '63', '101', '498');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('515', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=lDfAW-6CF9S70PEPwPmLuAE&q=panpour&oq=panpour&gs_l=img.3..0l10.16861.18700.0.18784.7.6.0.1.1.0.89.398.6.6.0....0...1c.1.64.img..0.7.410...0i67k1j0i10k1.0.F9L9nIVIFUM#imgrc=3HROqvh1nqL1CM:' , 'Panpour', '50', '53', '48', '53', '48', '64', '316');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('516','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=qDfAW_iiM4bA0PEPr4iisAg&q=simipour&oq=simipour&gs_l=img.3..0l5j0i67k1j0l4.19941.21314.0.21566.8.7.0.1.1.0.84.465.7.7.0....0...1c.1.64.img..0.8.472....0.0YJk-PtbfUA#imgrc=vp-6VR0_9nvujM:' , 'Simipour', '75', '98', '63', '98', '63', '101', '498');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('517','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=wDfAW_3ZFanB0PEPiKiI-AM&q=munna&oq=munna&gs_l=img.3..0j0i67k1j0l8.19217.19913.0.20115.5.5.0.0.0.0.72.318.5.5.0....0...1c.1.64.img..0.5.318....0.RlT_kGYQ0Wc#imgrc=UAMeqvF1nNMiDM:' , 'Munna', '76', '25', '45', '67', '55', '24', '292');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('518', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=1jfAW6a-FcXD0PEPua6P4AE&q=musharna&oq=usharna&gs_l=img.1.0.0i10i24k1l3.25771.25771.0.27564.1.1.0.0.0.0.64.64.1.1.0....0...1c.1.64.img..0.1.61....0.6CZP4rUgLXs#imgrc=LIKbTU1SS3Gw7M:' , 'Musharna', '116', '55', '85', '107', '95', '29', '487');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('519', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=8zfAW_ayPI-w0PEP2_KqoAU&q=pidove&oq=pidove&gs_l=img.3..0l10.18196.18885.0.19393.6.5.0.1.1.0.88.362.5.5.0....0...1c.1.64.img..0.6.371...0i67k1j0i10k1.0.UyjbGFqXFKI#imgrc=jLSFLLC74XBZUM:' , 'Pidove', '50', '55', '50', '36', '30', '43', '264');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('520','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=bjjAW9jlLqK-0PEPwqmKUA&q=tranquill&oq=tranquill&gs_l=img.3..0l4j0i10k1j0l5.885.885.0.1065.1.1.0.0.0.0.73.73.1.1.0....0...1c.1.64.img..0.1.69....0.XQrEtxeR74Q#imgrc=duJB5ZOL-ApdhM:' , 'Tranquill', '62', '77', '62', '50', '42', '65', '358');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('521','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=cjjAW9jVBeiU0PEP4LCX0Aw&q=Unfezant&oq=Unfezant&gs_l=img.3..0l10.3789.4763.0.4992.2.2.0.0.0.0.72.129.2.2.0....0...1c.1.64.img..0.2.124...0i30k1.0.oVbQWiNg868#imgrc=Pnm65JJsGBcP5M:' , 'Unfezant', '80', '115', '80', '65', '55', '93', '488');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('522','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=nDjAW9XDHLm40PEP8My0gAo&q=blitzle&oq=blitzle&gs_l=img.3..0l10.15863.18267.0.19033.7.7.0.0.0.0.76.432.7.7.0....0...1c.1.64.img..0.7.427...0i67k1.0.hW79IfCF72U#imgrc=FjigLQEHV_vh6M:' , 'Blitzle', '45', '60', '32', '50', '32', '76', '295');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('523','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=zDjAW4GYG8Xl9AOS1Z34BA&q=Zebstrika&oq=Zebstrika&gs_l=img.3..0i67k1j0l9.672.672.0.845.1.1.0.0.0.0.100.100.0j1.1.0....0...1c.1.64.img..0.1.100....0.LGMu9jzlm3M#imgrc=ZauKNAHPjUZiFM:' , 'Zebstrika', '75', '100', '63', '80', '63', '116', '497');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('524', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=zzjAW_K-CMvC0PEPqc6f0A0&q=Roggenrola&oq=Roggenrola&gs_l=img.3..0l10.109811.109811.0.110460.1.1.0.0.0.0.64.64.1.1.0....0...1c.1.64.img..0.1.64....0.EG-SeToxSfs#imgrc=6ehJG7MFeUdrsM:' , 'Roggenrola', '55', '75', '85', '25', '25', '15', '280');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('525','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=PznAW4qmBZe_0PEPtJy-wAk&q=Boldore&oq=Boldore&gs_l=img.3..0l10.20432.20432.0.21066.1.1.0.0.0.0.112.112.0j1.1.0....0...1c.1.64.img..0.1.108....0.UI3iEkCUWuk#imgrc=SFlhmQivXkB-LM:' , 'Boldore', '70', '105', '105', '50', '40', '20', '390');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('526','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=VjnAW_zuDMXQ9APmtZDgAw&q=gigalith&oq=gigalith&gs_l=img.3..0l10.18211.19339.0.21086.8.8.0.0.0.0.96.568.8.8.0....0...1c.1.64.img..0.8.568...0i67k1.0.qnK80MOCFds#imgrc=U7HPKiiTOAeNDM:' , 'Gigalith', '85', '135', '130', '60', '80', '25', '515');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('527', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=bTnAW6yqDpHs9APHkpLoCw&q=woobat&oq=woobat&gs_l=img.3..0l10.15811.16820.0.17127.6.6.0.0.0.0.180.490.5j1.6.0....0...1c.1.64.img..0.6.490...0i67k1j0i10k1.0.kz6WPZwCg9w#imgrc=a3FJ0voxlvVPZM:' , 'Woobat', '55', '45', '43', '55', '43', '72', '313');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('528','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=gDnAW6aaGMqu0PEP546vkA8&q=Swoobat&oq=Swoobat&gs_l=img.3..0j0i10i67k1j0l3j0i10k1j0l4.49310.49310.0.49710.1.1.0.0.0.0.69.69.1.1.0....0...1c.1.64.img..0.1.69....0.1DDg36fwqj4#imgrc=H4jYhkI-SIXMtM:' , 'Swoobat', '67', '57', '55', '77', '55', '114', '425');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('529', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=sznAW-mgMbDA0PEPtc2G2As&q=Drilbur&oq=Drilbur&gs_l=img.3..0l9j0i30k1.28996.34626.0.35572.7.5.0.0.0.0.72.184.3.3.0....0...1c.1.64.img..5.2.132...0i67k1j0i10i67k1j0i10k1.0.BFzpP_fwnic#imgrc=O6eEgc7Bq4bbYM:' , 'Drilbur', '60', '85', '40', '30', '45', '68', '328');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('530', '0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=9jnAW-GtB6H89AO6iJHwCQ&q=Excadrill&oq=Excadrill&gs_l=img.3..0i67k1j0l9.252.252.0.772.1.1.0.0.0.0.136.136.0j1.1.0....0...1c.1.64.img..0.1.136....0.2le5FvpMEG0#imgrc=OBwkxTytUIVx8M:', 'Excadrill' ,'110', '135', '60', '50', '65', '88', '508');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('531','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=-DnAW433Nv-w0PEPxf2K-Ag&q=Audino&oq=Audino&gs_l=img.3..0l10.13508.13508.0.13882.1.1.0.0.0.0.69.69.1.1.0....0...1c.1.64.img..0.1.65....0.xqGxzlCrIEI#imgrc=EuZwd53uCr30wM:' , 'Audino', '103', '60', '86', '60', '86', '50', '445');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('531','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=GzrAW57RJ7DA0PEPtc2G2As&q=Mega+Audino&oq=Mega+Audino&gs_l=img.3..0i67k1l2j0l8.1565.1700.0.2027.2.2.0.0.0.0.72.128.2.2.0....0...1c.1.64.img..0.2.128...0i30k1.0.PCjgZPiHquY#imgrc=yRR0OnSuBULNwM:' , 'Mega Audino', '103', '60', '126', '80', '126', '50', '545');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('532','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=HzrAW5CwIfa40PEPqcKzsAI&q=timburr&oq=timburr&gs_l=img.3..0l4j0i67k1j0l5.28950.30313.0.31106.7.7.0.0.0.0.83.498.7.7.0....0...1c.1.64.img..0.7.494...0i10k1.0.SMZJhmNbEfA#imgrc=TG-3Ut0BQyFkHM:' , 'Timburr', '75', '80', '55', '25', '35', '35', '305');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('533','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=QDrAW8r_Ff7C0PEPo7KMsAk&q=gurdurr&oq=gur&gs_l=img.3.0.0i67k1j0l9.13493.13769.0.16894.3.3.0.0.0.0.84.212.3.3.0....0...1c.1.64.img..0.3.212....0.jXai4QclG1M#imgrc=2MhM0XXtIkjCvM:' , 'Gurdurr', '85', '105', '85', '40', '50', '40', '405');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('534', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=UzrAW5WpBZO60PEPurW00AY&q=Conkeldurr&oq=Conkeldurr&gs_l=img.3..0l10.44721.44721.0.45251.1.1.0.0.0.0.67.67.1.1.0....0...1c.1.64.img..0.1.67....0.tKa4DsPX5F4#imgrc=wQilemduGsgY1M:' , 'Conkeldurr', '105', '140', '95', '55', '65', '45', '505');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('535', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=gjrAW-vcAurM0PEPuZOYqAY&q=Tympole&oq=Tympole&gs_l=img.3..0l10.18230.18970.0.19186.3.3.0.0.0.0.64.184.3.3.0....0...1c.1.64.img..0.3.184...0i30k1.0.VMT2cB7aFfQ#imgrc=_9SrneO_0FJmpM:' , 'Tympole', '50', '50', '40', '50', '40', '64', '294');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('536','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=lzrAW6SQCba20PEP_K6s8AM&q=Palpitoad&oq=Palpitoad&gs_l=img.3..0l10.23162.23162.0.23483.1.1.0.0.0.0.121.121.0j1.1.0....0...1c.1.64.img..0.1.121....0.X6cxe1vK1eQ#imgrc=fu-tTZOwQ3QCZM:' , 'Palpitoad', '75', '65', '55', '65', '55', '69', '384');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('537', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=sDrAW-25G_2y0PEPlaC3mAg&q=Seismitoad&oq=Seismitoad&gs_l=img.3..0l10.17646.20626.0.21382.3.3.0.0.0.0.368.635.1j1j0j1.3.0....0...1c.1.64.img..0.3.635...0i30k1.0.aWKUzCnmGyA#imgrc=X5Ohv6c7cUEpVM:' , 'Seismitoad', '105', '95', '75', '85', '75', '74', '509');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('538','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=xzrAW9nwO5y80PEPr4OI0Ag&q=Throh&oq=Throh&gs_l=img.3..0l5j0i10k1j0l4.20244.20244.0.20543.1.1.0.0.0.0.85.85.1.1.0....0...1c.1.64.img..0.1.85....0.GNCVcAEaHS8#imgrc=AmKJ2uyuNb5TqM:' , 'Throh', '120', '100', '85', '30', '85', '45', '465');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('539', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=8zrAW_GaE_is0PEP0u6piAU&q=Sawk&oq=Sawk&gs_l=img.3..0i67k1j0l9.657.657.0.901.1.1.0.0.0.0.171.171.0j1.1.0....0...1c.1.64.img..0.1.168....0.S4dmpuqS4n0#imgrc=70lIz5zT61vRDM:' , 'Sawk', '75', '125', '75', '30', '75', '85', '465');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('540','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=9jrAW-7nEYzK0PEP7qCk4A0&q=Sewaddle&oq=Sewaddle&gs_l=img.3..0l10.19478.19478.0.19818.1.1.0.0.0.0.104.104.0j1.1.0....0...1c.1.64.img..0.1.104....0.mGqE2lkeih4#imgrc=CvDyODcBNy_55M:' , 'Sewaddle', '45', '53', '70', '40', '60', '42', '310');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('541', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=DDvAW_nyCbSq0PEPnf--yA4&q=Swadloon&oq=Swadloon&gs_l=img.3..0l10.98500.103298.0.103598.2.2.0.0.0.0.72.128.2.2.0....0...1c.1.64.img..0.2.128....0.lvzJBDUvPnc#imgrc=pwXy_KiQtlRKPM:' , 'Swadloon', '55', '63', '90', '50', '80', '42', '380');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('542', '0' , 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=pDvAW_6XE4i60PEPpOqW-As&q=Leavanny&oq=Leavanny&gs_l=img.3..0i67k1j0l9.2082.2082.0.2845.1.1.0.0.0.0.74.74.1.1.0....0...1c.1.64.img..0.1.74....0.yv-yZn6CihE#imgrc=0Di02h07Rt4-BM:' , 'Leavanny', '75', '103', '80', '70', '80', '92', '500');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('543', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=qTvAW6DxCoHd9APSkLGoCQ&q=Venipede&oq=Venipede&gs_l=img.3..0l10.23488.23488.0.24374.1.1.0.0.0.0.67.67.1.1.0....0...1c.1.64.img..0.1.64....0.KEBnABcLlNQ#imgrc=ajHq54UMHxIbyM:' , 'Venipede', '30', '45', '59', '30', '39', '57', '260');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('544', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=wzvAW5LfApDG0PEP7969oAk&q=Whirlipede&oq=Whirlipede&gs_l=img.3..0l8j0i30k1l2.21156.21156.0.21817.1.1.0.0.0.0.76.76.1.1.0....0...1c.1.64.img..0.1.72....0.jLRNw0054I4#imgrc=FjK7YBdKhn49ZM:' , 'Whirlipede', '40', '55', '99', '40', '79', '47', '360');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('545','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=2jvAW4yfIYfA0PEP9raOwAw&q=Scolipede&oq=Scolipede&gs_l=img.3..0j0i67k1j0l8.17609.17609.0.18266.1.1.0.0.0.0.68.68.1.1.0....0...1c.1.64.img..0.1.68....0.jovZx0JCMyU#imgrc=RrFta9uJ4WEQMM:' , 'Scolipede', '60', '100', '89', '55', '69', '112', '485');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('546', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=7jvAW8uEMIzE0PEPzfWfgAs&q=Cottonee&oq=Cottonee&gs_l=img.3..0l10.25218.28525.0.28732.14.8.0.0.0.0.84.419.7.7.0....0...1c.1.64.img..10.2.119...0i30k1.0.L_MK2H7nYRs#imgrc=233v9XAiaFst8M:' , 'Cottonee', '40', '27', '60', '37', '50', '66', '280');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('547', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=DTzAW66sEomS0PEP8oyUwA8&q=Whimsicott&oq=Whimsicott&gs_l=img.3..0l10.20434.21065.0.21325.2.2.0.0.0.0.76.135.2.2.0....0...1c.1.64.img..0.2.131...0i30k1.0.T1pd9IHs2V0#imgrc=Xj7scYfcrrCaIM:' , 'Whimsicott', '60', '67', '85', '77', '75', '116', '480');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('548', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=JDzAW_KkI9nE0PEP2MKZ8A0&q=Petilil&oq=Petilil&gs_l=img.3..0l10.22607.23292.0.23548.3.3.0.0.0.0.72.193.3.3.0....0...1c.1.64.img..0.3.193...0i30k1.0.acd7Zg3mEYA#imgrc=pRQ4Ju1GvmbPEM:' , 'Petilil', '45', '35', '50', '70', '50', '30', '280');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('549', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=PjzAW_uUCunD0PEPyO-36AM&q=Lilligant&oq=Lilligant&gs_l=img.3..0l3j0i67k1j0l6.20016.20016.0.20811.1.1.0.0.0.0.125.125.0j1.1.0....0...1c.1.64.img..0.1.121....0.ncWhSWAODmQ#imgrc=uqgarjr5qYAyfM:' , 'Lilligant', '70', '60', '75', '110', '75', '90', '480');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('550', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=VDzAW9ucNb3F0PEPhemdgA4&q=Basculin&oq=Basculin&gs_l=img.3..0l9j0i30k1.40872.40872.0.41526.1.1.0.0.0.0.80.80.1.1.0....0...1c.1.64.img..0.1.77....0.W31A7a5_nS8#imgrc=W1qOmlmI0P9dtM:' , 'Basculin', '70', '92', '65', '80', '55', '98', '460');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('550', '0','https://www.google.ca/search?q=basculin+blue+stripe+form&rlz=1C1EJFA_enCA780CA780&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjO2Yvz4YHeAhWKhFQKHcZlAF4Q_AUIDigB&biw=585&bih=588#imgrc=zAU0QkN--VZDbM:' , 'Basculin(Blue-Striped Form)', '70', '92', '65', '80', '55', '98', '460');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('551','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=xWrAW9GDDOu80PEP1LyJMA&q=sandile&oq=sandile&gs_l=img.3..0l10.24837.25573.0.26018.7.7.0.0.0.0.127.632.4j3.7.0....0...1c.1.64.img..0.7.630...0i67k1.0.FSg3ZErYkxk#imgrc=WdupXldhXyf5wM:' , 'Sandile' , '50', '72', '35', '35', '35', '65', '292');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('552','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=4WrAW5enMMi50PEP8p-xwAk&q=krokorok&oq=krokorok&gs_l=img.3..0l10.16169.17315.0.17621.8.6.0.2.2.0.90.427.6.6.0....0...1c.1.64.img..0.8.441...0i67k1.0.-zIxMyjZIlg#imgrc=tPDQxRdvsOzCtM:' , 'Krokorok', '60', '82', '45', '45', '45', '74', '351');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('553', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=9WrAW9KtHoy40PEP556n4Ao&q=Krookodile&oq=Krookodile&gs_l=img.3..0i67k1j0l9.24038.24038.0.24379.1.1.0.0.0.0.72.72.1.1.0....0...1c.1.64.img..0.1.69....0.LZvKfMaROEI#imgrc=6cTETOWdFdDcxM:' , 'Krookodile', '95', '117', '80', '65', '70', '92', '519');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('554', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=D2vAW8H-KeTH0PEPuf6qiAQ&q=darumaka&oq=darumaka&gs_l=img.3..0l10.15085.18094.0.18453.18.13.0.0.0.0.114.909.10j3.13.0....0...1c.1.64.img..10.3.225...0i7i5i30k1j0i8i7i30k1j0i8i7i10i30k1.0.G8gI0B6RkSo#imgrc=MConlOwh5qocNM:' , 'Darumaka', '70', '90', '45', '15', '45', '50', '315');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('555','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=I2vAW-2ZON-50PEPmrmAIA&q=Darmanitan&oq=Darmanitan&gs_l=img.3..0j0i67k1j0l8.17777.18606.0.18777.2.2.0.0.0.0.74.131.2.2.0....0...1c.1.64.img..0.2.126...0i30k1.0.UwHNO0KBY_c#imgrc=ZXGCSslkqi6FcM:',  'Darmanitan' , '105', '140', '55', '30', '55', '95', '480');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('555', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=OGvAW6j_JMHa9APY66GgBQ&q=Darmanitan%28Zen+Mode%29&oq=Darmanitan%28Zen+Mode%29&gs_l=img.3..0i30k1j0i5i30k1l2.21520.21520.0.22609.1.1.0.0.0.0.67.67.1.1.0....0...1c.1.64.img..0.1.65....0.JM43ksmk2Po#imgrc=PtJpBP6Kc2HbPM:' , 'Darmanitan(Zen Mode)', '105', '30', '105', '140', '105', '55', '540');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('556','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=aWvAW_OsIpHw9APHyJbIBw&q=Maractus&oq=Maractus&gs_l=img.3..0i67k1j0l9.327.327.0.742.1.1.0.0.0.0.347.347.3-1.1.0....0...1c.1.64.img..0.1.346....0.GfMihIy4u1k#imgrc=pkFuZr9HV5Qh9M:' , 'Maractus', '75', '86', '67', '106', '67', '60', '461');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('557', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=bGvAW4zeGeTs9APh3ZbgAw&q=Dwebble&oq=Dwebble&gs_l=img.3..0l8j0i30k1l2.13282.13282.0.13993.1.1.0.0.0.0.73.73.1.1.0....0...1c.1.64.img..0.1.71....0.9cJiYsCpjlM#imgrc=I67WhfB4n7ib6M:' , 'Dwebble', '50', '65', '85', '35', '35', '55', '325');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('558','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=fGvAW9WZBLTl9APPlaa4Aw&q=crustle&oq=crustle&gs_l=img.3..0l10.18203.18986.0.19308.7.6.0.1.1.0.137.506.5j1.6.0....0...1c.1.64.img..0.7.514...0i67k1.0.aiJPtQT2gEk#imgrc=bplz_Jwic53z2M:' , 'Crustle', '70', '95', '125', '65', '75', '45', '475');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('559', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=kWvAW6GcCsvB0PEP6Nu3iAw&q=scarggy&oq=scarggy&gs_l=img.3..0i10i24k1l2.13053.14778.0.14850.7.7.0.0.0.0.298.618.5j0j1.6.0....0...1c.1.64.img..1.6.612...0j0i67k1j0i10k1.0.sqKe6UBjviU#imgrc=C8tUi3nyvdmaaM:' , 'Scarggy' , '50', '75', '70', '35', '70', '48', '348');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('560','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=omvAW_fdBoW60PEPkpWCqAo&q=scrafty&oq=scraftly&gs_l=img.3.0.0i10i24k1.14489.15946.0.17332.8.8.0.0.0.0.177.794.4j4.8.0....0...1c.1.64.img..0.8.788...0j0i67k1j0i10k1.0.tMAreV0fwhI#imgrc=WsNhy7RglzwZVM:' , 'Scrafty', '65', '90', '115', '45', '115', '58', '488');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('561','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=tWvAW8nSE6zF0PEPppGygAs&q=Sigilyph&oq=Sigilyph&gs_l=img.3..0l10.14937.15797.0.16010.2.2.0.0.0.0.76.140.2.2.0....0...1c.1.64.img..0.2.138...0i30k1.0.F2pcWNnmurg#imgrc=biAaaEQDDS48ZM:' , 'Sigilyph', '72', '58', '80', '103', '80', '97', '490');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('562', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=2mvAW6njNN-q0PEPqMuOuAQ&q=yamask&oq=yamask&gs_l=img.3..0l10.17632.20038.0.20217.9.8.1.0.0.0.107.570.7j1.8.0....0...1c.1.64.img..0.9.573...0i67k1.0.cbVxIz8DLzY#imgrc=i_ftCqW9-qQ45M:' , 'Yamask', '38', '30', '85', '55', '65', '30', '303');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('563','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=CmzAW4DaC6yy0PEP8N2-uAU&q=Cofagrigus&oq=Cofagrigus&gs_l=img.3..0l10.10760.10760.0.11550.1.1.0.0.0.0.73.73.1.1.0....0...1c.1.64.img..0.1.72....0.62A9aH4wATg#imgrc=07FD3CZX1IZjvM:' , 'Cofagrigus', '58', '50', '145', '95', '105', '30', '483');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('564','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=F2zAW5_DKbe_0PEPof-z4Ac&q=tirtouga&oq=tirtouga&gs_l=img.3..0l10.22103.24699.0.25065.2.2.0.0.0.0.71.118.2.2.0....0...1c.1.64.img..0.1.68....0.VbdXdL3QegE#imgrc=v5gzMvYmxBG_nM:' , 'Tirtouga', '54', '78', '103', '53', '45', '22', '355');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('565', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=MmzAW5qNF8uy0PEPupCH0A4&q=carracosta&oq=carracosta&gs_l=img.3..0l10.15408.16748.0.16974.10.9.0.1.1.0.84.633.9.9.0....0...1c.1.64.img..0.10.641...0i67k1j0i10k1.0.vT-VN81cc_A#imgrc=vK8v58lq2_qA2M:' , 'Carracosta', '74', '108', '133', '83', '65', '32', '495');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('566','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=RWzAW-SFEKy60PEP1_SEiAs&q=archen&oq=archen&gs_l=img.3..0l10.15590.16194.0.16463.6.6.0.0.0.0.106.482.5j1.6.0....0...1c.1.64.img..0.6.480...0i67k1.0.s5UXJP-DF8k#imgrc=fFEUt-Slf5zy3M:' , 'Archen', '55', '112', '45', '74', '45', '70', '401');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('567', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=V2zAW9SHNtzD0PEPlfuM8AE&q=Archeops&oq=Archeops&gs_l=img.3..0j0i67k1j0l8.13781.14532.0.15026.2.2.0.0.0.0.72.129.2.2.0....0...1c.1.64.img..0.2.125...0i30k1.0.SJqWiYApxng#imgrc=b1ULHaWIWaKZYM:' , 'Archeops', '75', '140', '65', '112', '65', '110', '567');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('568','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=dGzAW-OGOIu90PEPhLKT8AM&q=trubbish&oq=trubbish&gs_l=img.3..0l10.13119.16562.0.16974.10.8.1.1.1.0.78.518.8.8.0....0...1c.1.64.img..0.10.536...0i67k1.0.b8C9WkuCWvQ#imgrc=7JcVXEJFoAYaiM:' , 'Trubbish', '50', '50', '62', '40', '62', '65', '329');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('569', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=h2zAW5uTJd7D0PEP1OyT-AQ&q=garbodor&oq=garbodor&gs_l=img.3..0l10.11238.12525.0.12792.8.6.0.2.2.0.74.403.6.6.0....0...1c.1.64.img..0.8.421...0i67k1.0.yVgQFK6DNeo#imgrc=fPAA-FF4GPc0hM:' , 'Garbodor', '80', '95', '82', '60', '82', '75', '474');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('570','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=lmzAW5eTE4Tj9APWlZCQCQ&q=zorua&oq=zorua&gs_l=img.3..0l10.11280.12196.0.12419.5.4.0.1.1.0.72.258.4.4.0....0...1c.1.64.img..0.5.263...0i67k1.0.tGmmsU1sWg8#imgrc=moGo_ihsFeQO9M:' , 'Zorua' ,'40' , '65', '40', '80', '40', '65', '330');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('571','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=pGzAW7WTHdi_0PEPnPWJoAo&q=Zoroark&oq=Zoroark&gs_l=img.3..0i67k1j0l2j0i67k1j0l2j0i67k1j0j0i67k1l2.11713.12372.0.12635.2.2.0.0.0.0.90.178.2.2.0....0...1c.1.64.img..0.2.175....0.uzToWmk0j2A#imgrc=aSjRx5bqL-0A6M:' , 'Zoroark', '60', '105', '60', '120', '60', '105', '510');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('572', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=s2zAW_uAA7y80PEPtPqJ-Ac&q=Minccino&oq=Minccino&gs_l=img.3..0l10.16609.16609.0.16933.1.1.0.0.0.0.71.71.1.1.0....0...1c.1.64.img..0.1.68....0.1-X-Hg1TpnM#imgrc=UHbyrmaN7T0oaM:' , 'Minccino', '55', '50', '40', '40', '40', '75', '300');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('573','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=xWzAW5SVOam_0PEPoe6L0Aw&q=Cinccino&oq=Cinccino&gs_l=img.3..0l2j0i67k1j0l2j0i67k1j0l4.16914.16914.0.17206.1.1.0.0.0.0.65.65.1.1.0....0...1c.1.64.img..0.1.64....0.HOzbYt55zfc#imgrc=hbELWLwzcVlCgM:' , 'Cinccino', '75', '95', '60', '65', '60', '115', '470');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('574', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=92zAW_3IOr650PEP1vCO0AQ&q=Gothita&oq=Gothita&gs_l=img.3..0l10.15527.15527.0.15829.1.1.0.0.0.0.74.74.1.1.0....0...1c.1.64.img..0.1.71....0.K81UyRJbmB0#imgrc=tKcKFFlAvoRYdM:' , 'Gothita', '45', '30', '50', '55', '65', '45', '290');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('575','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=CW3AW8DDIN2t0PEPjpyikAI&q=Gothorita&oq=Gothorita&gs_l=img.3..0l8j0i30k1l2.13412.14110.0.14321.2.2.0.0.0.0.111.173.1j1.2.0....0...1c.1.64.img..0.2.172....0.D_EKwC5C75A#imgrc=PBWfVB-9_hZD6M:' , 'Gothorita', '60', '45', '70', '75', '85', '55', '390');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('576', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=GW3AW_yZK4_I0PEPurmb6AY&q=Gothitelle&oq=Gothitelle&gs_l=img.3..0l10.16921.21719.0.21882.3.3.0.0.0.0.72.190.3.3.0....0...1c.1.64.img..0.3.189...0i30k1j0i7i30k1.0.Vq8LrN5G0y8#imgrc=e0r8JIVrA8EAxM:' , 'Gothitelle', '70', '55', '95', '95', '110', '65', '490');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('577','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=MW3AW5CbGKrL0PEP_pG94AM&q=Solosis&oq=Solosis&gs_l=img.3..0l10.13932.13932.0.14298.1.1.0.0.0.0.73.73.1.1.0....0...1c.1.64.img..0.1.71....0.WhQ0HsVyiFI#imgrc=gOcbs4J2CWMRQM:',  'Solosis' , '45', '30', '40', '105', '50', '20', '290');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('578','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=QW3AW_2uGaqy0PEP3d6nyAI&q=duosion&oq=duosoi&gs_l=img.3.0.0i10i24k1.12931.13902.0.15609.6.6.0.0.0.0.116.466.5j1.6.0....0...1c.1.64.img..0.6.465...0j0i67k1j0i10k1.0.wx-Kvby-aV4#imgrc=hLyjcZBbC3K95M:' , 'Duosion', '65', '40', '50', '125', '60', '30', '370');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('579','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Um3AW961K8G90PEPy9qOsAs&q=Reuniclus&oq=Reuniclus&gs_l=img.3..0l10.13120.13120.0.13551.1.1.0.0.0.0.69.69.1.1.0....0...1c.1.64.img..0.1.67....0.9FpPTA1r4c8#imgrc=fVoEsXSXkAaceM:' , 'Reuniclus', '110', '65', '75', '125', '85', '30', '490');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('580', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Ym3AW-HyD_PD0PEPoM2xuAM&q=Ducklett&oq=Ducklett&gs_l=img.3..0l8j0i10k1l2.14877.14877.0.15194.1.1.0.0.0.0.74.74.1.1.0....0...1c.1.64.img..0.1.71....0.LaqJQAzw67k#imgrc=9P40kvZ_1j3hLM:' , 'Ducklett', '62', '44', '50', '44', '50', '55', '305');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('581', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=c23AW6DZDqTk9AOarLTICA&q=Swanna&oq=Swanna&gs_l=img.3..0l10.127705.127705.0.128021.1.1.0.0.0.0.69.69.1.1.0....0...1c.1.64.img..0.1.68....0.12Cc4n9pdHk#imgrc=cbGLxJ2chbXjXM:' , 'Swanna', '75', '87', '63', '87', '63', '98', '473');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('582','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Bm7AW9mLM77C0PEPpK2ciA0&q=Vanillite&oq=Vanillite&gs_l=img.3..0i67k1j0l9.763.763.0.1003.1.1.0.0.0.0.87.87.1.1.0....0...1c.1.64.img..0.1.86....0.SsAaLLlOqzY#imgrc=W7wx5sJFB7-uoM:' , 'Vanillite', '36', '50', '50', '65', '60', '44', '305');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('583','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=CW7AW-WCOOGt0PEPqf-hsAw&q=Vanillish&oq=Vanillish&gs_l=img.3..0l10.14113.14113.0.14852.1.1.0.0.0.0.72.72.1.1.0....0...1c.1.64.img..0.1.71....0.e3AYoetqAow#imgrc=fXjjGkmdAw_V8M:' , 'Vanillish', '51', '65', '65', '80', '75', '59', '395');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('584','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Gm7AW8vHFYq50PEP1vyqqAo&q=Vanillux&oq=Vanillux&gs_l=img.3..0l10.23020.23020.0.23327.1.1.0.0.0.0.67.67.1.1.0....0...1c.1.64.img..0.1.66....0.zTaY6H-MKis#imgrc=QN17D3J060Cj5M:' , 'Vanilluxe', '71', '95', '85', '110', '95', '79', '535');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('585','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Rm7AW5zYF76_0PEPpOa16A4&q=Deerling&oq=Deerling&gs_l=img.3..0l10.17699.17699.0.18017.1.1.0.0.0.0.89.89.1.1.0....0...1c.1.64.img..0.1.87....0.TraoYXJN-xU#imgrc=pkVmV1MKv3MtaM:' , 'Deerling', '60', '60', '50', '40', '50', '75', '335');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('586','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=hm7AW5uXHve50PEPyrOduAY&q=Sawsbuck&oq=Sawsbuck&gs_l=img.3..0i67k1j0l9.1032.1032.0.1243.1.1.0.0.0.0.67.67.1.1.0....0...1c.1.64.img..0.1.64....0.Kf01tuZvzdM#imgrc=wR0q7LHtjgbVGM:' , 'Sawsbuck', '80', '100', '70', '60', '70', '95', '475');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('587','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=m27AW7j9Bc3A0PEPq6CTUA&q=Emolga&oq=Emolga&gs_l=img.3..0i67k1j0l9.583.583.0.710.1.1.0.0.0.0.104.104.0j1.1.0....0...1c.1.64.img..0.1.103....0.MTmBnyiJmmw#imgrc=UQ2eKXRnugYARM:' , 'Emolga', '55', '75', '60', '75', '60', '103', '428');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('588', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=nW7AW_brK6q_0PEP-d2sYA&q=Karrablast&oq=Karrablast&gs_l=img.3..0l10.15284.15284.0.15599.1.1.0.0.0.0.67.67.1.1.0....0...1c.1.64.img..0.1.65....0.avQM7OECySI#imgrc=V0Kij6bZsgLgFM:' , 'Karrablast' , '50', '75', '45', '40', '45', '60', '315');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('589','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=rm7AW62MLbL59AP6-ZrwDg&q=Escavalier&oq=Escavalier&gs_l=img.3..0j0i67k1j0l8.14274.14274.0.14572.1.1.0.0.0.0.67.67.1.1.0....0...1c.1.64.img..0.1.66....0.YYWnQtLGbFo#imgrc=-OBEYDKvxLpLlM:' , 'Escavalier' , '70', '135', '105', '60', '105', '20', '495');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('590','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=v27AW4jACL690PEPlOOi4AM&q=Foongus&oq=Foongus&gs_l=img.3..0l10.12397.12397.0.12729.1.1.0.0.0.0.73.73.1.1.0....0...1c.1.64.img..0.1.72....0.MkrGirqKc_k#imgrc=rLNmo9D5AL4bVM:' , 'Foongus' , '69', '55', '45', '55', '55', '15', '294');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('591', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=zW7AW5_OHqu_0PEPvZONsA4&q=Amoonguss&oq=Amoonguss&gs_l=img.3..0l10.12222.13225.0.13432.2.2.0.0.0.0.71.129.2.2.0....0...1c.1.64.img..0.2.124...0i30k1.0.vU_zq2zjeBM#imgrc=wjVxA29Cpzt6TM:' , 'Amoonguss' , '114', '85', '70', '85', '80', '30', '464');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('592', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=3G7AW5XoH4C_0PEPvYuOsA8&q=Frillish&oq=Frillish&gs_l=img.3..0l10.14958.14958.0.15264.1.1.0.0.0.0.87.87.1.1.0....0...1c.1.64.img..0.1.87....0.ZEpfQfe2Uwg#imgrc=ozfROVMSkWmxdM:' , 'Frillish' , '55', '40', '50', '65', '85', '40', '335');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('593', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=7W7AW4uPIfSq0PEPmtebkAg&q=Jellicent&oq=Jellicent&gs_l=img.3..0l10.28364.28364.0.28636.1.1.0.0.0.0.66.66.1.1.0....0...1c.1.64.img..0.1.65....0.O8SHNaOKrxg#imgrc=KVT26Bk79D0zuM:' , 'Jellicent' , '100', '60', '70', '85', '105', '60', '480');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('594','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=E2_AW-eXF5TA0PEPy-6I6Ak&q=Alomomola&oq=Alomomola&gs_l=img.3..0l10.20986.20986.0.21552.1.1.0.0.0.0.72.72.1.1.0....0...1c.1.64.img..0.1.70....0._-5pWoExfy0#imgrc=WUNVVwUzOdEx9M:' , 'Alomomola' , '165', '75', '80', '40', '45', '65', '470');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('595','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Km_AW-vjIKHe9AP66prADA&q=Joltik+&oq=Joltik+&gs_l=img.3..0l10.13620.17708.0.18462.3.3.0.0.0.0.73.188.3.3.0....0...1c.1.64.img..0.3.185...0i30k1.0.Mxu1NbqxY-s#imgrc=8TyGh3cRUClRgM:' , 'Joltik' , '50', '47', '50', '57', '50', '65', '319');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('596', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Pm_AW5G7LKT29APS25ngCQ&q=Galvantula&oq=Galvantula&gs_l=img.3..0l5j0i67k1j0l4.14581.14581.0.14896.1.1.0.0.0.0.71.71.1.1.0....0...1c.1.64.img..0.1.68....0.GC_WCDESzI8#imgrc=hmDK1FqKCMAf_M:' , 'Galvantula' , '70', '77', '60', '97', '60', '108', '472');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('597','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=T2_AW8noJMmz0PEP4bWqyAc&q=ferroseed&oq=ferroseed&gs_l=img.3..0l8j0i30k1l2.16228.17696.0.17818.9.8.0.1.1.0.79.524.8.8.0....0...1c.1.64.img..0.9.527...0i67k1j0i10k1.0.eU6_KO9B4HA#imgrc=1LYj2HJ-MPm06M:' , 'Ferroseed' , '44', '50', '91', '24', '86', '10', '305');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('598', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=Y2_AW82dFfy-0PEP56yRqAM&q=ferrothorn&oq=ferrothorn&gs_l=img.3..0l10.12754.13171.0.13587.2.2.0.0.0.0.75.134.2.2.0....0...1c.1.64.img..0.2.132...0i67k1.0.X2A-ZTho9qE#imgrc=yxLGhnGSnFDhVM:' , 'Ferrothorn' , '74' , '94', '131', '54', '116', '20', '489');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('599','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=cm_AW5TKOYq70PEPoLWp4A0&q=Klink&oq=Klink&gs_l=img.3..0l10.13834.13834.0.14472.1.1.0.0.0.0.69.69.1.1.0....0...1c.1.64.img..0.1.67....0.7RqdpGOUOao#imgrc=zfGlf4vffmecNM:' , 'Klink' , '40', '55', '70', '45', '60', '30', '300');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('600','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=mW_AW6HCErLK0PEPpM2q2AY&q=klang+pokemon&oq=Klang+po&gs_l=img.3.0.0l7j0i5i30k1l3.5833.8667.0.9827.5.5.0.0.0.0.179.472.4j1.5.0....0...1c.1.64.img..0.5.467...0i30k1j0i67k1j0i10k1.0.RLMFdT-Ogfg#imgrc=1nspsI9cVHgl7M:' , 'Klang' , '60', '80', '95', '70', '85', '50', '440');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('601', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=pG_AW5DKKpq70PEPmdeNsAs&q=Klinklang&oq=Klinklang&gs_l=img.3..0j0i67k1l2j0l7.16474.16474.0.16802.1.1.0.0.0.0.70.70.1.1.0....0...1c.1.64.img..0.1.68....0.PWpsBPyRNVE#imgrc=5tnBb8fJwDIhTM:' , 'Klinklang' , '60', '100' , '115' , '70', '85', '90', '520');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('602','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=ym_AW8GVH-a50PEPuoeH2AY&q=tynamo&oq=tyamo&gs_l=img.3.2.0l2j0i10i24k1l2j0i24k1l2j0i10i24k1l4.785.1283.0.4989.5.5.0.0.0.0.204.530.3j1j1.5.0....0...1c.1.64.img..0.5.528...0i67k1j0i10k1j0i5i30k1.0.d-QuBQMtSBA#imgrc=EhDw_HdEoTg1kM:' , 'Tynamo' , '35', '55', '40', '45', '40', '60', '275');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('603','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=0W_AW7GUA4_K0PEP35-b2A0&q=Eelektrik&oq=Eelektrik&gs_l=img.3..0l7j0i30k1l2j0i24k1.18143.18143.0.18798.1.1.0.0.0.0.71.71.1.1.0....0...1c.1.64.img..0.1.70....0.gjlt0j70E6g#imgrc=MWhIBwf9HBVjhM:' , 'Eelektrik' , '65', '85', '70', '75', '70', '40', '405');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('604','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=5W_AW_bSLsW60PEPmpm_4Ao&q=Eelektross&oq=Eelektross&gs_l=img.3..0l10.16757.16757.0.17332.1.1.0.0.0.0.82.82.1.1.0....0...1c.1.64.img..0.1.81....0.xWeoQArZsQk#imgrc=vfzwHV9fkAWDuM:' , 'Eelektross' , '85' , '115', '80', '105', '80', '50', '515');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('605', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=-W_AW_zUAaTI0PEPxfWEyAE&q=Elgyem&oq=Elgyem&gs_l=img.3..0l10.21345.21345.0.21802.1.1.0.0.0.0.76.76.1.1.0....0...1c.1.64.img..0.1.75....0.R6sNUh_6Neo#imgrc=qSsKrRt2G2mimM:' , 'Elgyem' , '55', '55', '55', '85', '55', '30', '335');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('606','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=EHDAW_ySKdav0PEPo9Sh2A8&q=Beheeyem&oq=Beheeyem&gs_l=img.3..0l10.15854.15854.0.16460.1.1.0.0.0.0.73.73.1.1.0....0...1c.1.64.img..0.1.72....0.aomwNj-aIyU#imgrc=vS0UyeXZwbyMgM:' , 'Beheeyem' ,  '75', '75', '75', '125', '95', '40', '485');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('607','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=I3DAW6qfAd3I0PEP56ik0AU&q=Litwick&oq=Litwick&gs_l=img.3..0l10.17003.21760.0.22107.5.5.0.0.0.0.75.299.5.5.0....0...1c.1.64.img..0.5.296...0i30k1.0.12iK52seMyM#imgrc=St0T8jkQbwwOMM:' , 'Litwick' , '50', '30', '55', '65', '55', '20', '275');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('608', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=VHDAW72nFNOV0PEPnZWJ-A8&q=Lampent&oq=Lampent&gs_l=img.3..0i67k1j0l9.720.720.0.886.1.1.0.0.0.0.157.157.0j1.1.0....0...1c.1.64.img..0.1.155....0.tCH3VeYVnRw#imgrc=0w9eqqyUK70LjM:' , 'Lampent' , '60', '40', '60', '95', '60', '55', '370');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('609','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=V3DAW92nBYm-0PEP-82i4AI&q=Chandelure&oq=Chandelure&gs_l=img.3...23484.23484.0.25272.1.1.0.0.0.0.0.0..0.0....0...1c.1.64.img..1.0.0....0.emO-ZgWssG0#imgrc=kBwNaWrU9FQC9M:' , 'Chandelure' , '60', '55', '90', '145', '90', '80', '520');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('610','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=cXDAW9C_PPS00PEP576eoAw&q=axew&oq=axew&gs_l=img.3..0l10.12846.13184.0.13781.4.4.0.0.0.0.119.348.3j1.4.0....0...1c.1.64.img..0.4.346...0i67k1.0.DClGZCziONQ#imgrc=fsHw0tLeajU3-M:' , 'Axew' , '46', '87', '60', '30', '40', '57', '320');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('611', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=gXDAW7SDNcK90PEP-8uH8Ac&q=fraxure&oq=fraxure&gs_l=img.3..0l10.13731.14690.0.14994.7.6.0.1.1.0.137.468.5j1.6.0....0...1c.1.64.img..0.7.475...0i67k1.0.Oc5iqpyyUz8#imgrc=7-lqjTdWEiShFM:' , 'Fraxure' , '66', '117', '70', '40', '50', '67', '410');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('612', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=knDAW5HBIfiz0PEPlbqYqA8&q=haxorus&oq=haxorus&gs_l=img.3..0i67k1j0l9.14605.16422.0.16880.7.6.0.1.1.0.78.417.6.6.0....0...1c.1.64.img..0.7.426....0.1B65_1W38tY#imgrc=9gTAsq3FqkswOM:' , 'Haxorus' , '76', '147', '90', '60', '70', '97', '540');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('613','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=pXDAW6jfD4K_0PEPl_KdyA4&q=Cubchoo&oq=Cubchoo&gs_l=img.3..0l10.26345.26345.0.26619.1.1.0.0.0.0.70.70.1.1.0....0...1c.1.64.img..0.1.67....0.lq3biL4f9V4#imgrc=xTAFrQHhEgjSdM:' , 'Cubchoo' , '55', '70', '40', '60', '40', '40', '305');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('614', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=13DAW5-XCODA0PEPifWfuAE&q=beartic&oq=beartic&gs_l=img.3..0l10.1074.1074.0.1379.1.1.0.0.0.0.76.76.1.1.0....0...1c.1.64.img..0.1.75....0.zwc9ukz_twM#imgrc=6P_Cj5whnMErkM:' , 'Beartic' , '95', '110', '80', '70', '80', '50', '485');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('615', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=2nDAW5WRIuO50PEPmf2hsA8&q=Cryogonal&oq=Cryogonal&gs_l=img.3..0l10.16772.16772.0.17064.1.1.0.0.0.0.68.68.1.1.0....0...1c.1.64.img..0.1.67....0.Bjb-zmGgr9s#imgrc=bBJwZ88YHvrMlM:' , 'Cryogonal' , '70', '50', '30', '95', '135', '105', '485');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('616', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=7XDAW-_hD9La9APHyZDQCg&q=Shelmet&oq=Shelmet&gs_l=img.3..0l10.4372.5526.0.5733.2.2.0.0.0.0.65.127.2.2.0....0...1c.1.64.img..0.2.125...0i30k1.0.FtZObWF6KIc#imgrc=_4OEbBq35UmFEM:' , 'Shelmet' , '50', '40', '85', '40', '65', '25', '305');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('617','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=CHHAW9igBM3A0PEPq6CTUA&q=Accelgor&oq=Accelgor&gs_l=img.3..0l10.25074.25074.0.25764.1.1.0.0.0.0.69.69.1.1.0....0...1c.1.64.img..0.1.67....0.vCH8l8DRy0Y#imgrc=oBcL4VTL8k4McM:' , 'Accelgor' , '80', '70', '40', '100', '60', '145', '495');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('618', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=I3HAW-TgG9jE0PEP99C_yAw&q=stunfisk&oq=stunfisk&gs_l=img.3..0l10.16518.19930.0.20199.18.10.5.3.3.0.163.881.7j3.10.0....0...1c.1.64.img..0.11.571...0i67k1j0i10k1.0.kiQI0zE5gE4#imgrc=yrEfvYk-N5RqpM:' , 'Stunfisk' , '109', '66', '84', '81', '99', '32', '471');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('619','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=OXHAW_XeGaDL0PEP1-a64AM&q=mienfoo&oq=mienfoo&gs_l=img.3..0l10.13289.14028.0.14270.7.5.0.2.2.0.117.321.3j1.4.0....0...1c.1.64.img..1.6.336...0i67k1.0.BjNxWR0gsT8#imgrc=TyBsa_w4SH6WDM:' , 'Mienfoo' , '45', '85', '50', '55', '50', '65', '350');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('620', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=SXHAW7qHH6iu0PEP8dSa4AM&q=Mienshao&oq=Mienshao&gs_l=img.3..0l9.17675.17675.0.18024.1.1.0.0.0.0.67.67.1.1.0....0...1c.1.64.img..0.1.66....0.Z5kO2qZqY_c#imgrc=t5BVnSiF17QcPM:' , 'Mienshao' , '65', '125', '60', '95', '60', '105', '510');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('621', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=XXHAW--wFee90PEPv9ClkAs&q=druddigon&oq=druddigon&gs_l=img.3..0l10.13358.14673.0.14903.9.5.0.4.4.0.70.310.5.5.0....0...1c.1.64.img..0.9.347...0i67k1.0.W2TjTwC2y-g#imgrc=s0fXtz8SpE_-rM:' , 'Druddigon' , '77', '120', '90', '60', '90', '48', '485');

INSERT INTO pokemon_table1 (NPNumber, HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('622', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=bXHAW7P-OJur0PEP3N2CmAQ&q=Golett&oq=Golett&gs_l=img.3..0l10.16935.16935.0.17205.1.1.0.0.0.0.69.69.1.1.0....0...1c.1.64.img..0.1.68....0.DwkXXjx-sv0#imgrc=z64G-XgcXL_s9M:' , 'Golett' , '59', '74', '50', '35', '50', '35', '303');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('623', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=gHHAW872OIu-0PEP2YOOuA4&q=Golurk&oq=Golurk&gs_l=img.3..0l10.23303.23303.0.23756.1.1.0.0.0.0.67.67.1.1.0....0...1c.1.64.img..0.1.67....0.KR-nddoFp6g#imgrc=8zHjaemi7HYUjM:' , 'Golurk' , '89', '124', '80', '55', '80', '55', '483');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('624', '0','https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=mnHAW42tGdPj9AOVmZX4Cw&q=Pawniard&oq=Pawniard&gs_l=img.3..0l10.15752.15752.0.16172.1.1.0.0.0.0.74.74.1.1.0....0...1c.1.64.img..0.1.72....0.znCp2y3jvUY#imgrc=AsJiDCBvEdFQcM:' , 'Pawniard' , '45', '85', '70', '40', '40', '60', '340');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('625','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=rHHAW4GCEYjB0PEPp9eA0AM&q=Bisharp&oq=Bisharp&gs_l=img.3..0l9j0i67k1.12533.12533.0.12853.1.1.0.0.0.0.68.68.1.1.0....0...1c.1.64.img..0.1.67....0.qnWNfQTJuOQ#imgrc=PLlq0C9gRPQ8_M:' , 'Bisharp' , '65', '125', '100', '60', '70', '70', '490');

INSERT INTO pokemon_table1 (NPNumber,HPNumber , Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('626','0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=unHAW5mRNeex0PEPiuqO2A0&q=Bouffalant&oq=Bouffalant&gs_l=img.3..0l10.12938.13648.0.13845.2.2.0.0.0.0.72.128.2.2.0....0...1c.1.64.img..0.2.125...0i30k1j0i10i30k1.0.7ViJYknYdI4#imgrc=7H3KbZEuELymgM:' , 'Bouffalant' , '95', '110', '95', '40', '95', '55', '490');

INSERT INTO pokemon_table1 (NPNumber, HPNumber ,Image, Name, HP, Atk, Def, SAt, SDf, Spd, BST) VALUES ('627', '0', 'https://www.google.ca/search?rlz=1C1EJFA_enCA780CA780&biw=1242&bih=597&tbm=isch&sa=1&ei=ynHAW6uiIILe9APg-ZKACg&q=Rufflet&oq=Rufflet&gs_l=img.3..0l10.25226.26106.0.26305.2.2.0.0.0.0.72.136.2.2.0....0...1c.1.64.img..0.2.132...0i30k1.0.sFi3D25VJ9c#imgrc=XzLzE9S1blQXwM:' , 'Rufflet' , '70', '83', '50', '37', '50', '60', '350');

INSERT INTO pokemon_table2 (Name, Type) VALUES ('Turtwig', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES ('Grotle', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Torterra', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES ('Torterra', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Chimchar', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Monferno', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type)VALUES ('Monferno', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type)  VALUES ('Infernape', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Infernape', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Piplup', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Prinplup', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Empoleon', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Empoleon', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Starly', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Starly', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES ('Staravia', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Staravia', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES ('Staraptor', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Staraptor', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bidoof', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bibarel', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bibarel', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Kricketot', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES ('Kricketune', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Shinx', 'electric') ; 

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Luxio', 'electric') ; 

INSERT INTO pokemon_table2 (Name, Type) VALUES  ( 'Budew', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Budew', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES ('Roserade', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Roserade', 'Poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cranidos', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rampardos', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Shieldon', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Shieldon', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bastiodon', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bastiodon', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Burmy', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Wormadam', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Wormadam', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Wormadam(Sandy Cloak)', 'bug');

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Wormadam(Sandy Cloak)', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Wormadam(Trash Cloak)', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Wormadam(Trash Cloak)', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mothim', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mothim', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Combee', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Combee', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Vespiquen', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Vespiquen', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Pachirisu', 'electric') ; 

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Buizel', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Floatzel', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cherubi', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cherrim', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cherrim(Sunshine Form)', 'grass') ; 

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Shellos', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gastrodon', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gastrodon', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Ambipom', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Drifloon', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Drifloon', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Drifblim', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Drifblim', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Buneary', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Lopunny', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Lopunny', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Lopunny', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mismagius', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Honchkrow', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Honchkrow', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Glameow', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Purugly', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Chingling', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Stunky', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Stunky', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Stuntank', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Stuntank', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bronzor', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bronzor', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bronzong', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bronzong', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bonsly', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mime Jr.', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mime Jr.', 'fairy') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Happiny', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Chatot', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Chatot', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Spiritomb', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Spiritomb', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gible', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gible', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gabite', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gabite', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Garchomp', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Garchomp', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Garchomp', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Garchomp', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Munchlax', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Riolu', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Lucario', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Lucario', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Lucario', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Lucario', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Hippopotas', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Hippowdon', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Skorupi', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Skorupi', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Drapion', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Drapion', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Croagunk', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Croagunk', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Toxicroak', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Toxicroak', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Carnivine', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Finneon', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Lumineon', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mantyke', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mantyke', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Snover', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Snover', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Abomasnow', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Abomasnow', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Abomasnow', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Abomasnow', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Weavile', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Weavile', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Magnezone', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Magnezone', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Lickilicky', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rhyperior', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rhyperior', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Tangrowth', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Electivire', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Magmortar', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Togekiss', 'fairy') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Togekiss', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Yanmega', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Yanmega', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Leafeon', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('glaceon', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('gliscor', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('gliscor', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mamoswine', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mamoswine', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Porygon-Z', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gallade', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gallade', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Gallade', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Gallade', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Probopass', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Probopass', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Dusknoir', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Froslass', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Froslass', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom(Heat Rotom)', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom(Heat Rotom)', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom(Wash Rotom)', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom(Wash Rotom)', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom(Frost Rotom)', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom(Frost Rotom)', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom(Fan Rotom)', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom(Fan Rotom)', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom(Mow Rotom)', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rotom(Mow Rotom)', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Uxie', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mesprit', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Azelf', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Dialga', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Dialga', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Palkia', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Palkia', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Heatran', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Heatran', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Regigigas', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Giratina', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Giratina', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Giratina(Origin Forme)', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Giratina(Origin Forme)', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cresselia', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Phione', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Manaphy', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Darkrai', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Shaymin', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Shaymin(Sky Forme)', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Shaymin(Sky Forme)', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Arceus', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Victini', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Victini', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Snivy', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Servine', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Serperior', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Tepig', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Pignite', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Pignite', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Emboar', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Emboar', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Oshawott', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Dewott', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Samurott', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Patrat', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Watchog', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Lillipup', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Herdier', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Stoutland', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Purrloin', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Liepard', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Pansage', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Simisage', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Pansear', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Simisear', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Panpour', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Simipour', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Munna', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Musharna', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Pidove', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Pidove', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Tranquill', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Tranquill', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Unfezant', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Unfezant', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Blitzle', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Zebstrika', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Roggenrola', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Boldore', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gigalith', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Woobat', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Woobat', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Swoobat', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Swoobat', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Drilbur', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Excadrill', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Excadrill', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Audino', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Audino', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mega Audino', 'fairy') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Timburr', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gurdurr', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Conkeldurr', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Tympole', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Palpitoad', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Palpitoad', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Seismitoad', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Seismitoad', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Throh', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Sawk', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Sewaddle', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Sewaddle', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Swadloon', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Swadloon', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Leavanny', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Leavanny', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Venipede', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Venipede', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Whirlipede', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Whirlipede', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Scolipede', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Scolipede', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cottonee', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cottonee', 'fairy') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Whimsicott', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Whimsicott', 'fairy') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Petilil', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Basculin', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Basculin(Blue-Striped Form)', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Sandile', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Sandile', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Krokorok', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Krokorok', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Krookodile', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Krookodile', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Darumaka', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Darmanitan', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Darmanitan(Zen Mode)', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Darmanitan(Zen Mode)', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Maractus', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Dwebble', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Dwebble', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Crustle', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Crustle', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Scarggy', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Scarggy', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Scrafty', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Scrafty', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Sigilyph', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Sigilyph', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Yamask', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cofagrigus', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Tirtouga', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Tirtouga', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Carracosta', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Carracosta', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Archen', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Archen', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Archeops', 'rock') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Archeops', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Trubbish', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Garbodor', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Zorua', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Zoroark', 'dark') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Minccino', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cinccino', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gothita', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gothorita', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Gothitelle', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Solosis', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Duosion', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Reuniclus', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Ducklett', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Ducklett', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Swanna', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Swanna', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Vanillite', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Vanillish', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Vanilluxe', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Deerling', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Deerling', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Sawsbuck', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Sawsbuck', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Emolga', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Emolga', 'flying') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Karrablast', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Escavalier', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Escavalier', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Foongus', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Foongus', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Amoonguss', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Amoonguss', 'poison') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Frillish', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Frillish', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Jellicent', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Jellicent', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Alomomola', 'water') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Joltik', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Joltik', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Galvantula', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Galvantula', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Ferroseed', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Ferroseed', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Ferrothorn', 'grass') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Ferrothorn', 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Klink' , 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Klang' , 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Klinklang' , 'steel') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Tynamo', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Eelektrik', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Eelektross', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Elgyem', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Beheeyem', 'psychic') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Litwick', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Litwick', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Lampent', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Lampent', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Chandelure', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Chandelure', 'fire') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Axew', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Fraxure', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Haxorus', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cubchoo', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Beartic', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Cryogonal', 'ice') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Shelmet', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Accelgor', 'bug') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Stunfisk', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Stunfisk', 'electric') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mienfoo', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Mienshao', 'fight') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Druddigon', 'dragon') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Golett', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Golett', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Golurk', 'ground') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Golurk', 'ghost') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Pawniard' , 'dark');

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Pawniard' , 'steel');

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bisharp' , 'dark');

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bisharp' , 'steel');

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Bouffalant', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rufflet', 'normal') ;

INSERT INTO pokemon_table2 (Name, Type) VALUES  ('Rufflet', 'flying') ;

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Normal', 'Ghost');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fighting', 'Rock');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fighting', 'Bug');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fighting', 'Dark');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Flying', 'Fighting');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Flying', 'Ground');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Flying', 'Bug');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Flying', 'Grass');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Poison', 'Fighting');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Poison', 'Poison');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Poison', 'Grass');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Poison', 'Fairy');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Ground', 'Poison');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Ground', 'Rock');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Ground', 'Electric');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Rock', 'Normal');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Rock', 'Flying');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Rock', 'Poison');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Rock', 'Fire');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Bug', 'Fighting');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Bug', 'Ground');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Bug', 'Grass');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Ghost', 'Normal');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Ghost', 'Fighting');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Ghost', 'Poison');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Ghost', 'Bug');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Normal');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Flying');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Poison');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Rock');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Bug');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Steel');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Grass');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Psychic');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Ice');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Dragon');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Steel', 'Fairy');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fire', 'Bug');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fire', 'Steel');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fire', 'Fire');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fire', 'Grass');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fire', 'Ice');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Water', 'Steel');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Water', 'Fire');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Water', 'Water');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Water', 'Ice');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Grass', 'Ground');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Grass', 'Water');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Grass', 'Grass');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Grass', 'Electric');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Electric', 'Flying');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Electric', 'Steel');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Electric', 'Electric');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Psychic', 'Fighting');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Psychic', 'Psychic');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Ice', 'Ice');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Dragon', 'Fire');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Dragon', 'Water');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Dragon', 'Grass');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Dragon', 'Electric');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fairy', 'Fighting');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fairy', 'Bug');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fairy', 'Dragon');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Fairy', 'Dark');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Dark', 'Ghost');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Dark', 'Psychic');

INSERT INTO type_resistant (Type, ResistantTo) VALUES ('Dark', 'Dark');

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Normal', 'None'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fighting', 'Normal'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fighting', 'Rock'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fighting', 'Steel'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fighting', 'Ice'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fighting', 'Dark'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Flying', 'Fighting'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Flying', 'Bug'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Flying', 'Grass'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Poison', 'Grass'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Poison', 'Fairy'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ground', 'Poison');
 
INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ground', 'Rock'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ground', 'Steel'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ground', 'Fire'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ground', 'Electric'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Rock', 'Flying'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Rock', 'Bug'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Rock', 'Fire'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Rock', 'Ice'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Bug', 'Grass'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Bug', 'Pshchic'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Bug', 'Dark'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ghost', 'Ghost'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ghost', 'Psychic'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Steel', 'Rock'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Steel', 'Ice'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Steel', 'Fairy'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fire', 'Bug'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fire', 'Steel'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fire', 'Grass'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fire', 'Ice'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Water', 'Ground'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Water', 'Rock'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Water', 'Fire'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Grass', 'Ground'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Grass', 'Rock'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Grass', 'Water'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Electric', 'Flying'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Electric', 'Water'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Psychic', 'Fighting'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Psychic', 'Poison'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ice', 'Flying');
 
INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ice', 'Ground'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ice', 'Grass'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Ice', 'Dragon'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Dragon', 'Dragon'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fairy', 'Fighting'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fairy', 'Dragon'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Fairy', 'Dark'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Dark', 'Ghost'); 

INSERT INTO type_strong (Type, StrongAgainst) VALUES ('Dark', 'Psychic'); 

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Normal', 'Fighting');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Fighting', 'Flying');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Fighting', 'Psychic');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Fighting', 'Fairy');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Flying', 'Rock');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Flying', 'Electric');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Flying', 'Ice');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Poison', 'Ground');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Poison', 'Psychic');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Ground', 'Water');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Ground', 'Grass');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Ground', 'Ice');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Rock', 'Fighting');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Rock', 'Ground');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Rock', 'Steel');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Rock', 'Water');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Rock', 'Grass');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Bug', 'Flying');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Bug', 'Rock');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Bug', 'Fire');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Ghost', 'Ghost');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Ghost', 'Dark');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Steel', 'Fighting');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Steel', 'Ground');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Steel', 'Fire');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Fire', 'Ground');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Fire', 'Rock');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Fire', 'Water');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Water', 'Grass');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Water', 'Electric');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Grass', 'Flying');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Grass', 'Poison');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Grass', 'Bug');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Grass', 'Fire');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Grass', 'Ice');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Electric', 'Ground');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Psychic', 'Bug');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Psychic', 'Ghost');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Psychic', 'Dark');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Ice', 'Fighting');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Ice', 'Rock');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Ice', 'Steel');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Ice', 'Fire');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Dragon', 'Ice');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Dragon', 'Dragon');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Dragon', 'Fairy');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Fairy', 'Poison');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Fairy', 'Steel');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Dark', 'Fighting');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Dark', 'Bug');

INSERT INTO type_vulnerable (Type, VulnerableTo) VALUES ('Dark', 'Fairy');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Normal', 'Rock'); 

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Normal', 'Ghost'); 

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Normal', 'Steel'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fighting', 'Flying'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fighting', 'Poison'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fighting', 'Psychic'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fighting', 'Bug'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fighting', 'Ghost'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fighting', 'Fairy'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Flying', 'Rock'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Flying', 'Steel'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Flying', 'Electric'); 

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Poison', 'Poison'); 

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Poison', 'Ground'); 

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Poison', 'Rock'); 

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Poison', 'Ghost'); 

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Poison', 'Steel'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Ground', 'Flying'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Ground', 'Bug'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Ground', 'Grass'); 

INSERT INTO type_weak (Type, WeakAgainst) VALUES('Rock', 'Fighting');

INSERT INTO type_weak (Type, WeakAgainst) VALUES('Rock', 'Ground');

INSERT INTO type_weak (Type, WeakAgainst) VALUES('Rock', 'Steel');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Bug', 'Fighting');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Bug', 'Flying');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Bug', 'Poison');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Bug', 'Ghost');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Bug', 'Steel');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Bug', 'Fire');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Bug', 'Fairy');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Ghost', 'Normal');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Ghost', 'Dark');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Steel', 'Steel');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Steel', 'Fire');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Steel', 'Water');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Steel', 'Electric');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fire', 'Rock'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fire', 'Fire'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fire', 'Water'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fire', 'Dragon'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Water', 'Water');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Water', 'Grass');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Water', 'Dragon');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Grass', 'Flying'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Grass', 'Poison'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Grass', 'Bug'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Grass', 'Steel'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Grass', 'Fire'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Grass', 'Grass'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Grass', 'Dragon'); 

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Electric', 'Ground');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Electric', 'Grass');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Electric', 'Electric');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Electric', 'Dragon');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Psychic', 'Steel');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Psychic', 'Psychic');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Psychic', 'Dark');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Ice', 'Steel');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Ice', 'Fire');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Ice', 'Water');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Ice', 'Ice');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Dragon', 'Steel');

INSERT INTO type_weak (Type, WeakAgainst) VALUES ('Dragon', 'Fairy');

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fairy', 'Poison'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fairy', 'Steel'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Fairy', 'Fire'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Dark', 'Fighting'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Dark', 'Dark'); 

INSERT INTO type_weak(Type, WeakAgainst) VALUES ('Dark', 'Fairy'); 

















