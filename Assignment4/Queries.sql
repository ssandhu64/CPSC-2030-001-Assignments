
SELECT * 
FROM (
	SELECT * 
    	FROM pokemon_table1 
    	ORDER BY NPNumber DESC 
    	LIMIT 10
                ) AS pokemon_record
ORDER BY Spd ASC;




SELECT pokemon_table1.NPNumber , pokemon_table1.HPNumber , pokemon_table1.Image , 
	pokemon_table1.Name , pokemon_table1.HP , pokemon_table1.Atk , pokemon_table1.Def , pokemon_table1.SAt , 
        	pokemon_table1.SDf , pokemon_table1.Spd , pokemon_table1.BST , type_vulnerable.VulnerableTo , type_resistant.ResistantTo
FROM pokemon_table1 
	INNER JOIN pokemon_table2 
	ON 
    	pokemon_table1.name = pokemon_table2.name 
   	 INNER JOIN type_vulnerable 
    	ON 
    	pokemon_table2.type = type_vulnerable.type
    	INNER JOIN type_resistant
    	ON 
    	pokemon_table2.type = type_resistant.type
WHERE type_vulnerable.VulnerableTo = 'Ground' AND type_resistant.ResistantTo = 'Steel';





SELECT pokemon_table1.NPNumber , pokemon_table1.HPNumber , pokemon_table1.Image , 
		pokemon_table1.Name , pokemon_table1.HP , pokemon_table1.Atk , pokemon_table1.Def , pokemon_table1.SAt , 
        		pokemon_table1.SDf , pokemon_table1.Spd , pokemon_table1.BST , type_weak.WeakAgainst
FROM pokemon_table1 
     	INNER JOIN pokemon_table2 
    	ON 
   	 pokemon_table1.name = pokemon_table2.name 
    	INNER JOIN type_weak 
    	ON 
    	pokemon_table2.type = type_weak.type
WHERE pokemon_table1.BST > 200     AND     pokemon_table1.BST < 500     AND    type_weak.WeakAgainst = 'Water';





SELECT pokemon_table1.NPNumber , pokemon_table1.HPNumber , pokemon_table1.Image , 
		pokemon_table1.Name , pokemon_table1.HP , pokemon_table1.Atk , pokemon_table1.Def , pokemon_table1.SAt , 
        		pokemon_table1.SDf , pokemon_table1.Spd , pokemon_table1.BST , type_vulnerable.VulnerableTo
FROM pokemon_table1 
	INNER JOIN pokemon_table2 
    	ON 
    	pokemon_table1.name = pokemon_table2.name 
    	INNER JOIN type_vulnerable 
    	ON 
    	pokemon_table2.type = type_vulnerable.type
WHERE type_vulnerable.VulnerableTo ='Fire'       AND       pokemon_table1.Name LIKE 'Mega%'      AND     pokemon_table1.Atk = ( SELECT MAX(Atk)  FROM  pokemon_table1) ;
