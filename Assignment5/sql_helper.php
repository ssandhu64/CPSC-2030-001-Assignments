<?php

function clearConnection($mysql){
    while($mysql->more_results()){
       $mysql->next_result();
       $mysql->use_result();
    }
}

//helper function
function wrap($tag,$value) {
    return "<$tag>$value</$tag>";
}
?>
