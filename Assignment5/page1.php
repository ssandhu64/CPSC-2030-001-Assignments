<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet/less" type="text/css" href="styles.less" />
      <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
      <title>Pokedex</title>
  </head>

  <?php

    include "sql_helper.php";

    //SQL.
    $servername = "localhost";
    $username = "CPSC2030";
    $password = "CPSC2030";
    $database = "pokedex";

    //Creating connection.
    $conn = new mysqli($servername, $username, $password , $database);

    //Checking Connection.
    if ($conn->connect_error)
    {
        die("Connection failed: " . $conn->connect_error);
    }

    if(isset($_GET["Type"]))
    {
      $Type = mysqli_real_escape_string($conn, $_GET["Type"]); //prevent SQL injection
      $result = $conn->query("call filter_table(\"$Type\")");
    }
    else
    {
      $result = $conn->query("call get_NumberNameType()");
    }

    clearConnection($conn);

  ?>

  <body>

    <h1>Pokedex</h1>

      <a href="page1.php">Click Here to See Unfiltered Result</a>
      <p>Choose a <span>Pokedex Card Type</span> to Filter the Results. <br> Click on the <span>National Pokemon Number (NPN)</span> to read its description.</p>

    <?php

      if ($result)
      {
        while($row = $result->fetch_assoc())
        {
          $link1 = "page1.php?Type=".urlencode($row["Type"]);

          $link2 = "page2.php?NPNumber=".urlencode($row["NPNumber"]);

          echo "<div> <br>"."<strong>"."NPN :"."</strong>"."<a href= '$link2'>".$row["NPNumber"]."</a> <br>"."<strong> Name</strong>: ". $row["Name"]. "</a> <br>" ."<strong>Type: </strong>"."<a href= '$link1'>".$row["Type"]."<br>". "</a>" ."<br>"."</div>" ;

        }
      }
      else
      {
        echo "No Result Available.";
      }
      $conn->close();

    ?>

  </body>
</html>
