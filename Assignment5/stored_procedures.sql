DELIMITER //
CREATE PROCEDURE get_NumberNameType()
BEGIN
  SELECT pokemon_table1.NPNumber , pokemon_table1.Name , pokemon_table2.Type
  From pokemon_table1 INNER JOIN pokemon_table2 ON pokemon_table1.Name = pokemon_table2.Name;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE filter_table
(IN tp varchar(20))
BEGIN
select pokemon_table1.NPNumber , pokemon_table1.Name , pokemon_table2.Type
from pokemon_table1 INNER JOIN pokemon_table2 ON pokemon_table1.Name = pokemon_table2.Name
WHERE pokemon_table2.type = tp;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE description_by_NPNumber
(IN np INT)
BEGIN
select pokemon_table1.NPNumber,pokemon_table1.Name,pokemon_table2.Type,
pokemon_table1.HPNumber , pokemon_table1.HP , pokemon_table1.Atk , pokemon_table1.Def , pokemon_table1.SAt , pokemon_table1.SDf , pokemon_table1.Spd , pokemon_table1.BST , type_resistant.ResistantTo , type_strong.StrongAgainst,type_vulnerable.VulnerableTo,type_weak.WeakAgainst
from pokemon_table1
INNER JOIN pokemon_table2 	ON pokemon_table1.Name 	= pokemon_table2.Name
INNER JOIN type_resistant 	ON pokemon_table2.type 	= type_resistant.type
INNER JOIN type_strong 	  	ON type_resistant.type 	= type_strong.type
INNER JOIN type_vulnerable 	ON type_strong.type 	= type_vulnerable.type
INNER JOIN type_weak 		ON type_vulnerable.type = type_weak.type
WHERE pokemon_table1.NPNumber = np
GROUP BY pokemon_table1.NPNumber , pokemon_table1.NAME,   pokemon_table1.HPNumber , pokemon_table1.HP , pokemon_table1.Atk , pokemon_table1.Def , pokemon_table1.SAt , pokemon_table1.SDf , pokemon_table1.Spd , pokemon_table1.BST ;
END //
DELIMITER ;
