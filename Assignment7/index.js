let characterNumber;

//Creating a constructor for each attribute of a character.
function character(name , side , unit , rank , role , description , imageUrl) {

    this.name = name;
    this.side = side;
    this.unit = unit;
    this.rank = rank;
    this.role = role;
    this.description = description;
    this.imageUrl = imageUrl;

}

//Creating an Array of size 10 for storing character objects.
let characters = new Array(10);

//Creating different character objects and storing in an array.
characters[0] = new character("Claude Wallace" , "Edinburgh Army" , "Ranger Corps, Squad E" , "First Lieutenant" , "Tank Commander" , "Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates." , "Images/claudeWallace.jpg");

characters[1] = new character("Riley Miller" , "Edinburgh Army" , "Federate Joint Ops" , "Second Lieutenant" , "Artillery Advisor" , "Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones." , "Images/rileyMiller.jpg");

characters[2] = new character("Raz" , "Edinburgh Army" , "Ranger Corps, Squad E" , "Sergeant" , "Fireteam Leader" , "Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible." , "Images/raz.jpg");

characters[3] = new character("Kai Schulen" , "Edinburgh Army" , "Ranger Corps, Squad E" , "Sergeant Major" , "Fireteam Leader" , "Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename 'Deadeye Kai'. Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault." , "Images/kaiSchulen.jpg");

characters[4] = new character("Louffe" , "Edinburgh Navy" , "Centurion, Cygnus Fleet" , "Sergeant" , "Radar Operator" , "As the Centurion's crewmember responsible for route reconnaissance, this young Darcsen has a keen intellect and acerbic wit beyond her age. Although her harsh tongue keeps most people at arm's length, she's developed a strong companionship with Marie Bennet." , "Images/louffe.jpg");

characters[5] = new character("Minerva Victor" , "Edinburgh Army" , "Ranger Corps, Squad F" , "First Lieutenant" , "Senior Commander" , "Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals." , "Images/minervaVictor.jpg");

characters[6] = new character("Karen Stuart" , "Edinburgh Army" , "Squad E" , "Corporal" , "Combat EMT" , "Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household." , "Images/karenStuart.jpg");

characters[7] = new character("Miles Arbeck" , "Edinburgh Army" , "Ranger Corps, Squad E" , "Sergeant" , "Tank Operator" , "Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby." , "Images/milesArbeck.jpg");

characters[8] = new character("Dan Bentley","Edinburgh Army","Ranger Corps, Squad E" , "Private First Class" , "APC Operator" , "Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat." , "Images/danBentley.jpg");

characters[9] = new character("Ronald Albee","Edinburgh Army","Ranger Corps, Squad F", "Second Lieutenant","Tank Operator","Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor's underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code." , "Images/ronaldAlbee.jpg");

function myFunction()
{
    var string="";
    for(let i = 1 ; i <= 10 ;i++)
    {
        string += "<div onmouseover='profile_display(id)' id='name" + i + "' >";
        string += characters[i-1].name;
        string += "</div>"
    }

    document.getElementById("name").innerHTML=string;
    selectAll(".names");

}

function selectAll(container)
{
  //Selects all the div elements inside the .list_of_names method.
  var character_names = document.querySelectorAll(container + "> div");

  character_names.forEach((element)=>{

    //Applying quick handler on each of the element that is, the div elements.
  element.onclick = clickHandler1(element);

   });
}

let count = 0;
let i;
let id;
let character_id = new Array(5);
let squad_name = new Array();
let repetition = false;
let index = 0;
let matchValue;
let countFlag = false;

//Returns each div element like: <div id="name1" class="names" ></div>
function clickHandler1(name_element)
{
  var element = name_element;
  return  (event) => {

        //If the user clicks on the right element.
        if (event.target == element)
        {
            element.className = "newClass";
            id = event.target.id;

              //PUSHING the character ids into the array to check for UNIQUENESS.
              for(i = 0 ; i < 5 ; i++)
              {
                      if(id == character_id[i])
                      {
                        alert("No duplicates allowed!");
                        repetition = true;
                        break;
                      }

                      else
                      {
                        repetition = false;
                      }
              }


              if( repetition == false)
              {
                character_id[index++] = id;
                count++;
                let idArray = ["name1" , "name2" , "name3" , "name4" ,"name5" , "name6" , "name7" , "name8" , "name9" , "name10" ];

                for( x = 0 ; x < character_id.length  ; x++)
                {
                  for(y = 0 ; y < idArray.length ; y++)
                  {
                      if(character_id[x] == idArray[y])
                      {
                         matchValue = y;
                      }
                  }
                }

                if(count > 5)
                {
                  //Removing the 6th id from the character_id array since MAXIMUM of 5 characters are allowed.
                  character_id.pop();
                  countFlag = true;
                  alert("Maximum of 5 Unique Names can be added to the Squad");

                }

                if(countFlag == true)
                {

                }
                else
                {
                    squad_name.push(characters[matchValue].name);
                    document.getElementById("squad").innerHTML  = document.getElementById("squad").innerHTML + "<div onmouseover='profile_display()'>" + characters[matchValue].name +  "</div>";
                }

              } // Repetition is false.

        } //End of event.target condition.

    } //End of the event.

} //End of click handler function.

let matchingValue;
function profile_display(id)
{

  let idArray = ["name1" , "name2" , "name3" , "name4" ,"name5" , "name6" , "name7" , "name8" , "name9" , "name10" ];

    for(y = 0 ; y < idArray.length ; y++)
    {
        if(id == idArray[y])
        {
           matchingValue = y;
        }
    }

    document.getElementById("characterProfile").innerHTML  =  "<div>   <span>Name:</span>" + characters[matchingValue].name +  "</div>" + "<div> <span>  Side:</span>" + characters[matchingValue].side +  "</div>" +  "<div> <span>  Unit:</span>" + characters[matchingValue].unit +  "</div>" + "<div>   <span> Rank: </span>" + characters[matchingValue].rank +  "</div>"  +  "<div>  <span> Role:</span>" + characters[matchingValue].role +  "</div>";

    var url = characters[matchingValue].imageUrl;

    document.getElementById("characterPicture").src= url;

}
