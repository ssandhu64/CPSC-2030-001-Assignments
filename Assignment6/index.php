<?php

    //Start the Session
    session_start();

   require_once 'sqlhelper.php';
   require_once './vendor/autoload.php';
   $loader = new Twig_Loader_Filesystem('./templates');

   $twig = new Twig_Environment($loader);

    //SQL SetUp.
    $conn = connectToMyDatabase();
    $result = $conn->query("call popular_pokemons()");

    echo $twig->render('header.twig.html',array(
    'txt' => 'My Favourite Pokemon' ));

    /////////////////////////////Result 1///////////////////////////////////////
    if($result)
    {
       $table = $result->fetch_all(MYSQLI_ASSOC);
       $template = $twig->load('sidebar.twig.html');
       echo $template->render(array("popular_pokemons"=>$table));
       $conn->close();
    }
    else
    {
     $template = $twig->load("error.twig.html");
     echo $template->render(array("message"=>"SQL errorm query failed"));
    }

      ///////////////////////////Result 3///////////////////////////////////////
      $conn = connectToMyDatabase();
      if(isset($_GET["Name"]))
      {
        $Name = mysqli_real_escape_string($conn, $_GET["Name"]); //prevent SQL injection
        $result3 = $conn->query("call cards_by_Name(\"$Name\")");

      }
      else
      {
        $result3 = "No Favourites Selected Yet";
      }
      echo $twig->render('favourite_pokemons.twig.html',array("favourite_pok" => $result3
      ));

    ///////////////////////////Result 2/////////////////////////////////////////
    $conn = connectToMyDatabase();
    $result2 = $conn->query("call get_NumberName()");
    if($result2)
    {
       $table = $result2->fetch_all(MYSQLI_ASSOC);
       $template = $twig->load('pokemon_table.twig.html');
       echo $template->render(array(
      "pokemon_table"=>$table ,
      "favourite_pok"=> $result3
    ));
       $conn->close();
    }
    else
    {
     $template = $twig->load("error.twig.html");
     echo $template->render(array("message"=>"SQL errorm query failed"));
    }


?>
