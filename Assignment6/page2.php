<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles2.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>

    <title>Description</title>


</head>

  <?php

    $servername = "localhost";
    $username = "CPSC2030";
    $password = "CPSC2030";
    $database = "pokedex";
    $Name =  $_GET["Name"];

    //Creating connection.
  $conn = new mysqli($servername, $username, $password , $database);

    //Checking Connection.
    if ($conn->connect_error)
    {
        die("Connection failed: " . $conn->connect_error);
    }

    $result = $conn->query("call description_by_Name(\"$Name\")");

  ?>

  <body>

    <a href="index.php">Click Here to see Page 1</a>

    <?php

      if ($result)
      {
        while($row = $result->fetch_assoc())
        {
          echo "<div><br><strong>NPN:</strong> ".$row["NPNumber"]."<br><strong>Name:</strong>".$row["Name"]."<br><strong>Type:</strong>".$row["Type"]."<br><strong>HPNumber:</strong>".$row["HPNumber"]."<br><strong>HP:</strong>".$row["HP"]."<br><strong>Atk:</strong>".$row["Atk"]."<br><strong>Def:</strong>".$row["Def"]."<br><strong>SAt:</strong>".$row["SAt"]."<br><strong>SDf:</strong>".$row["SDf"]."<br><strong>Spd:</strong>".$row["Spd"]."<br><strong>BST:</strong>".$row["BST"]."<br><strong>Resistant to: </strong>".$row["ResistantTo"]."<br><strong>Strong Against: </strong>".$row["StrongAgainst"]."<br><strong>Vulnerable to: </strong>".$row["VulnerableTo"]."<br><strong>Weak Against: </strong>".$row["WeakAgainst"]."</div>";
        }
      }
      else
      {
        echo "No Result Available.";
      }
      $conn->close();

    ?>

  </body>
</html>
