DELIMITER //
CREATE PROCEDURE popular_pokemons()
BEGIN
select pokemon_table1.NPNumber,pokemon_table1.Name,pokemon_table2.Type,
pokemon_table1.HPNumber , pokemon_table1.HP , pokemon_table1.Atk , pokemon_table1.Def , pokemon_table1.SAt , pokemon_table1.SDf , pokemon_table1.Spd , pokemon_table1.BST , type_resistant.ResistantTo , type_strong.StrongAgainst,type_vulnerable.VulnerableTo,type_weak.WeakAgainst
from pokemon_table1
INNER JOIN pokemon_table2 	ON pokemon_table1.Name 	= pokemon_table2.Name
INNER JOIN type_resistant 	ON pokemon_table2.type 	= type_resistant.type
INNER JOIN type_strong 	ON type_resistant.type 	= type_strong.type
INNER JOIN type_vulnerable 	ON type_strong.type 		= type_vulnerable.type
INNER JOIN type_weak 	ON type_vulnerable.type 	= type_weak.type
WHERE pokemon_table1.Name = 'Empolean' OR pokemon_table1.Name = 'Torterra' OR pokemon_table1.Name = 'Electivire' OR pokemon_table1.Name = 'Hippowdon' OR pokemon_table1.Name = 'Tangrowth' OR pokemon_table1.Name = 'Rampardos' OR pokemon_table1.Name = 'Togekiss' OR pokemon_table1.Name = 'Mamoswine' OR pokemon_table1.Name = 'Rhyperior' OR pokemon_table1.Name = 'Garchomp'
GROUP BY pokemon_table1.NPNumber , pokemon_table1.Name,   pokemon_table1.HPNumber , pokemon_table1.HP , pokemon_table1.Atk , pokemon_table1.Def , pokemon_table1.SAt , pokemon_table1.SDf , pokemon_table1.Spd , pokemon_table1.BST ;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE description_by_Name
(IN nm VARCHAR(40))
BEGIN
select pokemon_table1.NPNumber,pokemon_table1.Name,pokemon_table2.Type,
pokemon_table1.HPNumber , pokemon_table1.HP , pokemon_table1.Atk , pokemon_table1.Def , pokemon_table1.SAt , pokemon_table1.SDf , pokemon_table1.Spd , pokemon_table1.BST , type_resistant.ResistantTo , type_strong.StrongAgainst,type_vulnerable.VulnerableTo,type_weak.WeakAgainst
from pokemon_table1
INNER JOIN pokemon_table2 	ON pokemon_table1.Name 	= pokemon_table2.Name
INNER JOIN type_resistant 	ON pokemon_table2.type 	= type_resistant.type
INNER JOIN type_strong 	  	ON type_resistant.type 	= type_strong.type
INNER JOIN type_vulnerable 	ON type_strong.type 	= type_vulnerable.type
INNER JOIN type_weak 		ON type_vulnerable.type = type_weak.type
WHERE pokemon_table1.Name = nm
GROUP BY pokemon_table1.NPNumber , pokemon_table1.NAME,   pokemon_table1.HPNumber , pokemon_table1.HP , pokemon_table1.Atk , pokemon_table1.Def , pokemon_table1.SAt , pokemon_table1.SDf , pokemon_table1.Spd , pokemon_table1.BST ;
END //
DELIMITER ;



DELIMITER //
CREATE PROCEDURE cards_by_Name
(IN name VARCHAR(40))
BEGIN
select pokemon_table1.NPNumber,pokemon_table1.Name
from pokemon_table1
WHERE pokemon_table1.Name = name;
END //
DELIMITER ;



DELIMITER //
CREATE PROCEDURE get_NumberName()
BEGIN
select pokemon_table1.NPNumber, pokemon_table1.Name
from pokemon_table1;
END //
DELIMITER ;
